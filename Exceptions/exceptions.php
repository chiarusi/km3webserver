<?php

/**
 * Define a custom exception class
 */
class MalformedJSONException extends Exception
{
    // Redefine the exception so message isn't optional
    public function __construct($message, $code = 400, Exception $previous = null) {
    
        $message = "Malformed JSON data." . $message;
        parent::__construct($message, $code, $previous);
    }

    // custom string representation of object
    public function __toString() {
        return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
    }

}

class UserAlreadyLoggedInException extends Exception
{
    // Redefine the exception so message isn't optional
    public function __construct($user, $code = 406, Exception $previous = null) {
    
        $message = "User:" . $user . " has already logged In, use the retrive token service or logout";
        parent::__construct($message, $code, $previous);
    }

    // custom string representation of object
    public function __toString() {
        return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
    }

}

class LoginFailException extends Exception
{
    // Redefine the exception so message isn't optional
    public function __construct($message = null, $code = 406, Exception $previous = null) {
    
        $message = "Login Failed, bad username or password " . $message;
        parent::__construct($message, $code, $previous);
    }

    // custom string representation of object
    public function __toString() {
        return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
    }

}

class TokenExpiredException extends Exception
{
    // Redefine the exception so message isn't optional
    public function __construct($message = null, $code = 403, Exception $previous = null) {
    
        $message = "The token is expired please login again " . $message;
        parent::__construct($message, $code, $previous);
    }

    // custom string representation of object
    public function __toString() {
        return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
    }

}
class CommandNotFoundException extends Exception
{
    // Redefine the exception so message isn't optional
    public function __construct($message = null, $code = 404, Exception $previous = null) {
    
        $message = "The request command not exist " . $message;
        parent::__construct($message, $code, $previous);
    }

    // custom string representation of object
    public function __toString() {
        return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
    }

}
class UnathorizedUserExeption extends Exception
{
    // Redefine the exception so message isn't optional
    public function __construct($message = null, $code = 401, Exception $previous = null) {
    
        $message = "The User can't issue commands: " . $message;
        parent::__construct($message, $code, $previous);
    }

    // custom string representation of object
    public function __toString() {
        return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
    }
}
    
class CantLoginException extends Exception
{
    // Redefine the exception so message isn't optional
    public function __construct($message = null, $code = 401, Exception $previous = null) {
    
        $message = "This User can't Login: " . $message;
        parent::__construct($message, $code, $previous);
    }

    // custom string representation of object
    public function __toString() {
        return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
    }
}

class CantEscalateException extends Exception
{
    // Redefine the exception so message isn't optional
    public function __construct($message = null, $code = 406, Exception $previous = null) {
    
        $message = "This User can't escalate: " . $message;
        parent::__construct($message, $code, $previous);
    }

    // custom string representation of object
    public function __toString() {
        return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
    }
}

class LocalDaemonNotFoundException extends Exception
{
    // Redefine the exception so message isn't optional
    public function __construct($message = null, $code = 1, Exception $previous = null) {
    
        $message = "Can't open the local socket " . $message;
        parent::__construct($message, $code, $previous);
    }

    // custom string representation of object
    public function __toString() {
        return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
    }
}
