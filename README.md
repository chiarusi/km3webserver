### Preamble:

the "tridas" user must have the environment variable
TRIDAS_ROOT = <whatever>/<directory>/<for>/<installation>/<of>/<TriDAS>
 
### INSTALLATION TRIDAS
 
 1. Download last version from  Jenkins  
    * https://ci.infn.it/jenkins/job/KM3/job/tridas_full_build_release/label=centos7,node=Docker/  
2. Untar the download tarball in some repo direcotry; 
3. move the "bin" and "lib" directories contained in the untarred package in the $TRIDAS_ROOT ; 
4. log into the  machine committed to act as the TriDAS GUI as user "tridas" (tridas) 
5. log into the screen/tmux section called "TSC",    CTRL-C for killing the current version of TSC (if it is running) 
    * screen -r TSC
    * if there isn't 
        * screen -S TSC    
6. launch a new TSC command:   RunTSC 
7. ctlr+a d
### Deployment del s/w di TriDAS GUI/webserver/CrossBARIO
1.  create directory
    * sudo  mkdir -p /var/www/km3itagui 
    * sudo mkdir -p /var/www/km3webserver
2. cd /etc/httpd/conf.d
3. git clone https://ciberkids@bitbucket.org/ciberkids/km3apacheconfitagui.git .
    * **Attention to the last DOT**
### INSTALLATION OF WEBSERVICE
1. cd /var/www/km3webserver
2. clone project
3. git clone https://ciberkids@bitbucket.org/ciberkidskm3webserver.git .
    * **Attention to the last DOT**
4. disable selinux
    * vim /etc/selinux/config
    * modify SELINUX=enabled → SELINUX=disabled
5. install (httpd php)
    * yum install httpd php
6. install composer
    * php -r "readfile('https://getcomposer.org/installer');" > composer-setup.php
    * php -r "if (hash('SHA384', file_get_contents('composer-setup.php')) === '7228c001f88bee97506740ef0888240bd8a760b046ee16db8f4095c0d8d525f2367663f22a46b48d072c816e7fe19959') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
    * php composer-setup.php
    * php -r "unlink('composer-setup.php');"
7. launch composer library installation
    * composer update
8. install crossbar
    * http://crossbar.io/docs/Installation-on-CentOS/
    * (need pyhton-pip)
    * Follow the instructions contained in the subsection "Installing Crossbar.io" (which requires python2.7)
    * sudo pip install crossbar[all]
9. enable httpd daemon
    * systemctl enable httpd
    
## Install GUI:

1.  cd /var/www/km3itagui 
2. git clone https://ciberkids@bitbucket.org/ciberkids/km3itagui.git
    * **Attention to the last DOT**
3. That's it….

### Setup Service

1. Start the httpd Service
    * systemctl restart httpd
2. Try to open the gui from browser if the login is succesfull the configuration is correct.
3. If problem with the log (not present for example, see /var/www/km3webserver/logs)
    * see NOTE 1
    
### Launch e finalize the setup

1.  install screen/tmux
    * sudo yum install screen
3. launch a screen session with TSC title
    * screen -S crossbar
    * cd /var/www/km3webserver
    * crossbar start –loglevel=info
    * ctrl+a d
4. **END** the webservice is up and running

NOTE 1) pecl config-set php_ini /etc/php.ini

NOTE 2) possible problem with socket:
    * vim /usr/lib/systemd/system/httpd.service
    * privateTmp=false
    * systemctl daemon-reload
    * systemctl restart httpd