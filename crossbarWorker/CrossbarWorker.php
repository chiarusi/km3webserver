<?php
/**
 * Created by PhpStorm.
 * User: favaro
 * Date: 22/10/15
 * Time: 13.42
 */
require __DIR__ . '/../vendor/autoload.php';

echo "Connector WORKER";
//echo __DIR__."\n";


use TSCWebServer\PushNotification;
use TSCWebServer\SQLiteWrapper;
use TSCWebServer\LocalDaemon;
use TSCWebServer\LocalDaemonAnswer;
use TSCWebServer\utils;
use TSCWebServer\CommandParam;
use TSCWebServer\Commands;
use \PDOException;
use Thruway\ClientSession;
use Thruway\Connection;

$logWriter           = new \Slim\Extras\Log\DateTimeFileWriter(
  array(
    'path'           => __DIR__ . '/../logs/',
    'name_format'    => '\c\o\n\n\e\c\t\o\r\-\w\o\r\k\e\r',
    'message_format' => '%label% - %date% - %message%'
  )
);
$logWriterEscalation = new \Slim\Extras\Log\DateTimeFileWriter(
  array(
    'path'           => __DIR__ . '/../logs/',
    'name_format'    => '\e\s\c\a\l\a\t\i\o\n\-\w\o\r\k\e\r',
    'message_format' => '%label% - %date% - %message%'
  )
);

//echo print_r($logWriter);

$log           = new \Slim\Log($logWriter);
$logEscalation = new \Slim\Log($logWriterEscalation);
//$log->setLevel( \Slim\Log::INFO );
//$logEscalation->setLevel( \Slim\Log::INFO );


error_reporting(E_ALL);

function handleError($errno,
                     $errstr,
                     $error_file,
                     $error_line
) {
  echo "Error: [$errno] $errstr - $error_file:$error_line";
  echo "\n";
  echo "Terminating PHP Script\n";
  throw new \Exception("Error: [$errno] $errstr - $error_file:$error_line");
}

//set error handler
set_error_handler("handleError");


$statusTimer    = null;
$autograntTimer = null;

$loop       = React\EventLoop\Factory::create();
$connection = new Connection(
  [
    "realm" => 'realm1',
    "url"   => 'ws://127.0.0.1:8080/'
  ],
  $loop
);

$publishConnection = new Connection(
  [
    "realm"  => 'realm1',
    "url"    => 'ws://127.0.0.1:8080/',
    "secret" => "kkjH68GiuUZ",
    "key"    => "test"
  ],
  $loop
);

$log->debug("----- START ---------");


$localDaemon      = new LocalDaemon($log);
$pushnotification = new PushNotification($log);

$dbDir = __DIR__ . "/..";


//TODO: fix with query command
$checkDaemonLoop = function () use
(
  &$localDaemon,
  &$log,
  &$pushnotification,
  &$dbDir
) {
  
  try {
    echo __DIR__;
    $db = new SQLiteWrapper($log, $dbDir);
    
    $log->debug("----checkDaemonLoop---");
    $ans = $localDaemon->checkDaemon();
    if (!$ans->getDaemonActive()) {
      if ($localDaemon->connect()) {
        $log->debug(
          "yes! i'll connect again with the local daemon... inserting a new status!!!"
        );
        
        $daemonInitialData = $localDaemon->getConnectionStatus();
        
        $db->setStatus(
          $daemonInitialData->getAnswerObject(), // ojb
          $daemonInitialData->getCurrentState()
        );
      }
    }
    //daemon active! check status
    $command = new CommandParam();
    $command->setCommand("query");
    $command->setUserID(1);
    $log->debug($command);
    $pushnotification->issueCommandToDaemon($command->getData());
    
  } catch (\Exception $e) {
    echo $e;
    $log->error('Caught exception: ', $e->getMessage());
  } finally {
    if ($db) {
      unset($db);
    }
    $log->debug("-------end checkDaemonLoop---------");
  }
};

$grantFunction = function () use
(
  &$logEscalation,
  &$dbDir,
  &$pushnotification
) {
  $logEscalation->debug("autogranting started");
  $db = new SQLiteWrapper($logEscalation, $dbDir);
  //$db->releasePriviledge();
  $escalatinguser = $db->getEscalatingUserData();
  $db->priviledgeUser($escalatinguser->username);
  $pushnotification->newPriviledgedUser($db->getPriviledgedUserData());
  $logEscalation->debug(
    "autogranting end: new privilege to:" . $escalatinguser->username
  );
  
};


$connection->on(
  'open', function (ClientSession $session) use
(
  $connection,
  &$loop,
  &$statusTimer,
  &$autograntTimer,
  &$log,
  &$logEscalation,
  &$localDaemon,
  &$pushnotification,
  &$dbDir,
  &$grantFunction,
  &$checkDaemonLoop
) {
  
  // SUBSCRIBE to a topic and receive events
  $priviledgeChange = function ($args) {
    echo "event for 'com.tridas.statemachine.priviledged.change' received: "
         . print_r($args[0]) . "\n";
  };
  
  //Command EVENT
  $controlDAQSystem = function ($args) use
  (
    &$log,
    &$localDaemon,
    &$pushnotification,
    &$dbDir,
    &$loop,
    &$statusTimer,
    &$checkDaemonLoop
  ) {
    
    
    try {
      $log->debug("------ event for 'com.tridas.execute.daemon' ---------");
      if ($loop->isTimerActive($statusTimer)) {
        $log->debug("------ Cancelling status timer ---------");
        $loop->cancelTimer($statusTimer);
        $log->debug("---------------");
      }
      $log->debug("---args---");
      $log->debug($args);
      
      $command = $args[0]->command;
      $param   = $args[0]->param;
      $userid  = $args[0]->userid;
      $log->debug("---param----");
      $log->debug("'" . $param . "'");
      $log->debug("----------");
      
      $db = new SQLiteWrapper($log, $dbDir);
      
      $log->debug("----preflight check checkDaemon----");
      $ans = $localDaemon->checkDaemon(); //localdaemonanswer
      $log->debug("---preflight check getDaemonActive && getCommandOngoing---");
      if ($ans->getDaemonActive() && !$ans->getCommandOngoing()) {
        $log->debug("-taking off-");
        $ans->setCommandOngoing(true);
        $log->debug("- db Update on going-");
        $db->setStatus($ans->getAnswerObject(), $ans->getCurrentState());
        if ($ans->getCurrentState() == "idle" && $command == "query") {
          $command = "state";
        }

        $log->info("- controlDAQSystem: issuing command: $command");
        $result = $localDaemon->issueCommand(
          $command, $param
        ); //localdaemonanswer type
        $db->setStatus($result->getAnswerObject(), $result->getCurrentState());
        $log->debug("- db Update done -");
        
        $log->debug("-landing-");
        
        //TODO: audit on command
      } else {
        $log->debug("---- command on going OR Damon inactive----");
        $db->setStatus($ans->getAnswerObject(), $ans->getCurrentState());
        $log->debug("------------------------------------");
      }
      $log->debug("-LANDED-");
      
      $publicStatus = utils::getObjectFromSeparatedArray($db->getStatus());
      $log->debug("---- PUBLISHING STATUS ----");
      //$log->debug($publicStatus);
      $db->logCommand($userid, json_encode($publicStatus));
      $pushnotification->publishDaemonStatus($publicStatus);
      $log->debug("-----------------------");
      $log->debug("------ Setting up timer ---------");
      $statusTimer = $loop->addTimer(2, $checkDaemonLoop);
      $log->debug("---------------");
      
      
    } catch
    (\Exception $e) {
      $log->error('Caught exception: ' . $e);
      
      
    } catch (\PDOException $e) {
      $log->error('Caught PDOException exception: ' . $e);
      var_dump($e);
      echo "PDOException";
    } catch
    (\Exception $e) {
      $log->error('Caught exception: ' . $e);
      var_dump($e);
    } finally {
      if ($db) {
        unset($db);
        
      }
      $log->debug(
        "------ DONE event for 'com.tridas.execute.daemon' ---------"
      );
    }
  };
  $autoGrantStart   = function ($args) use
  (
    &$logEscalation,
    &$localDaemon,
    &$pushnotification,
    &$dbDir,
    &$loop,
    &$autograntTimer,
    &$grantFunction
  ) {
    $logEscalation->debug(
      "------ event for 'com.tridas.privilege.autogrant.start' ---------"
    );
    $logEscalation->debug("---args---");
    $logEscalation->debug($args);
    
    $secondsToAutoGrant = $args[0]->secondsToAutoGrant;
    //$startTimer                = $args[0]->startTimer;
    $currentEscalatingUserName = $args[0]->currentEscalatingUserName;
    $currentEscalatingName     = $args[0]->currentEscalatingName;
    $secondsToForceEnabling    = $args[0]->secondsToForceEnabling;
    
    if (($autograntTimer == null) || !$loop->isTimerActive($autograntTimer)) {
      $logEscalation->debug(
        "------ Starting autogrant timer ---------"
      );
      $autograntTimer = $loop->addTimer($secondsToAutoGrant, $grantFunction);
      $logEscalation->debug(
        "------ timer started ---------"
      );
    } else {
      $logEscalation->debug(
        "------ do nothing ---------"
      );
    }
    $logEscalation->debug(
      "------ DONE event for 'com.tridas.privilege.autogrant.start' ---------"
    );
  };
  $autoGrantStop    = function ($args) use
  (
    &$logEscalation,
    &$localDaemon,
    &$pushnotification,
    &$dbDir,
    &$loop,
    &$autograntTimer,
    &$grantFunction
  ) {
    $logEscalation->debug(
      "------ event for 'com.tridas.privilege.autogrant.stop' ---------"
    );
    
    $logEscalation->debug(
      "------ Halting autogrant timer ---------"
    );
    if ($loop->isTimerActive($autograntTimer)) {
      $loop->cancelTimer($autograntTimer);
      $logEscalation->debug(
        "------ Halted autogrant timer ---------"
      );
    } else {
      $logEscalation->debug(
        "------ do nothing ---------"
      );
    }
    
    $logEscalation->debug(
      "------ DONE event for 'com.tridas.privilege.autogrant.stop' ---------"
    );
    
  };
  
  
  $session->subscribe(
    'com.tridas.statemachine.priviledged.change', $priviledgeChange
  );
  $log->debug(
    "subscribed to topic 'com.tridas.statemachine.priviledged.change'"
  );
  
  $session->subscribe('com.tridas.execute.daemon', $controlDAQSystem);
  $log->debug("subscribed to topic 'com.tridas.execute.daemon'");
  
  $session->subscribe('com.tridas.privilege.autogrant.start', $autoGrantStart);
  $log->debug("subscribed to topic 'com.tridas.privilege.autogrant.start'");
  $session->subscribe('com.tridas.privilege.autogrant.stop', $autoGrantStop);
  $log->debug("subscribed to topic 'com.tridas.privilege.autogrant.stop'");
  
  
}
);


$connection->on(
  'close', function ($reason) use
(
  $loop,
  &$statusTimer,
  &$db,
  &$log
) {
  $log->debug("-------------- CLOSING----------------");
  if ($statusTimer) {
    $loop->cancelTimer($statusTimer);
  }
  if ($db) {
    unset($db);
  }
  echo "The connected has closed with reason: {$reason}\n";
  $log->debug("-------------- CLOSING----------------");
}
);

$connection->on(
  'error', function ($reason) {
  echo "The connected has closed with error: {$reason}\n";
}
);

//TODO: fix with only timer and put the recall on the status
$statusTimer = $loop->addTimer(2, $checkDaemonLoop);


$connection->open();

