<?php
require 'vendor/autoload.php';
//require_once './Exceptions/exceptions.php';
//require_once './util/HtpasswdAuth.php';
//require_once './util/Answers.php';
//require_once './util/sqlitelib.php';
//require_once './util/utils.php';
//require_once './util/Escalation.php';
//require_once './util/Commands.php';


//include_once './webSocket/startServer.php';

use TSCWebServer\SQLiteWrapper;
use TSCWebServer\Answer;
use TSCWebServer\Escalation;
use TSCWebServer\RunSetup;
use TSCWebServer\MalformedJSONException;
use TSCWebServer\TokenExpiredException;
use TSCWebServer\CommandNotFoundException;
use TSCWebServer\LoginFailException;
use TSCWebServer\CantLoginException;
use TSCWebServer\Commands;
use TSCWebServer\CommandParam;


$app = new \Slim\Slim(array(
    'debug' => true,
    'mode' => 'development',
    'log.enabled' => true,
    'log.level' => \Slim\Log::DEBUG,
    'log.writer' => new \Slim\Extras\Log\DateTimeFileWriter(
      array(
        'path' => 'logs/',
        'name_format' => 'Y-m-d',
        'message_format' => '%label% - %date% - %message%'
      )
    ),
  )
);
// After instantiation
$log = $app->getLog();
//$log->debug("test");
//$log->info("test");
//$log->error("test");
//$log->critical("test");
//$auth = new HtpasswdAuth(".htpasswd");


/*
 * 
 * TEST WEB SOCKET
 * 
 * 
 *
 $entryData = array(
        'category' => "test",
        'title'    => "title2",
        'article'  => "bla bla lba ",
        'when'     => time()
    );
   $context = new ZMQContext();
   $socket = $context->getSocket(ZMQ::SOCKET_PUSH, 'my pusher');
   $socket->connect("tcp://localhost:5555");
   $socket->send(json_encode($entryData));
   $log->debug("web socket test send");
    */


/**
 *
 * END TEST
 *
 * **/


function dumpToLog($var, $log)
{
  ob_start();
  var_dump($var);
  $log->debug(ob_get_clean());
}

//______________________________________________________________________

$app->get('/login', function () use ($app, $log) {
  try {

    $ans = new Answer();
    $ans->setReason("required json field: username, password. Password must be hashed");
    $log->debug("GET->/login'");
    //$ans = array( 'usage' => 'required json field: username, password. Password must be hashed');
    echo json_encode($ans);
  } catch (Exception $e) {
    $log->error("POST /login Error: " . $e->getMessage());
    $log->error($e->getMessage());
    $ans->setFailAction();
    $ans->setReason($e->getMessage());
    $ans->setStatus(500);
  } finally {
    $app->response->setStatus($ans->getStatus());
    $app->response->headers->set('Content-Type', 'application/json');

    echo $ans->getJson();
  }

});
$app->post('/login', function () use ($app, $log) {

  try {
    $ans = new Answer();
    $log->debug(__DIR__);
    $db = new SQLiteWrapper($log, __DIR__);

    $log->info("POST /login");

    $json_post_data = $app->request->getBody();

    $json_post_data = json_decode($json_post_data);
    if (json_last_error() != 0) {
      ob_start();
      var_dump($json_post_data);
      $result = ob_get_clean();
      throw new MalformedJSONException($result);
    }
    $username = $json_post_data->username;
    $password = $json_post_data->password; //hashed
    //$token=$json_post_data->token;
    $log->debug("POST /login " . $username . ":" . $password);


    //1° phase check login information
    if (!$db->matches($username, $password)) {
      throw new LoginFailException();
    }

    //2° phase check existing login session check DB! and in case reset it
    if ($db->checkUserAlreadyLogged($username)) {
      $log->debug("POST /login session Exist resetting");
      $db->resetUserSession($username);
    }
    $log->debug("POST /login existing session fixed");
    $userData = $db->getUserData($username);
    $log->debug($userData);
    if (!$userData->can_login) {
      throw new CantLoginException("Disabled by admin");
    }
    $ans->setRole($userData->role_name);
    $ans->setName($userData->name);

    //3° phase create a new session
    $ans->setToken($db->createAuthToken($username));
    $ans->setOkAction();
    $ans->setReason("User " . $username . " succesfully logged in");
    $db->logAction($userData->id, $ans->getJson());

  } catch (MalformedJSONException $e) {
    $log->error("POST /login Error: " . $e->getMessage());
    //$log->error("POST /login Error: ".$e->getTraceAsString());
    //ob_start();
    //var_dump($e->getTrace()[0]);
    //$log->error("POST /login Error: " . ob_get_clean());

    $ans->setFailAction();
    $ans->setReason($e->getMessage());
    $ans->setStatus($e->getCode());
    $db->logAction(-1, $ans->getJson());

  } catch (\Exception $e) {
    $log->error("POST /login Error: " . $e->getMessage());
    //$log->error("POST /login Error: ".$e->getTraceAsString());
    //ob_start();
    //var_dump($e->getTrace()[0]);
    //$log->error("POST /login Error: " . ob_get_clean());

    $ans->setFailAction();
    $ans->setReason($e->getMessage());
    if($e->getCode() >= 100 && $e->getCode() <= 505 )
      $ans->setStatus($e->getCode());
    else
      $ans->setStatus(500);
    $db->logAction(-1, $ans->getJson());

  } finally {
    $app->response->setStatus($ans->getStatus());
    $app->response->headers->set('Content-Type', 'application/json');
    $log->debug($ans);
    echo $ans->getJson();
  }

});

$app->options('/login', function () use ($app, $log) {
  //Return response headers
  $app->response->headers->set('Access-Control-Allow-Methods', 'GET, POST');
  $app->response->headers->set('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept', 'Authorization');
  //$log->debug($app->response->headers);
});

//______________________________________________________________________
$app->post('/logout', function () use ($app, $log) {
  $ans = new Answer();

  try {
    $log->info("POST /logout");
    $db = new SQLiteWrapper($log, __DIR__);

    $json_post_data = $app->request->getBody();

    $json_post_data = json_decode($json_post_data);
    if (json_last_error() != 0) {
      ob_start();
      var_dump($json_post_data);
      $result = ob_get_clean();
      throw new MalformedJSONException($result);
    }
    $authToken = $json_post_data->authToken;
    //$token=$json_post_data->token;

    $log->debug("POST /logout " . $authToken);


    //1° phase check login information
    $userData = null;
    if ($db->tokenExist($authToken)) {
      $userData = $db->getUserDataFromToken($authToken);
      $db->invalidateToken($userData, $authToken);
    }
    
    $ans->setOkAction();
    $ans->setReason("User " . $userData->username . " succesfully logged out");
    $db->logAction($userData->id, $ans->getJson());


  } catch (MalformedJSONException $e) {
    $log->error("POST /logout Error: " . $e->getMessage());
    $log->error("POST /logout Error: " . $e->getTraceAsString());
    ob_start();
    var_dump($e->getTrace()[0]);
    $log->error("POST /logout Error: " . ob_get_clean());

    $ans->setFailAction();
    $ans->setReason($e->getMessage());
    $ans->setStatus($e->getCode());

  } catch (Exception $e) {
    $log->error("POST /logout Error: " . $e->getMessage());

    //$log->error("POST /logout Error: ".$e->getTraceAsString());
    //ob_start();
    //var_dump($e->getTrace()[0]);
    //$log->error("POST /logout Error: " . ob_get_clean());

    $ans->setFailAction();
    $ans->setReason($e->getMessage());
    if($e->getCode() >= 100 && $e->getCode() <= 505 )
      $ans->setStatus($e->getCode());
    else
      $ans->setStatus(500);
    
    $db->logAction(-1, $ans->getJson());

  } finally {
    $app->response->setStatus($ans->getStatus());
    $app->response->headers->set('Content-Type', 'application/json');
    $log->debug($ans);

    echo $ans->getJson();
  }
});

$app->options('/logout', function () use ($app, $log) {
  //Return response headers
  $app->response->headers->set('Access-Control-Allow-Methods', 'GET, POST');
  $app->response->headers->set('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept', 'Authorization');
  //$log->debug($app->response->headers);
});
//______________________________________________________________________

$app->post('/verifyToken', function () use ($app, $log) {
  $ans = new Answer();

  try {
    $log->info("POST /verifyToken");
    $db = new SQLiteWrapper($log, __DIR__);

    $json_post_data = $app->request->getBody();

    $json_post_data = json_decode($json_post_data);
    if (json_last_error() != 0) {
      ob_start();
      var_dump($json_post_data);
      $result = ob_get_clean();
      throw new MalformedJSONException($result);
    }
    $token = $json_post_data->authToken;
    $log->debug("POST /verifyToken " . $token);

    //1° phase check token information
    if (!$db->checkToken($token)) {
      $log->debug("POST /verifyToken token expired or not exist");
      throw new TokenExpiredException();
    }
    //2° phase renew session expiring date
    $db->renewSession($token);
    $log->debug("POST /verifyToken renewing sessions");
    $userData = $db->getUserDataFromToken($token);
    $log->debug($userData);

    $ans->setUserName($userData->username);
    $ans->setRole($userData->role_name);
    $ans->setName($userData->name);

    //3° phase create a new session
    $ans->setToken($token);
    $ans->setOkAction();
    $ans->setReason("User " . $userData->username . " succesfully logged in");
    $db->logAction($userData->id, $ans->getJson());

  } catch (MalformedJSONException $e) {
    $log->error("POST /login Error: " . $e->getMessage());
    //$log->error("POST /login Error: ".$e->getTraceAsString());
    //ob_start();
    //var_dump($e->getTrace()[0]);
    //$log->error("POST /login Error: " . ob_get_clean());

    $ans->setFailAction();
    $ans->setReason($e->getMessage());
    $ans->setStatus($e->getCode());
    $db->logAction(-1, $ans->getJson());

  } catch (Exception $e) {
    $log->error("POST /verifyToken Error: " . $e->getMessage());
    //$log->error("POST /verifyToken Error: ".$e->getTraceAsString());
    //ob_start();
    //var_dump($e->getTrace()[0]);
    //$log->error("POST /verifyToken Error: " . ob_get_clean());

    $ans->setFailAction();
    $ans->setReason($e->getMessage());
    if($e->getCode() >= 100 && $e->getCode() <= 505 )
      $ans->setStatus($e->getCode());
    else
      $ans->setStatus(500);

    $db->logAction(-1, $ans->getJson());

  } finally {
    $app->response->setStatus($ans->getStatus());
    $app->response->headers->set('Content-Type', 'application/json');
    $log->debug($ans);
    echo $ans->getJson();
  }
});

$app->options('/verifyToken', function () use ($app, $log) {
  //Return response headers
  $app->response->headers->set('Access-Control-Allow-Methods', 'GET, POST');
  $app->response->headers->set('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept', 'Authorization');
  //$log->debug($app->response->headers);
});

//______________________________________________________________________


$app->get('/commands', function () use ($app, $log) {
  echo "Commands API<br />";


  //var_dump(scandir("."));

  //$app->halt(403, 'You shall not pass!');
});

$app->post('/commands', function () use ($app, $log) {
  /*
   * check if the token is valid
   * check if the token is issued from the priviledged user
   *
   * issue the command to the tsc
   * wait an answer
   *
   * if the answer is positive send the push notification
   *
   *
   * */
  $ans = new Answer();
  
  try {
    $log->info("POST /commands");
    $db = new SQLiteWrapper($log, __DIR__);

    $json_post_data = $app->request->getBody();

    $json_post_data = json_decode($json_post_data);
    if (json_last_error() != 0) {
      ob_start();
      var_dump($json_post_data);
      $result = ob_get_clean();
      throw new MalformedJSONException($result);
    }
    $command = $json_post_data->command;
    $param = new CommandParam();
    if (isset($json_post_data->param) && !empty($json_post_data->param)) {
      $log->info("POST /commands");
      $log->info($json_post_data->param);
      $param->setParam($directory = getcwd() . '/storage/runsetup/' . $json_post_data->param);
    }
    $authToken = $json_post_data->authToken;
    //$token=$json_post_data->token;
    $log->debug("POST /commands " . $command . ":" . $authToken);


    //1° phase check login information
    if (!$db->checkToken($authToken)) {
      throw new TokenExpiredException();
    }
    
    //2° phase retrieve user information
    $userData = $db->getUserDataFromToken($authToken);
    $log->debug("userdata:");
    $log->debug($userData);
    $commands = new Commands($db, $log, $userData);
    $param->setUserID($userData->user_id);
    //3° phase check if user can issue commands
    if (!$commands->userCanIssueCommands()) {
      throw new UnathorizedUserExeption($userData->username);
    }
    
    //4° phase add the right answer data to answer
    if (!method_exists($commands, $command)) {
      throw new CommandNotFoundException("commands ->" . $command);
    }
    
    $ans->setAnswerArray($commands->$command($param)->getAnsArray());

    $db->logAction($userData->id, $ans->getJson());


  } catch (MalformedJSONException $e) {
    $log->error("POST /commands Error: " . $e->getMessage());
    $log->error("POST /commands Error: " . $e->getTraceAsString());
    ob_start();
    var_dump($e->getTrace()[0]);
    $log->error("POST /commands Error: " . ob_get_clean());

    $ans->setFailAction();
    $ans->setReason($e->getMessage());
    $ans->setStatus($e->getCode());
    $db->logAction(-1, $ans->getJson());

  } catch (Exception $e) {
    $log->error("POST /commands Error: " . $e->getMessage());
    $log->error("POST /commands Error: " . $e->getTraceAsString());
    //ob_start();
    //var_dump($e->getTrace()[0]);
    //$log->error("POST /commands Error: " . ob_get_clean());

    $ans->setFailAction();
    $ans->setReason($e->getMessage());
    if($e->getCode() >= 100 && $e->getCode() <= 505 )
      $ans->setStatus($e->getCode());
    else
      $ans->setStatus(500);

    $db->logAction(-1, $ans->getJson());

  } finally {
    $app->response->setStatus($ans->getStatus());
    $app->response->headers->set('Content-Type', 'application/json');
    $log->debug($ans);
    echo $ans->getJson();
  }
  //$app->halt(403, 'You shall not pass!');
});

$app->options('/commands', function () use ($app, $log) {
  //Return response headers
  $app->response->headers->set('Access-Control-Allow-Methods', 'GET, POST');
  $app->response->headers->set('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept', 'Authorization');
  //$log->debug($app->response->headers);
});

//______________________________________________________________________


$app->get('/status', function () use ($app, $log) {
  //Do something

  echo "Status API";
  //$app->halt(403, 'You shall not pass!');
});
$app->post('/status', function () use ($app, $log) {
  /*
   * check if the token is valid
   * check if the token is issued from the priviledged user
   * 
   * issue the command to the tsc
   * wait an answer
   * 
   * if the answer is positive send the push notification
   * 
   * 
   * */
  $ans = new Answer();
  
  try {
    $log->info("POST /status");
    $db = new SQLiteWrapper($log, __DIR__);

    $json_post_data = $app->request->getBody();

    $json_post_data = json_decode($json_post_data);
    if (json_last_error() != 0) {
      ob_start();
      var_dump($json_post_data);
      $result = ob_get_clean();
      throw new MalformedJSONException($result);
    }
    $command = $json_post_data->command;
    $param = "";
    if (isset($json_post_data->param)) {
      $param = $json_post_data->param;
    }
    $authToken = $json_post_data->authToken;
    //$token=$json_post_data->token;
    $log->debug("POST /status " . $command . ":" . $authToken);


    //1° phase check login information
    if (!$db->checkToken($authToken)) {
      throw new TokenExpiredException();
    }
    
    //2° phase retrieve user information
    $userData = $db->getUserDataFromToken($authToken);
    $log->debug("POST /status userdata:");
    $log->debug($userData);


    $commands = new Commands($db, $log, $userData);
    if (!method_exists($commands, $command)) {
      throw new CommandNotFoundException("POST /status commands ->" . $command);
    }
    $commandParam = new \TSCWebServer\CommandParam();
    $commandParam->setUserID($userData->id);
    $commandParam->setCommand($command);
    $commandParam->setParam($json_post_data->param);
    $ans->setAnswerArray($commands->$command($commandParam)->getAnsArray());

  } catch (MalformedJSONException $e) {
    $log->error("POST /status Error: " . $e->getMessage());
    $log->error("POST /status Error: " . $e->getTraceAsString());
    ob_start();
    var_dump($e->getTrace()[0]);
    $log->error("POST /status Error: " . ob_get_clean());

    $ans->setFailAction();
    $ans->setReason($e->getMessage());
    $ans->setStatus($e->getCode());
    $db->logAction(-1, $ans->getJson());


  } catch (Exception $e) {
    $log->error("POST /status Error: " . $e->getMessage());
    //$log->error("POST /commands Error: ".$e->getTraceAsString());
    //ob_start();
    //var_dump($e->getTrace()[0]);
    //$log->error("POST /commands Error: " . ob_get_clean());

    $ans->setFailAction();
    $ans->setReason($e->getMessage());
    if($e->getCode() >= 100 && $e->getCode() <= 505 )
      $ans->setStatus($e->getCode());
    else
      $ans->setStatus(500);
  } finally {
    $app->response->setStatus($ans->getStatus());
    $app->response->headers->set('Content-Type', 'application/json');
    $log->debug($ans);
    $db->logAction($userData->id, $ans->getJson());

    echo $ans->getJson();
  }


});

$app->options('/status', function () use ($app, $log) {
  //Return response headers
  $app->response->headers->set('Access-Control-Allow-Methods', 'GET, POST');
  $app->response->headers->set('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept', 'Authorization');
  //$log->debug($app->response->headers);
});

//______________________________________________________________________

$app->get('/runsetup', function () use ($app) {
  //Do something

  echo "Runsetup API";
  //$app->halt(403, 'You shall not pass!');
});
$app->post('/runsetup', function () use ($app, $log) {
  /*
   * check if the token is valid
   * check if the token is issued from the priviledged user
   *
   * issue the command to the tsc
   * wait an answer
   *
   * if the answer is positive send the push notification
   *
   *
   * */
  $ans = new Answer();
  $userData = null;
  try {
    $log->info("POST /runsetup");
    $db = new SQLiteWrapper($log, __DIR__);

    //$log->info($_FILES);

    $json_post_data = $app->request->getBody();

    $json_post_data = json_decode($json_post_data);
    if (json_last_error() != 0) {
      ob_start();
      var_dump($json_post_data);
      $result = ob_get_clean();
      throw new MalformedJSONException($result);
    }

    $authToken = $json_post_data->authToken;
    $command = $json_post_data->command;
    $param = $json_post_data->param;

    $log->debug("POST /runsetup " . $command . ": " . $authToken);
    $log->debug("POST /runsetup param: ");
    $log->debug($param);

    //1° phase check login information

    if (!$db->checkToken($authToken)) {
      throw new TokenExpiredException();
    }

    //2° phase retrieve user information
    $userData = $db->getUserDataFromToken($authToken);
    $log->debug("POST /runsetup userdata:");
    $log->debug($userData);

    // 3° phase check and execute the command
    $runsetup = new RunSetup($log, $db, $userData);
    if (!method_exists($runsetup, $command)) {
      throw new CommandNotFoundException("POST /runsetup commands ->" . $command);
    }

    $ans->setAnswerArray($runsetup->$command($param)->getAnsArray());

    $db->logAction($userData->id, $ans->getJson());

  } catch (MalformedJSONException $e) {
    $log->error("POST /runsetup Error: " . $e->getMessage());
    $log->error("POST /runsetup Error: " . $e->getTraceAsString());
    ob_start();
    var_dump($e->getTrace()[0]);
    $log->error("POST /runsetup Error: " . ob_get_clean());

    $ans->setFailAction();
    $ans->setReason($e->getMessage());
    $ans->setStatus($e->getCode());
    $db->logAction(-1, $ans->getJson());


  } catch (Exception $e) {
    $log->error("POST /runsetup Error: " . $e->getMessage());
    //$log->error("POST /commands Error: ".$e->getTraceAsString());
    //ob_start();
    //var_dump($e->getTrace()[0]);
    //$log->error("POST /commands Error: " . ob_get_clean());

    $ans->setFailAction();
    $ans->setReason($e->getMessage());
    if($e->getCode() >= 100 && $e->getCode() <= 505 )
      $ans->setStatus($e->getCode());
    else
      $ans->setStatus(500);
    $db->logAction(-1, $ans->getJson());

  } finally {
    $app->response->setStatus($ans->getStatus());
    $app->response->headers->set('Content-Type', 'application/json');
    $log->debug($ans);
    echo $ans->getJson();
  }


});


$app->options('/runsetup', function () use ($app, $log) {
  //Return response headers
  $app->response->headers->set('Access-Control-Allow-Methods', 'GET, POST');
  $app->response->headers->set('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept', 'Authorization');
  //$log->debug($app->response->headers);
});

//______________________________________________________________________
/*
 * Commands: amiprivileged|iwouldliketoescalate|imlosingprivilege|authorizeescalation/releaseprivilege
 * 
 * 
 */

$app->get('/escalate', function () use ($app) {
  //Do something

  echo "Escalate API";
  //$app->halt(403, 'You shall not pass!');
});

$app->post('/escalate', function () use ($app, $log) {

  try {
    $log->info("POST /escalate");
    $db = new SQLiteWrapper($log, __DIR__);
    $ans = new Answer();
    $escalate = new Escalation($db, $log);
    $json_post_data = json_decode($app->request->getBody());

    //$json_post_data = json_decode($json_post_data);
    if (json_last_error() != 0) {
      ob_start();
      var_dump($json_post_data);
      $result = ob_get_clean();
      throw new MalformedJSONException($result);
    }
    $command = $json_post_data->command;
    unset($json_post_data->command);
    $authToken = $json_post_data->authToken;
    unset($json_post_data->authToken);

    $params = $json_post_data;

    $log->debug("POST /escalate " . $command . ":" . $authToken);


    //1° phase check login information
    if (!$db->checkToken($authToken)) {
      throw new TokenExpiredException();
    }
    $log->debug("POST /escalate, check token passed");

    //2° phase retrieve username from the token
    $userData = $db->getUserDataFromToken($authToken);
    $log->debug($userData);

    //3° call the right method on the object
    if (method_exists($escalate, $command)) {
      $log->debug("POST /escalate, calling method");

      $ans->setAnswerArray($escalate->$command($userData, $params)->getAnsArray());
      $log->debug("POST /escalate, answering");


    } else {
      throw new CommandNotFoundException("escalate ->" . $command);
    }

    $db->logAction($userData->id, $ans->getJson());

  } catch (MalformedJSONException $e) {
    $log->error("POST /escalate Error: " . $e->getMessage());
    $log->error("POST /escalate Error: " . $e->getTraceAsString());
    //ob_start();
    //var_dump($e->getTrace()[0]);
    //$log->error("POST /escalate Error: " . ob_get_clean());

    $ans->setFailAction();
    $ans->setReason($e->getMessage());
    $ans->setStatus($e->getCode());
    $db->logAction(-1, $ans->getJson());

  } catch (Exception $e) {
    $log->error("POST /escalate Error: " . $e->getMessage());
    //$log->error("POST /escalate Error: ".$e->getTraceAsString());
    //ob_start();
    //var_dump($e->getTrace()[0]);
    //$log->error("POST /escalate Error: " . ob_get_clean());

    $ans->setFailAction();
    $ans->setReason($e->getMessage());
    if($e->getCode() >= 100 && $e->getCode() <= 505 )
      $ans->setStatus($e->getCode());
    else
      $ans->setStatus(500);
    $db->logAction(-1, $ans->getJson());    $db->logAction(-1, $ans->getJson());

  } finally {
    $app->response->setStatus($ans->getStatus());
    $app->response->headers->set('Content-Type', 'application/json');
    $log->debug($ans);

    echo $ans->getJson();
  }
});

$app->options('/escalate', function () use ($app, $log) {
  //Return response headers
  $app->response->headers->set('Access-Control-Allow-Methods', 'GET, POST');
  $app->response->headers->set('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept', 'Authorization');
  //$log->debug($app->response->headers);
});
//______________________________________________________________________


/*
 * Commands: ""
 *
 *
 */

$app->get('/runnumber', function () use ($app, $log) {
  $ans = new Answer();

  try {
    $log->info("GET /runnumber");
    $json_post_data = json_decode($app->request->getBody());

    $runnumber = $db->getRunnumber();

    $log->debug($runnumber->runnumber);

    $runnumber->runnumber++;
    $db->setRunnumber($runnumber->runnumber);
    $ans->addValue("runnumber", $runnumber->runnumber);

  } catch (MalformedJSONException $e) {
    $log->error("GET /runnumber Error: " . $e->getMessage());
    $log->error("GET /runnumber Error: " . $e->getTraceAsString());
    //ob_start();
    //var_dump($e->getTrace()[0]);
    //$log->error("POST /escalate Error: " . ob_get_clean());

    $ans->setFailAction();
    $ans->setReason($e->getMessage());
    $ans->setStatus($e->getCode());
  } catch (Exception $e) {
    $log->error("POST /escalate Error: " . $e->getMessage());
    //$log->error("POST /escalate Error: ".$e->getTraceAsString());
    //ob_start();
    //var_dump($e->getTrace()[0]);
    //$log->error("POST /escalate Error: " . ob_get_clean());

    $ans->setFailAction();
    $ans->setReason($e->getMessage());
    if($e->getCode() >= 100 && $e->getCode() <= 505 )
      $ans->setStatus($e->getCode());
    else
      $ans->setStatus(500);
    $db->logAction(-1, $ans->getJson());
  } finally {
    $app->response->setStatus($ans->getStatus());
    $app->response->headers->set('Content-Type', 'application/json');
    $log->debug($ans);

    echo $ans->getJson();
  }
});

$app->options('/runnumber', function () use ($app, $log) {
  //Return response headers
  $app->response->headers->set('Access-Control-Allow-Methods', 'GET');
  $app->response->headers->set('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept', 'Authorization');
  //$log->debug($app->response->headers);
});
//______________________________________________________________________


$app->get('/result/error/(:message)', function ($message) use ($app, $log) {
  //Do something
  //ob_start();
  //var_dump($json_post_data);
  //$result = ob_get_clean($result);

  $json_data['message'] = "Error performing request " . (!empty($message) ? " - $message" : "");
  $env = $app->environment();


  $log->debug("GET /result/error:'" . $message . "'");
  $app->halt(500, json_encode($json_data));

  //$app->halt(403, 'You shall not pass!');
});

/*
$app->get('/result/badRequest/(:message)', function ($message) use ($app, $log) {
    //Do something
    //ob_start();
	//var_dump($json_post_data);
	//$result = ob_get_clean($result);
	
    $json_data = array('message' => "bad Request");    
    
    $log->error("GET /result/badRequest:'".$message."'");
    $app->halt(400, json_encode($json_data));
});

$app->get('/result/success', function () use ($app) {
    //Do something
});
*/

$app->notFound(function () use ($app, $log) {
  $app->halt(404, '');
});

$app->run();

