<?php


class Answer {

    private $obj = array();
    private $status = NULL;
    function __construct()
    {
      
    }   
    
    public function setOkAction() {
        $this->obj['action'] = "ok";
    }
    public function setFailAction() {
        $this->obj['action'] = "fail";
    }
    public function setReason($reason) {
        $this->obj['message'] = $reason;
    }
    public function setStatus($status) {
        $this->status = $status;
    }
    public function getStatus() {
      return ($this->status === NULL? 200 : $this->status);
    }
    public function setToken($token) {
      $this->obj['token'] = $token;
    }
    public function setRole($role){
      $this->obj['role'] = $role;
    }
    public function setUserName($username) {
      $this->obj['username'] = $username;
    }
    public function setName($name) {
        $this->obj['name'] = $name;
    }
    public function setResult($result) {
        $this->obj['result'] = $result;
    }
    public function setCurrentState($state) {
        $this->obj['currentState'] = $state;
    }
    public function addValue($key, $value) {
      $this->obj[$key] = $value;
    }
    public function setAnswerArray($arr) {
      //array_merge($this->obj, $arr);
      $this->obj = $arr;
    }
    public function getAnsArray() {
      return $this->obj;
    }
    public function setCurrentPriviledgedUserName($username) {
      $this->obj['currentPriviledgedUserName'] = $username;
    }
    public function setCurrentPriviledgedName($name) {
      $this->obj['currentPriviledgedName'] = $name;
    }
    public function setCurrentEscalatingName($name) {
      $this->obj['currentEscalatingName'] = $name;
    }
    public function setPriviledgeExpirationSeconds($seconds) {
      $this->obj['priviledgeWillExpireInSeconds'] = $seconds;
    }
    public function setSecondsToForceEnabling($seconds) {
      $this->obj['secondsToForceEnabling'] = $seconds;
    }
    
    
    
    public function getJson() {
        
        return json_encode($this->obj);
    
    }
    
}
