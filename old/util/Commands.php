<?php

namespace TSCWebServer;

require_once './util/Answers.php';
require_once './util/utils.php';
require_once './util/PushNotification.php';
require_once './util/LocalDaemon.php';

class Commands {

  private $db = NULL;  
  private $log = NULL;  
  private $userdata = NULL;
  private $pushnotification = NULL;
  private $localDaemon = NULL;
  function __construct($db, $log, $userData)
  {
    $this->db = $db;
    $this->log = $log;
    $this->userdata = $userData;
    $this->pushnotification = new PushNotification($this->log);
    try {
      $this->localDaemon = new LocalDaemon($db, $log);
    }
    catch(\Exception $e) {
      $this->log->warn($e->getMessage());
      //$this->log->warn($errno.":".$errstr);
      throw $e;
      

    }
  }   

  //@return Answers istance
  public function init($runsetup) {
    $ans = new Answer();

    // 1° phase retrive user priviledge
       //connect with tsc issue commands
    $currentState = "standby";
    $this->log->debug($this->tscSock);
    
    //2° 
    $ans->setCurrentState($currentState);
    $ans->setOkAction();
    $this->db->setNewStateMachineStatus($currentState);
    
    
    
    $this->log->debug("Commands: Commands->init(".$runsetup."):".$currentState);

    $this->pushnotification->stateMachineChange($currentState);
    $this->log->debug($ans);
    return $ans;
  }
  public function configure($none) {
    $ans = new Answer();
    
    // 1° phase retrive user priviledge
    //connect with tsc issue commands
    $currentState = "ready";
    //2° 
    $ans->setCurrentState($currentState);
    $ans->setOkAction();
    $this->db->setNewStateMachineStatus($currentState);
    
    
    
    $this->log->debug("Commands: Commands->configure(".$none."):".$currentState);

    $this->pushnotification->stateMachineChange($currentState);
    $this->log->debug($ans);
    return $ans;
  }
  public function start($none) {
    $ans = new Answer();
    
    // 1° phase retrive user priviledge
       //connect with tsc issue commands
    $currentState = "running";
    //2° 
    $ans->setCurrentState($currentState);
    $ans->setOkAction();
    $this->db->setNewStateMachineStatus($currentState);
    
    
    
    $this->log->debug("Commands: Commands->start(".$none."):".$currentState);

    $this->pushnotification->stateMachineChange($currentState);
    $this->log->debug($ans);
    return $ans;
  }
  public function stop($none) {
    $ans = new Answer();
    
    // 1° phase retrive user priviledge
       //connect with tsc issue commands
    $currentState = "ready";
    //2° 
    $ans->setCurrentState($currentState);
    $ans->setOkAction();
    $this->db->setNewStateMachineStatus($currentState);
    
    
    
    $this->log->debug("Commands: Commands->stop(".$none."):".$currentState);

    $this->pushnotification->stateMachineChange($currentState);
    $this->log->debug($ans);
    return $ans;
  }
  public function reset($none) {
    $ans = new Answer();
    
    // 1° phase retrive user priviledge
       //connect with tsc issue commands
    $currentState = "idle";
    //2° 
    $ans->setCurrentState($currentState);
    $ans->setOkAction();
    $this->db->setNewStateMachineStatus($currentState);
    
    
    
    $this->log->debug("Commands: Commands->reset(".$none."):".$currentState);

    $this->pushnotification->stateMachineChange($currentState);
    $this->log->debug($ans);
    return $ans;
  }
  public function userCanIssueCommands() {
    $priviledgeUser = $this->db->getPriviledgedUserData();
    return !$priviledgeUser->UserEmpty
            &&
            $this->userdata->username == $priviledgeUser->username;
          
  }

}
