<?php

###############################################################################
##
##  Copyright (C) 2014, Tavendo GmbH and/or collaborators. All rights reserved.
##
##  Redistribution and use in source and binary forms, with or without
##  modification, are permitted provided that the following conditions are met:
##
##  1. Redistributions of source code must retain the above copyright notice,
##     this list of conditions and the following disclaimer.
##
##  2. Redistributions in binary form must reproduce the above copyright notice,
##     this list of conditions and the following disclaimer in the documentation
##     and/or other materials provided with the distribution.
##
##  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
##  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
##  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
##  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
##  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
##  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
##  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
##  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
##  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
##  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
##  POSSIBILITY OF SUCH DAMAGE.
##
###############################################################################

require __DIR__ . '/../vendor/autoload.php';

echo __DIR__;

require_once 'PushNotification.php';
require_once 'sqlitelib.php';



use Psr\Log\NullLogger;
use Thruway\ClientSession;
use Thruway\Connection;
use Thruway\Logging\Logger;


//Uncomment to disable logging
//Logger::set(new NullLogger());
$counter = 0;
$log = new \Slim\Extras\Log\DateTimeFileWriter(
        array(
          'path' => 'logs/',
          'name_format' => 'Y-m-d',
          'message_format' => '%label% - %date% - %message%'
        )
      );
$timer      = null;
$loop       = React\EventLoop\Factory::create();
$connection = new Connection(
    [
        "realm" => 'realm1',
        "url"   => 'ws://127.0.0.1:8080/'
    ],
    $loop
);
//com.tridas.statemachine.priviledged.change

$loopFunction = function () use (&$counter) {
    echo "sta funzionando! " . $counter++;
    //$timer = $loop->addPeriodicTimer(1, $loopFunction);
};

$connection->on('open', function (ClientSession $session) use ($connection, $loop, &$timer) {

  // SUBSCRIBE to a topic and receive events
  $onHello = function ($args) {
      echo "event for 'com.tridas.statemachine.priviledged.change' received: " . print_r($args[0]) . "\n";
  };
  $session->subscribe('com.tridas.statemachine.priviledged.change', $onHello);
  echo "subscribed to topic 'com.tridas.statemachine.priviledged.change'";
  
  
  
});



//$timer = $loop->addPeriodicTimer(1, $loopFunction);

//$loop->run();
$connection->open();
