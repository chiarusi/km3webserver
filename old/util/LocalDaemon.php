<?php
require_once './Exceptions/exceptions.php';


class LocalDaemon {
  
  private $sock_path = '/tmp/tsc.sock';
  private $sock = NULL;
  private $db = NULL;
  private $log = NULL;
  private $parser = NULL;
  private $preconnectionValue = NULL;
  private $postCommandValue = NULL;
  
  
  function __construct($db = NULL, $log = NULL )
  {
    $this->db = $db;
    $this->log = $log;
    $this->parser = new LocalDaemonParser($log);
    
  }   
  public function issueCommand($command, $param = array()) {
      $this->open_socket();
      $command = $this->prepareCommand($command, $param);
      socket_write($this->sock, $command, strlen($command));
      $log->log->debug ("command issued: '" .$command . "'");
      $log->log->debug (" writing result:" . socket_strerror(socket_last_error()) );;
      $this->postCommandValue = $this->parser($this->readline());
      $this->close_socket();
  }
  
  private function open_socket()
  {
    $this->sock = socket_create(AF_UNIX, SOCK_STREAM, 0);
    if ($this->sock === false) {
      throw new LocalDaemonNotFoundException( socket_strerror(socket_last_error()));
    }
    else {
      if(!socket_connect($this->sock, $this->sock_path)){
        throw new LocalDaemonNotFoundException(socket_strerror(socket_last_error()));
      }
      else {
        $log->log->debug ("read the answer line at initial connetion with tsc:");;
        $this->preconnectionValue=$this->parser($this->readline());
        //$this->log->info("LocalDaemon socket opened succesfully");
      }
    }
  }
  
  public function checkDaemon() {
    $this->open_socket();
    $this->close_socket();
    return true;
  }
  
  private function prepareCommand($command, $param) {
    return $command . " " . implode(" ", $param) . "\n\r";
  }
  
  private function close_socket() {
    socket_close($this->sock);
  }
  
  private function readline() {
    $data = "";
    $log->log->debug ("reading from socket...");
    $data = socket_read($this->sock, 8192, PHP_NORMAL_READ);
    $log->log->debug ("data read: '" .  str_replace(PHP_EOL, '', $data) . "'");
    return $data;
  }
  
}
