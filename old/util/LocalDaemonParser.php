<?php
require_once './Exceptions/exceptions.php';


class LocalDaemonParser {
  
  /*  Possible output:
   * O idle TSV: HM: TCPU: EM: CH: <--- AT FIRST CONNECT ALWASY "STATE"
   * O standby RUNSETUP:/home/favaro/Develop/tridas/datacards/configuration.json
   * O standby TSV: HM: TCPU: EM: CH:
   * X standby
   * O ready TSV:0/1 HM:18446744073709551615/1 TCPU:18446744073709551615/2 EM:0/1 CH:18446744073709551615/1
   * O ready RUNSETUP:/home/favaro/Develop/tridas/datacards/configuration.json
   * O ready RUNDATACARD:/tmp//tridas_configuration.json_664343.json NODEMAP:5
   * O running TSV:18446744073709551615/1 HM:18446744073709551615/1 TCPU:18446744073709551615/2
   * 
   * */
  
  private $log = NULL;
  
  function __construct($log = NULL )
  {
    $this->db = $db;
    $this->log = $log;
  }
  
  public function parse($string) {
    $this->log->debug("LocalDaemonParser->parse(" . $string . ")");
    
    $remaining = $string;
    $ret = array();
    
    $succesful = $this->readSpaceSeparatedVar($remaining);
    $evaluating = strtoupper($succesful[0]);
    $this->log->debug("LocalDaemonParser->parse: evaluating" . $evaluating);
    $remaining = $pieces[1];
    $this->log->debug("LocalDaemonParser->parse: remaining" . $remaining);
    $tryCalling = "read". $evaluating;
    
    try {
      $values = $this->$tryCalling($evaluating);
      $ret[$values[0]] = $values[1];
    }
    catch(Exception $e) {
      throw new ParsingException("the daemon output isn't conforming to standard");
    }
    
    $state = $this->readSpaceSeparatedVar($remaining);
    $this->log->debug("LocalDaemonParser->parse: remaining" . $remaining);
    
    $ret['state'] = $state[0];
    
    try {
      $values = $this->readStateInformation($state[1]);
      foreach ($array as $key => $value) {
        $ret[$key] = $value;
      }
    }
    catch(Exception $e) {
      throw new ParsingException("the daemon output isn't conforming to standard");
    }
    $this->log->debug($ret);
    return $ret
  }
  
  //idle| .. | .. 
  private function readStateInformation($informationString)
  {
    $values = explode(" ", $informationString);
    $ret = array();
    
    foreach ($values as $value) {
      
      try {
        $keyValue = $this->readColonSeparatedVar($value);
        $ret[$keyValue[0]] = $keyValue[1];
      }
      catch(Exception $e) {
        throw new ParsingException("the daemon output isn't conforming to standard");
      }
    
      
    }
    return $ret;
  }

  private function readX($Xstring) {
    return array("successful", false);
  }
  
  private function readO($Ostring) {
    return array("successful", true);

  }  

/////////////////////////////////////////////7777
  
  private function readColonSeparatedVar($colonseparatedString) {
  return  explode(":", $spaceseparatedString, 1);
  }
  private function readSpaceSeparatedVar($spaceseparatedString) {
    return  explode(" ", $spaceseparatedString, 1);
  }
  
  private function readSlashSeparatedVar($slashseparatedString) {
    
  }
  
}
