<?php

require_once './util/Answers.php';


class PushNotification {

  private $secret = "kkjH68GiuUZ";
  private $key = "test";
  private $websocketServerAddress = "http://127.0.0.1:5555";
  protected $log;

  function __construct($log)
  {
    $this->log = $log;
  }   
  
  public function stateMachineChange($state) {
    $this->curlPost($this->getStatemachineStateChangeBody($state));
  }
  
  public function escalationInitiated($currentPriviledgeUserData, $escalatingUserData ){
    $this->curlPost($this->getEscalationProcedureInitiatedBody($currentPriviledgeUserData, $escalatingUserData));
  }
  
  public function escalationForced($token){
    
  }
  
  public function escalationDenied($token) {
    
  }
  
  public function escalationAccepted($token) {
  }
  
  public function newPriviledgedUser($newPriviledgedUserData) {
    $this->curlPost($this->getNewPriviledgeUserBody($newPriviledgedUserData));
  }
  
  public function youHasBeenKickedOut($token) {
    
  }
  
  //__________________________________________________________________//
  
  private function curlPost($body) {
    //set the url, number of POST vars, POST datai
    $this->log->debug($body);
    $websocketserverpath = $this->getPath($this->getParam(), $body);
    //echo "\ncurl -H \"Content-Type: application/json\" -d '" . json_encode($body) . "' \"". $websocketserverpath . "\"\n\n";
    
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $websocketserverpath);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));
    curl_setopt($ch, CURLOPT_POST, count($body));
    curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HEADER, 1);

    $responce = curl_exec($ch);
    $result = array( 'header' => '', 
                             'body' => '', 
                             'curl_error' => '', 
                             'http_code' => '',
                             'last_url' => '');
    $output=$responce;

    //var_dump($output);

    $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
    $header=substr($responce, 0, $header_size);
    $body = substr( $responce, $header_size, strlen($responce) );
    //storing result
    $result['header'] = $header;
    $result['http_code'] = curl_getinfo($ch,CURLINFO_HTTP_CODE);
    $result['last_url'] = curl_getinfo($ch,CURLINFO_EFFECTIVE_URL);

    if($result['http_code'] >= 500 ) {
      $result['body'] = array(  'action' => 'failed',
                                'reason' => 'remote services has replied with ' . $result['http_code'] . ' Error\n contact sysadmin',
                                'remote_url' => $result['last_url']
                              );
    }
    else $result['body'] = $body;

    //var_dump($result);
    $this->log->debug($result);
    //close connection
    curl_close($ch);
  
  }
  
  private function getParam() {
    
    $param = array();
    
    $param['timestamp'] = $this->getUTCTimestamp();
    $param['seq'] = 1; // on multiple push notification this number must increase
    $param['key'] = $this->key;
    $param['nonce'] = rand ( 0 , 2^53 );
    return $param;
  }
  
  private function getUTCTimestamp() {
  
    $t = microtime(true);
    $micro = sprintf("%06d",($t - floor($t)) * 1000000);
    $timestamp= new DateTime(date('Y-m-d H:i:s.'.$micro,$t));
    $timestamp->setTimeZone(new DateTimeZone("UTC"));
    $formatedtimestamp = $timestamp->format('Y-m-d\TH:i:s.u\Z');
    //$formatedtimestamp ="2015-04-01T14:09:47.960279Z";
    return $formatedtimestamp;
  }

  private function getStatemachineStateChangeBody($state) {
    $body=array();
    $body['topic'] = 'com.tridas.statemachine.statechange';
    $body['args'] = array();
    $argument=array();
    
    $argument[] = array('state'=> $state);
    $body['args']= $argument;

    
    return utf8_encode(json_encode($body));
  
  }
  
   private function getNewPriviledgeUserBody($newPriviledgedUSerData) {
    $body=array();
    $argumentElements = new Answer();
    
    $body['topic'] = 'com.tridas.statemachine.priviledged.change';
    $body['args'] = array();
    $argument=array();
    
    $argumentElements->setCurrentPriviledgedName($newPriviledgedUSerData->name);
    $argumentElements->setCurrentPriviledgedUserName($newPriviledgedUSerData->username);
    $argumentElements->setPriviledgeExpirationSeconds($newPriviledgedUSerData->priviledge_expiration - time());
    $argumentElements->setCurrentEscalatingName("");
    
    
    $argument[] = $argumentElements->getAnsArray();
    
    $body['args']= $argument;

    
    return utf8_encode(json_encode($body));

  
  }
  
  private function getEscalationProcedureInitiatedBody($currentPriviledgeUserData, $escalatingUserData) {
    //com.tridas.statemachine.escalation.' + token

    $body=array();
    $argumentElements = new Answer();
    
    $body['topic'] = 'com.tridas.statemachine.escalation.' . $currentPriviledgeUserData->token;
    $body['args'] = array();
    $argument=array();
    
    //$argumentElements->setCurrentPriviledgedName($newPriviledgedUSerData->name);
    //$argumentElements->setCurrentPriviledgedUserName($newPriviledgedUSerData->username);
    // ADD TO ANSWER the autogranting field 
    $argumentElements->setPriviledgeExpirationSeconds($escalatingUserData->priviledge_expiration - time());
    $argumentElements->setCurrentEscalatingName($escalatingUserData->name);
    
    $argument[] = $argumentElements->getAnsArray();
    
    $body['args']= $argument;

    
    return utf8_encode(json_encode($body));
  }
  
  
  private function getPath($param, $bodyJsoned) {
    
    $param['signature'] = $this->signatureCalc($param, $bodyJsoned);
    $this->log->debug($param);
    $path = $this->websocketServerAddress . "?" . http_build_query ($param );

    return $path;
  }
  
  private function signatureCalc($param, $bodyJsoned) {
    $this->log->debug(json_encode($param));
    /*
    $ctx = hash_init('sha256', HASH_HMAC, $this->secret);
    $signature = hash_final($ctx);
    $this->log->debug( "1-->" . $signature );
    
    
     
    $ctx = hash_init('sha256', HASH_HMAC, $this->secret);
    hash_update($ctx, $this->key);
    $signature = hash_final($ctx);
    $this->log->debug( "2-->" . $signature );
    
    
    $ctx = hash_init('sha256', HASH_HMAC, $this->secret);
    hash_update($ctx, $this->key);
    hash_update($ctx, $param['timestamp']);
    $signature = hash_final($ctx);
    $this->log->debug( "3-->" . $signature );
    
    $ctx = hash_init('sha256', HASH_HMAC, $this->secret);
    hash_update($ctx, $this->key);
    hash_update($ctx, $param['timestamp']);
    hash_update($ctx, $param['seq']);
    $signature = hash_final($ctx);
    $this->log->debug( "4-->" . $signature );
    
    
    $ctx = hash_init('sha256', HASH_HMAC, $this->secret);
    hash_update($ctx, $this->key);
    hash_update($ctx, $param['timestamp']);
    hash_update($ctx, $param['seq']);
    hash_update($ctx, $param['nonce']);
    $signature = hash_final($ctx);
    $this->log->debug( "5-->" . $signature );
    
    
    $ctx = hash_init('sha256', HASH_HMAC, $this->secret);
    hash_update($ctx, $this->key);
    hash_update($ctx, $param['timestamp']);
    hash_update($ctx, $param['seq']);
    hash_update($ctx, $param['nonce']);
    hash_update($ctx, $bodyJsoned);
    $signature = hash_final($ctx);
    $this->log->debug( "final-->" . $signature );
    */
    $ctx = hash_init('sha256', HASH_HMAC, $this->secret);
    hash_update($ctx, $this->key);
    hash_update($ctx, $param['timestamp']);
    hash_update($ctx, $param['seq']);
    hash_update($ctx, $param['nonce']);
    hash_update($ctx, $bodyJsoned);

    $signature = hash_final($ctx, true);
    $encodedsign = base64_encode($signature);
    $encodedsign = str_replace(array('+','/'),array('-','_',''),$encodedsign);
    return $encodedsign;
  }
  
  
}
