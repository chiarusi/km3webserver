<?php

require_once './util/Answers.php';
require_once './util/utils.php';
require_once './util/PushNotification.php';
require_once './util/LocalDaemon.php';

class Status {

  private $db = NULL;  
  private $log = NULL;  
  private $userdata = NULL;
  private $pushnotification = NULL;
  private $tscSock = NULL;
  
  function __construct($db, $log, $userData)
  {
    $this->db = $db;
    $this->log = $log;
    $this->userdata = $userData;
    $this->pushnotification = new PushNotification($this->log);
    try {
      $this->tscSock = new LocalDaemon($db, $log);
    }
    catch(\Exception $e) {
      $this->log->warn($e->getMessage());
      //$this->log->warn($errno.":".$errstr);
      throw $e;
      

    }
  }   

  //@return Answers istance
  public function statemachine($param) {
    $ans = new Answer();
        
    
    // 1° phase retrive user priviledge
       //connect with tsc issue commands
    $currentState = "standby";
    $this->log->debug($this->tscSock);
    
    //2° 
    $ans->setCurrentState($currentState);
    $ans->setOkAction();
    $this->db->setNewStateMachineStatus($currentState);
    
    
    
    $this->log->debug("Commands: Commands->init(".$runsetup."):".$currentState);

    $this->pushnotification->stateMachineChange($currentState);
    $this->log->debug($ans);
    return $ans;
  }
  
  

}
