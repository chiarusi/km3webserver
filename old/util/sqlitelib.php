<?php 
require_once './util/utils.php';


class SQLiteWrapper
{
  private $defaultSessionDuration = 60 * 60 * 24 * 30; //default duration = 60 * 60 * 24 * 30 = 2592000 ~1 month
  private $defaultStateHistory = 30; //default duration = 30 sec
  private $defaultPriviledgeDurantion = 60 ;//* 15; //default duration = 15 min
  private $forceEnablingTime = 60; // 60 sec -> 1 min
  private $autoPriviledgeGrantingTimeout = 60 * 2; // 120 sec -> 3 min from the timerequest
  
  protected $headers = array();

  protected $config;
  
  protected $log;

  protected $fpdo; // PDO

  public function __construct($pdo, $log) {
      if (! $pdo ) {
        throw new exception("PDO NULL");
      }
      $this->fpdo = new FluentPDO($pdo);
      $this->log = $log;
  }
  //TRUE/FALSE
  public function matches($username, $password) {
    try {
      $this->log->debug("SQLiteWrapper.matches:" . $username);

      $password = hash("sha256", $password);
      $this->log->debug("hashed password->".$password);
      $query = $this->fpdo
                          ->from('users')
                          ->where(array ('username' => $username,
                                        'password'  => $password
                                          )
                                  )
                          ->select('COUNT(*) as count');
      $result = $query->fetch();
      $this->log->debug("SQLiteWrapper.matches count:" . $result['count']);

      return $result['count'] == 1;
    }
     catch (Exception $e) {
      $this->log->critical("SQLiteWrapper.matches:".$e->getMessage());
      throw $e;
    }    
  }
  
  //TRUE/FALSE
  public function checkUserAlreadyLogged($username) {
    try {
      $this->log->debug("checkUserAlreadyLogged:" . $username);

      $count = $this->fpdo->from('SESSIONS')
                          ->innerJoin('USERS on USERS.id = user_id')
                          ->where('USERS.username', $username)
                          ->select('COUNT(*) as count');

      //$this->log->debug($count->getQuery(true));
      //$this->log->debug(var_dump($count));
      //$this->log->debug(var_dump($count->getParameters()));
      //$this->log->debug(var_dump($count->getResult()));
      $result = $count->fetch();
      //$this->log->debug(var_dump($result));
      $this->log->debug("SQLiteWrapper.checkUserAlreadyLogged count:" . $result['count']);
      
      return $result['count'] >= 1;
    }
     catch (Exception $e) {
      $this->log->critical("SQLiteWrapper.checkUserAlreadyLogged:".$e->getMessage());
      throw $e;
    }    
  }
  
  public function resetUserSession($username) {
    try {
      $deleteQuery = $this->fpdo->deleteFrom('SESSIONS')
                          ->where('user_id', $this->getUserID($username)->id);
      //$this->dumpToLog($deleteQuery->getQuery(true));             
      $deleteQuery->execute();  
     }
     catch (Exception $e) {
      $this->log->critical("SQLiteWrapper.resetUserSession: ".$e->getMessage());
      throw $e;
    } 
  }
  
  public function getRole($username) {
    try {
      $role = $this->fpdo->from('ROLES')
                         ->innerJoin('USERS on USERS.user_role = ROLES.id')
                         ->where('USERS.username', $username)
                         ->select(array('ROLES.*'));
      //$this->dumpToLog($role->getQuery(true));                   
                         
      $result = $role->fetch();
      return $result['role_name'];
    
    }
     catch (Exception $e) {
      $this->log->critical("getRole:".$e->getMessage());
      throw $e;
    }    
    
    
  }
  
  public function getUserData($username) {
    try {
      $userDataQ = $this->fpdo->from('USERS')
                         ->innerJoin('ROLES on USERS.user_role = ROLES.id')
                         ->leftJoin('SESSIONS on SESSIONS.user_id = USERS.id')
                         ->where('USERS.username', $username)
                         ->select(array('ROLES.*', 'SESSIONS.*'));
      $this->log->debug('getUserData');                   
      $this->dumpToLog($userDataQ->getQuery(true));                   
      $this->log->debug('getUserData username:'.$username);                   
          
      $result = $userDataQ->fetch();
      if($userDataQ->count() == 0)
        return $userDataQ->createEmptyUserData();
      else {
        $result['UserEmpty'] = false;
        return $this->getObject($result);
      }
    }
     catch (Exception $e) {
      $this->log->critical("SQLiteWrapper.getUserData:".$e->getMessage());
      throw $e;
    }
  }
  
   
  public function getPriviledgedUserData() {
   try {
     //CREATE FILE USING CROSSBAR DEAEOMN FOR AUTO GRANTING Priviledge
     // SEE HELLO EXAMPLE http://crossbar.io/docs/Getting-started-with-PHP/
      $priviledgedQ = $this->fpdo->from('PRIVILEDGE')
                           ->innerJoin('USERS on USERS.id = PRIVILEDGE.user_id')
                           ->innerJoin('ROLES on USERS.user_role = ROLES.id')
                           ->innerJoin('SESSIONS on SESSIONS.user_id = USERS.id')
                           ->where('PRIVILEDGE.priviledge_expiration > :now_timestamp OR ROLES.priviledge_will_expire = :will_expire', array(':now_timestamp' => time(), ':will_expire' => 0))
                           ->select(array('USERS.*', 'ROLES.*', 'SESSIONS.*'))
                           ->limit(1);
      $this->log->debug("SQLiteWrapper.getPriviledgedUserData:");
      $this->dumpToLog($priviledgedQ->getQuery(true));                   
      
      $result = $priviledgedQ->fetch();
      if($priviledgedQ->count() == 0)
        return $this->createEmptyUserData();
      else {
        $result['UserEmpty'] = false;
        return $this->getObject($result);
      }
    }
     catch (Exception $e) {
      $this->log->critical("SQLiteWrapper.getPriviledgedUserData:".$e->getMessage());
      throw $e;
    }
  }
  
  public function getEscalatingUserData() {
     try {
      $this->log->debug("SQLiteWrapper.getEscalatingUserData()");

      $usernameQ = $this->fpdo->from('ESCALATION')
                              ->innerJoin('USERS on ESCALATION.user_id = USERS.id')
                              ->innerJoin('SESSIONS on SESSIONS.user_id = USERS.id')
                              ->innerJoin('ROLES on USERS.user_role = ROLES.id')
                              ->select(array('USERS.*', 'ROLES.*','SESSIONS.*'))
                              ->limit(1);
      
      $this->dumpToLog($usernameQ->getQuery(true));
      
      $result = $usernameQ->fetch();
      if($usernameQ->count() == 0)
        return $this->createEmptyUserData();
      else{
        $result['UserEmpty'] = false;
        return $this->getObject($result);
      }
    }
     catch (Exception $e) {
      $this->log->critical("SQLiteWrapper.getEscalatingUserData:".$e->getMessage());
      throw $e;
    }
  }
  
  public function getUserDataFromToken($token) {
    try {
      $userdataQ = $this->fpdo->from('USERS')
                              ->innerJoin('SESSIONS on SESSIONS.user_id = USERS.id')
                              ->innerJoin('ROLES on USERS.user_role = ROLES.id')
                              ->where('SESSIONS.token', $token)
                              ->select(array('ROLES.*', 'SESSIONS.*' ))
                              ->limit(1);
      $this->log->debug('getUserDataFromToken');
      $this->dumpToLog($userdataQ->getQuery(true));                   
      $this->log->debug('getUserDataFromToken token:' . $token);

      $result = $userdataQ->fetch();

      if($userdataQ->count() == 0)
        return $this->createEmptyUserData();
      else 
        return $this->getObject($result);
    }
    catch (Exception $e) {
      $this->log->critical("SQLiteWrapper.getUserDataFromToken:".$e->getMessage());
      throw $e;
    }
    
  }
  
  public function createEmptyUserData() {
    $empty = new stdClass;
    
    $empty->id = -1;
    $empty->name = "";
    $empty->username = "";
    $empty->password = "";
    $empty->user_role = -1;
    $empty->user_can_escalate = 0;
    $empty->can_login = 0;
    $empty->role_name = "";
    $empty->role_can_escalate = 0;
    $empty->priviledge_will_expire = 1;
    $empty->is_god_like = 0;
    $empty->can_edit_user = 0;
    $empty->session_will_expire = 1;
    $empty->priviledge_expiration = 0;
    $empty->request_time = 0;
    $empty->priviledge_grant_time = 0;
    $empty->force_enabling_time = 0;
    $empty->token = "";
    $empty->expire = -1;
    $empty->UserEmpty = true;
    
    return $empty;
    
  }
  
  public function createAuthToken($username) {
    try {
      $token = md5(uniqid(mt_rand(), true));
      
      
      $userID = $this->getUserID($username)->id;
      
      $values = array (
                  'user_id' => $userID,
                  'token'   => $token,
                  'expire'  => time() + $this->defaultSessionDuration
                );
                
      $insertQ = $this->fpdo->insertInto('SESSIONS')
                      ->values($values);
      $insertQ->execute();
      $this->log->debug('createAuthToken');
      $this->dumpToLog($insertQ->getQuery(true));                   
      $this->log->debug('createAuthToken token:' . $token);
      return $token;
    }
    catch (Exception $e) {
      $this->log->critical("SQLiteWrapper.createAuthToken:".$e->getMessage());
      throw $e;
    }    
    
  }
   
  public function renewSession($token) {
    try {
      $set = array('expire' => time() + $this->defaultSessionDuration);
      $sessionQ = $this->fpdo->update('SESSIONS')->set($set)
                             ->where('token', $token);
    }
    catch (Exception $e) {
      $this->log->critical("SQLiteWrapper.renewSession:".$e->getMessage());
      throw $e;
    }    
  }
  
  public function checkToken($token) {
    try {
      
      $sessionQ = $this->fpdo->from('SESSIONS')
                            ->where('token', $token)
                            ->select(array('COUNT(*) as count'));
      $result = $sessionQ->fetch();
      
      
      if ($result['count'] == 1 ) {
        if(!$this->getRoleFromToken($token)->session_will_expire) {
          $this->log->debug("SQLiteWrapper.checkToken: datamanager is logging in");
          return true;
        }
        else {
         $now = time();
         $this->log->debug("SQLiteWrapper.checkToken: Token Expired?:" . $result['expire'] . " >= " . $now);
         return $result['expire'] >= $now;
       }
      }
      return false;
  
    }
    catch (Exception $e) {
      $this->log->critical("SQLiteWrapper.checkToken Exception:".$e->getMessage());
      throw $e;
    }    
  }
  
  //TRUE/FALSE
  public function tokenExist($token) {
    try {
      
      $sessionQ = $this->fpdo->from('SESSIONS')
                            ->where('token', $token)
                            ->select(array('COUNT(*) as count'));
      $result = $sessionQ->fetch();
      
      return $result['count'] == 1;
  
    }
    catch (Exception $e) {
      $this->log->critical("SQLiteWrapper.tokenExist:".$e->getMessage());
      throw $e;
    }
  }
  
  public function invalidateToken($userData, $token){
    try {
      $sessionQ = $this->fpdo->deleteFrom('SESSIONS')
                            ->where(array("token" => $token, 
                                          "user_id" => $userData->id));
      $sessionQ->execute();
      $this->dumpToLog($sessionQ->getQuery(true));                   
      //$result = $sessionQ->fetch();
      return true;
    }
    catch (Exception $e) {
      $this->log->critical("SQLiteWrapper.invalidateToken:".$e->getMessage());
      throw $e;
    }
  }
  
  public function getRoleFromToken($token) {
    try {
      
      $roleQ = $this->fpdo->from('ROLES')
                          ->innerJoin('USERS on USERS.user_role = ROLES.id')
                          ->innerJoin('SESSIONS on USERS.id = SESSIONS.user_id')
                          ->where('SESSIONS.token', $token)
                          ->limit(1)
                          ->select(array('ROLES.*'));
      $result = $roleQ->fetch();
      return $this->getObject($result);
    }
    catch (Exception $e) {
      $this->log->critical("SQLiteWrapper.getRoleFromToken:".$e->getMessage());
      throw $e;
    }    
    
    
  }

  public function getTokenFromUsername($username) {
    try {
      $tokenQ = $this->fpdo->from('SESSIONS')
                              ->innerJoin('USERS on SESSIONS.user_id = USERS.id')
                              ->where('USERS.username', $username)
                              ->limit(1)
                              ->select(array('SESSIONS.token'));
      $result = $tokenQ->fetch();
        
      return $this->getObject($result);
    }
    catch (Exception $e) {
      $this->log->critical("SQLiteWrapper.getTokenFromUsername:".$e->getMessage());
      throw $e;
    }
      
  }
  
  public function setNewStateMachineStatus($newState) {
    try {
      $values = array('state_name' => $newState,
                   'timestamp_entering_state' => time()
                  );
      $newStateQ = $this->fpdo->insertInto('STATE_MACHINE')
                              ->values($values);
      $newStateQ->execute();
    }
     catch (Exception $e) {
      $this->log->critical("SQLiteWrapper.setNewStateMachineStatus:".$e->getMessage());
      throw $e;
    }

  }
  
  //TRUE/FALSE
  public function priledgeUser($username) {
    try {
      $userID = $this->getUserID($username)->id;
      $basePDO = $this->fpdo->getPdo();
        try {
          $basePDO->beginTransaction();
          $values = array (
                'user_id' => $userID,
                'priviledge_expiration'   => time() + $this->defaultPriviledgeDurantion ,
                'no_answer_count' => 0

              );
          $deletepriviledgeQ = $this->fpdo->deleteFrom('PRIVILEDGE');
          $deletepriviledgeQ->execute();
          $insertQ = $this->fpdo->insertInto('PRIVILEDGE')
                                 ->values($values);
          $insertQ->execute();
          $basePDO->commit();
          return true;
        
        } catch ( Exception $e ) { 
          $basePDO->rollBack();
          $this->log->critical("SQLiteWrapper.priviledgeAdmin ROLLBACK:".$e->getMessage());
          throw $e;
        }
      }
    catch (Exception $e) {
      $this->log->critical("SQLiteWrapper.priledgeUser:".$e->getMessage());
      return false;
    }
  }
  
  //TRUE/FALSE
  public function priviledgeAdmin($username) {
    try {
       
      $userID = $this->getUserID($username)->id;
      $basePDO = $this->fpdo->getPdo();

      try {
        $basePDO->beginTransaction();
        
        $values = array (
                'user_id' => $userID,
                'priviledge_expiration'   => time() + $this->defaultPriviledgeDurantion ,
                'no_answer_count' => 0

              );
        $deletepriviledgeQ = $this->fpdo->deleteFrom('PRIVILEDGE');
        $deletepriviledgeQ->execute();
        $this->log->debug("priviledgeAdmin");
        $this->dumpToLog($deletepriviledgeQ->getQuery(true));  
        $deleteescalationQ = $this->fpdo->deleteFrom('ESCALATION');
        $deleteescalationQ->execute();
        
        $this->dumpToLog($deleteescalationQ->getQuery(true));  

        $insertQ = $this->fpdo->insertInto('PRIVILEDGE')
                    ->values($values);
        $insertQ->execute();
        $basePDO->commit();
        return true;
        
      } catch ( Exception $e ) { 
          $basePDO->rollBack();
          $this->log->critical("SQLiteWrapper.priviledgeAdmin: ROLLBACK".$e->getMessage());
          throw $e;

      } 
    }
    catch (Exception $e) {
      $this->log->critical("SQLiteWrapper.priviledgeAdmin:".$e->getMessage());
      return false;
    }
  }
  
  public function startEscalation($username) {
     try {
       
      $userID = $this->getUserID($username)->id;
      $basePDO = $this->fpdo->getPdo();

      try {
        $basePDO->beginTransaction();
        
        $values = array (
                'user_id' => $userID,
                'request_time'   => time(),
                'priviledge_grant_time' => time() + $this->autoPriviledgeGrantingTimeout,
                'force_enabling_time' => time() + $this->forceEnablingTime,
              );
        $this->log->debug("SQLiteWrapper.startEscalation");
        $deleteescalationQ = $this->fpdo->deleteFrom('ESCALATION');
        $deleteescalationQ->execute();
        $this->dumpToLog($deleteescalationQ->getQuery(true));  
        $insertQ = $this->fpdo->insertInto('ESCALATION')
                    ->values($values);
        $insertQ->execute();
        $basePDO->commit();
        return true;
        
      } catch ( Exception $e ) { 
          $basePDO->rollBack();
          $this->log->critical("SQLiteWrapper.startEscalation: ROLLBACK".$e->getMessage());
          throw $e;

      } 
    }
    catch (Exception $e) {
      $this->log->critical("SQLiteWrapper.startEscalation:".$e->getMessage());
      return false;
    }
  }
  
  private function dumpToLog($var) {
    ob_start();
    var_dump($var);
    $this->log->debug(ob_get_clean());
  }
  
  private function getUserID($username) {
    try {
      $userQ = $this->fpdo->from('USERS')
                    ->where('USERS.username', $username);
      $result = $userQ->fetch();
      return $this->getObject($result);
    }
     catch (Exception $e) {
      $this->log->critical("SQLiteWrapper.getUserID:".$e->getMessage());
      throw $e;
    }
  }
  
  private function getObject($queryResult) {
    return json_decode(json_encode($queryResult), FALSE);
  }
  public function dummy($var) {
    try {
    }
     catch (Exception $e) {
      $this->log->critical("SQLiteWrapper.dummy:".$e->getMessage());
      throw $e;
    }
  }
  
  
}
