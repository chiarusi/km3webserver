<?php


function functionallyEmpty($o)
{
  if (empty($o)) return true;
  else if (is_numeric($o)) return false;
  else if (is_string($o)) return !strlen(trim($o)); 
  else if (is_object($o)) return functionallyEmpty((array)$o);
 
  // It's an array!
  foreach($o as $element) 
    if (functionallyEmpty($element)) continue; // so far so good.
    else return false; 
    
  // all good.
  return true;
}
