<?php

namespace TSCWebServer;
class CantEscalateException extends \Exception
{
    // Redefine the exception so message isn't optional
    public function __construct($message = null, $code = 1, Exception $previous = null) {
    
        $message = "This User can't escalate: " . $message;
        parent::__construct($message, $code, $previous);
    }

    // custom string representation of object
    public function __toString() {
        return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
    }
}
