<?php

namespace TSCWebServer;
class CantWriteToFileException extends \Exception
{
    // Redefine the exception so message isn't optional
    public function __construct($message = null, $code = 500, Exception $previous = null) {
    
        $message = "Can't write to filesystem " . $message;
        parent::__construct($message, $code, $previous);
    }

    // custom string representation of object
    public function __toString() {
        return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
    }
}
