<?php

namespace TSCWebServer;
class CommandNotFoundException extends \Exception
{
    // Redefine the exception so message isn't optional
    public function __construct($message = null, $code = 404, Exception $previous = null) {
    
        $message = "The request command not exist " . $message;
        parent::__construct($message, $code, $previous);
    }

    // custom string representation of object
    public function __toString() {
        return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
    }

}
