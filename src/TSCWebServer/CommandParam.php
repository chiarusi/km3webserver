<?php
/**
 * Created by PhpStorm.
 * User: favaro
 * Date: 20/11/15
 * Time: 10.20
 */

namespace TSCWebServer;


class CommandParam
{
  private $param = array();
  private $userID = -1;
  private $command = "";

  function __construct()
  {

  }

  function setParam($string)
  {
    array_push($this->param, $string);
  }

  function getParam()
  {
    return implode(" ", $this->param);
  }

  function setUserID($userID)
  {
    $this->userID = $userID;
  }

  function getUserID()
  {
    return $this->userID;
  }

  function setCommand($command)
  {
    $this->command = $command;
  }

  function getCommand()
  {
    return $this->command;
  }

  function getData()
  {
    $ret = new \stdClass();
    $ret->command = $this->command;
    $ret->param = $this->getParam();
    $ret->userid = $this->userID;

    return $ret;
  }
  
}