<?php

namespace TSCWebServer;

class Commands
{

  private $db = NULL;
  private $log = NULL;
  private $userdata = NULL;
  private $pushnotification = NULL;

  function __construct(SQLiteWrapper $db, $log, $userdata)
  {
    $this->db = $db;
    $this->log = $log;
    $this->userdata = $userdata;
    $this->pushnotification = new PushNotification($this->log);
  }
  /*___________________ COMMANDS ______________________________________*/
  //@return Answers istance
  public function init(CommandParam $command)
  {
    $command->setCommand(__FUNCTION__);
    return $this->sendCommandToLocalDaemonViaCrossBar($command);
  }

  public function configure(CommandParam $command)
  {
    $command->setCommand(__FUNCTION__);
    return $this->sendCommandToLocalDaemonViaCrossBar($command);
  }

  public function start(CommandParam $command)
  {
    $command->setCommand(__FUNCTION__);
    return $this->sendCommandToLocalDaemonViaCrossBar($command);

  }

  public function stop(CommandParam $command)
  {
    $command->setCommand(__FUNCTION__);
    return $this->sendCommandToLocalDaemonViaCrossBar($command);

  }

  public function reset(CommandParam $command)
  {
    $command->setCommand(__FUNCTION__);
    return $this->sendCommandToLocalDaemonViaCrossBar($command);
  }

  public function status(CommandParam $command)
  {
    $command->setCommand(__FUNCTION__);
    return $this->checkState($command);
  }

  /*________________________________________________________*/

  public function userCanIssueCommands()
  {

    $this->log->debug("Commands->" . __FUNCTION__);
    $this->log->debug($this->userdata);
    return $this->userdata->expire >= time();

  }

  /*______________ Private Member _____ */

  private function sendCommandToLocalDaemonViaCrossBar(CommandParam $command)
  {
    $ans = new Answer();
    $commandvalue = $command->getCommand();
    $this->log->debug("Commands->sendCommandToLocalDaemonViaCrossBar: '$commandvalue'");
    $this->log->debug($command);
    $this->pushnotification->issueCommandToDaemon($command->getData());
    $ans->setOkAction();
    $ans->setReason("Command issued to Daemon");
    return $ans;
  }

  private function checkState(CommandParam $command)
  {
    $ans = new Answer();
    $commandvalue = $command->getCommand();

    $this->log->debug("Commands->checkState: $commandvalue");
    $this->log->debug($command);

    $lastStatus = $this->db->getStatus();
    $this->log->debug("Commands->checkState: last status");

    $this->log->debug($lastStatus);
    $ans->setOkAction();
    $ans->setReason("Retrieved last known status :-)");

    $ans->setResult(utils::getObjectFromSeparatedArray($lastStatus));


    return $ans;
  }
}
