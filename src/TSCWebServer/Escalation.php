<?php

namespace TSCWebServer;



class Escalation {

  private $db = NULL;  
  private $log = NULL;
  private $defaultEscalationRequestDuration = 60;
  
  function __construct(SQLiteWrapper $db, $log)
  {
    $this->db = $db;
    $this->log = $log;
  }   

  //@return Answers istance
  public function amiprivileged($userData, $params = NULL) {
    $this->log->debug("Escalation.amiprivileged()");

    $ans = new Answer();
    
    //1° phase retrieve current priviledged user if any 
    $priviledgedUser = $this->db->getPriviledgedUserData();
    
    $this->log->debug($priviledgedUser);

    // 2° phase retrive user priviledge
    $is_priviledged = (
                ($userData->username == $priviledgedUser->username) &&  
                ($priviledgedUser->priviledge_expiration >= time()) )?
                'true'
                :'false';
    $ans->setResult($is_priviledged);
    $this->log->debug("amiprivileged 1° phase ok");
    if($priviledgedUser->UserEmpty)
      $message = "there aren't any priviledged user";
    else
      $message = "the current privileged user is " . $priviledgedUser->name;
    
    //4° phase retrive the escalating user if any
    $currentEscalatingUser = $this->db->getEscalatingUserData();
    if(!$currentEscalatingUser->UserEmpty)
      $message .= " the current escalating user is " . $currentEscalatingUser->name;

    
    $this->log->debug($currentEscalatingUser);

    
    $this->log->debug("amiprivileged 2° phase");
    $this->log->debug("Escalation: amiprivileged->". $is_priviledged);
    $ans->setCurrentPriviledgedUserName($priviledgedUser->username);
    $ans->setCurrentPriviledgedName($priviledgedUser->name);
    $ans->setCurrentEscalatingName($currentEscalatingUser->name);
    if (!$priviledgedUser->UserEmpty) {
      $ans->setPriviledgeExpirationSeconds($priviledgedUser->priviledge_expiration - time());
    } 
    else {
      $ans->setPriviledgeExpirationSeconds(0);
    }

    $ans->setReason($message);
    return $ans;
  }
  
  public function iwouldliketoescalate($userData, $params) {
    $ans = new Answer();
    $notification = new PushNotification($this->log);
    
    $result = "";
    $reason = "";
    $currentPriviledgedName = "";
    $currentPriviledgedUsername = "";
    $timestamp="";
    $currentEscalatingName = "";
    $currentEscalatingUsername = "";
    $remainingSeconds = 0;
    
    // 1° phase check if can escalate
    $this->log->debug("currentUser:");
    $this->log->debug($userData);
    
    $currentUsername = $userData->username;
    $this->log->debug("params:");

    $this->log->debug($params);

    $forceEscalation = (isset($params->forceEscalation) && $params->forceEscalation === 'true');
    $isAdmin = $userData->isAdmin;
    
    if (!$userData->user_can_escalate || !$userData->role_can_escalate) {
      throw new CantEscalateException();
    }
    // 2° phase check the escalation procedure
    $this->log->debug("pre scalating ");
    $escalatingUser = $this->db->getEscalatingUserData();
    $priviledgedUser = $this->db->getPriviledgedUserData();
  
    $this->log->debug("currentEscalatingUser:");
    $this->log->debug($escalatingUser);

    $this->log->debug("priviledgedUser:");
    $this->log->debug($priviledgedUser);

    
  
    $currentPriviledgedName = $priviledgedUser->name;
    $currentPriviledgedUsername = $priviledgedUser->username;
    
    $currentEscalatingName = $escalatingUser->name;
    $currentEscalatingUsername = $escalatingUser->username;
    $timestamp = $escalatingUser->request_time;
    $force_enabling_time = $escalatingUser->force_enabling_time;
    
    
    if ($currentPriviledgedUsername == $currentUsername) {
      //you are escalating on yourself... 
      $this->log->debug("iwouldliketoescalate: you are escalating on yourself... ");
      $result = "youAreAlreadyPriviledged";
      $reason = "You are already priviledge, nothing will be done";
    }
    else {
      //check if is the admin and he is forcing the escalation
      if ($isAdmin && $forceEscalation) {
        $this->log->debug("iwouldliketoescalate: is the admin and he is forcing the escalation");

        //remove from table current escalating user if any
        //remove from table priviledge user if any
        //assign priviledge to this user
        while (!$this->db->priviledgeAdmin($currentUsername)) {
          $this->log->warn("iwouldliketoescalate: conflict on db retry to gain power! i'm ADMIN!");
          sleep(1);
        }
        //send notification to all for the new priviledged user
        $notification->newPriviledgedUser($this->db->getPriviledgedUserData());
        if(!$escalatingUser->UserEmpty) {
          $notification->escalatingProcedureCanceled($escalatingUser->token);
        }
        if(!$priviledgedUser->UserEmpty) {
          $notification->priviledgeRemoved($escalatingUser->token);
        }
        $result ="ecalationCompleted";
        $reason = "You are the new priviledged user, you used the FORCE well, pay attention to the Dark Side Of The Force";
      }
      else {
        //normal escalation 
        //check if there is a current priviledged user
        if(empty($currentPriviledgedUsername) || 
                  ($priviledgedUser->priviledge_expiration < time() &&
                   $priviledgedUser->priviledge_will_expire)) {
          $this->log->debug("iwouldliketoescalate: there isn't a priviledged user or its priviledge time is over and his priviledge can expire");

          //there isn't a priviledged user or its priviledge time is over and his priviledge can expire
          //assign priviledge flag to this user
          //remove from table priviledge user if any

          $success = $this->db->priviledgeUser($currentUsername);

          if($success) {
            //send notification to all for the new priviledged user
            $notification->newPriviledgedUser($this->db->getPriviledgedUserData());
            $result = "escalationCompleted";
            $reason = "You are the new priviledged user, there wasn't any other, you like to win easy :)";
          }
          else {
            $result = "procedureAlreadyStarted";
            $reason = "The procedure is already started by another user, transaction fail";
          }
        }
        else {
          //there is a priviledge user
          //check if there are any other escalation in progress
          if(!empty($currentEscalatingUsername)) {
            $this->log->debug("iwouldliketoescalate:there is a priviledge user and there is another escalation in progress");

            //there is another escalation in progress
            //check if am I that is escalating
            if($currentUsername == $currentEscalatingUsername) {
              //yes it's me
              $this->log->debug("iwouldliketoescalate:there is a priviledge user and there is another escalation in progress and yes it's me");

              //check if I can control the force parameter
              if ( $priviledgedUser->no_answer_count > 2 ||
                  time() >= ($escalatingUser->force_enabling_time) ) {
                //I can check the Force parameter
                $this->log->debug("iwouldliketoescalate:I can check the Force parameter");

                if($forceEscalation) {
                  $this->log->debug("iwouldliketoescalate:remove the priviledge flag to the other user");

                  //remove the priviledge flag to the other user
                  //assign priviledge flag to this user
                  $success = $this->db->priviledgeUser($currentUsername);
                  if($success) {
                    //send notification to all for the new priviledged user
                    $notification->newPriviledgedUser($this->db->getPriviledgedUserData());
                    $result = "escalationCompleted";
                    $reason = "You are the new priviledged user, you used the force well my little padawan";
                  }
                  else {
                    $result = "procedureAlreadyStarted";
                    $reason = "The procedure is already started by another user, transaction fail";
                  }
                }
                else {
                  // It isn't used the force parameter but we the client can use
                  $result = "WAFOUFEE";
                  $reason = "Waiting Answer From " . $currentPriviledgedName . ", Force Escalation Enabled";
                }
              }
              else {
                //It isn't passed enough time calc the remaining or the aren't enough no answer
                $remainingSeconds = ($timestamp + $this->defaultEscalationRequestDuration) - time();
                $result = "WAFOU";
                $reason = "Waiting Answer From " . $currentPriviledgedName;
              }
            }
            else {
               $this->log->debug("iwouldliketoescalate:check if am I that is escalating: not it isn't me");

              //check if am I that is escalating: not it isn't me
              $result = "procedureAlreadyStarted";
              $reason = "The procedure is already started by " . $currentEscalatingName;
            }
          }
          else {
            // there isn't any other escalation in progress but there is the priviledged
            //insert user on the escalating user table
            //send notification to the current priviledged user
            $this->log->debug("iwouldliketoescalate:there isn't any other escalation in progress but there is the priviledged");
  
            $success = $this->db->startEscalation(
              $currentUsername, $userData->isAdmin
            );
            if($success) {
              //send notification to the current priviledge user for informing him that the escalation is started
              $escalatingUserData = $this->db->getEscalatingUserData();
  
              $this->log->debug("new current escalating user:");
              $this->log->debug($escalatingUserData);
              $notification->escalationInitiated(
                $priviledgedUser, $escalatingUserData
              );
              $notification->autoGrantTimer($escalatingUserData);
              
              $result = "procedureInitiated";
              $reason = "The procedure is now started by you";
            }
            else {
              $result = "procedureAlreadyStarted";
              $reason = "The procedure is already started by another user, transaction fail";
            }
          }
        }
      }
    }
  
    // 3° phase build answer
    $newPriviledgedUser = $this->db->getPriviledgedUserData();
    $newEscalatingUser = $this->db->getEscalatingUserData();
    $ans->setResult($result);
    $ans->setReason($reason);
    $ans->setCurrentPriviledgedName($newPriviledgedUser->name);
    $ans->setCurrentEscalatingName($newEscalatingUser->name);
    $ans->setPriviledgeExpirationSeconds($newPriviledgedUser->priviledge_expiration - time());
    $ans->setSecondsToForceEnabling(
      $newEscalatingUser->force_enabling_time - time()
    );
    $ans->setSecondsToAutoGrantPrivilege(
      $newEscalatingUser->priviledge_grant_time - time()
    );


    return $ans;
  }
  
  public function releaseprivilege($userData, $params = NULL)
  {
    $ans = new Answer();
    $notification = new PushNotification($this->log);
    $notification->stopAutoGrantTimer();
    //1° phase retrieve current priviledged user if any
    $priviledgedUser = $this->db->getPriviledgedUserData();

    $this->log->debug($priviledgedUser);

    // 2° phase retrive user priviledge
    $is_priviledged = (
      ($userData->username == $priviledgedUser->username) &&
      ($priviledgedUser->priviledge_expiration >= time()));

    if ($is_priviledged) {
      $this->db->releasePriviledge();
      $escalatinguser = $this->db->getEscalatingUserData();
      $this->log->debug($escalatinguser);
  
      if (!$escalatinguser->UserEmpty) {
        $this->db->priviledgeUser($escalatinguser->username);
      }
      $currentPrivilegeUser = $this->db->getPriviledgedUserData();
      $ans->setOkAction();
      $ans->setCurrentPriviledgedUserName($currentPrivilegeUser->username);
      $ans->setCurrentPriviledgedName($currentPrivilegeUser->name);
      $ans->setCurrentEscalatingName("");
      if (!$priviledgedUser->UserEmpty) {
        $ans->setPriviledgeExpirationSeconds(
          $currentPrivilegeUser->priviledge_expiration - time()
        );
      } else {
        $ans->setPriviledgeExpirationSeconds(0);
      }



      $ans->setReason("Now you haven't the power any more, you suck!");
      $notification->newPriviledgedUser($this->db->getPriviledgedUserData());
    } else {
      $ans->setFailAction();
      $ans->setReason("You aren't the priviledged user.... nice try :-)");
    }

    return $ans;


  }
}
