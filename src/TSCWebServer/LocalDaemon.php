<?php
namespace TSCWebServer;

use Ratchet\Wamp\Exception;
use React\Stream\Util;

define('SEM_KEY', 1000);


class LocalDaemon
{

  private $sock_path = '/tmp/tsc.sock';
  private $sock = NULL;
  private $log = NULL;
  private $parser = NULL;
  private $connectionStatusValue = NULL;
  private $commandOngoing = NULL;
  private $commandOngoingValue = "";
  private $commandOnGoingParam = "";
  private $lastCommand = "";
  private $lastParam = "";
  private $launchtime = 0;
  private $elaspedtime = 0;
  private $daemonActive = false;
  private $semRes;
  //php56w-process
  //php-pecl-event


  function __construct(\Slim\Log $log = NULL)
  {
    $this->log = $log;
    $this->log->debug("LocalDaemon->construct()");
    $this->parser = new LocalDaemonParser($log);
    $this->connectionStatusValue = new ParserData();
    $this->commandOngoing = FALSE;
    $this->log->debug("LocalDaemon->construct(): before sem_get()");
    $this->semRes = sem_get(SEM_KEY, 1, 0666, 0); // get the resource for the semaphore
    $this->log->debug("LocalDaemon->construct(): after sem_get()");
    try {
      $this->connect();
    } catch (\Exception $e) {
      $this->log->error($e->getMessage());
    }
  }

  private function setCommandOnGoing()
  {
    $ret = false;
    try {

      if ($this->semRes) {
        if (sem_acquire($this->semRes)) { // try to acquire the semaphore. this function will block until the sem will be available
          if (!$this->commandOngoing) {
            $this->commandOngoing = true;
            $ret = true;
            $this->log->debug("LocalDaemon->setCommandOnGoing(): Command on Going: " . ($this->commandOngoing ? 'true' : 'false'));
          }
          sem_release($this->semRes); // release the semaphore so other process can use it
        } else {
          $this->log->warning("LocalDaemon->setCommandOnGoing(): Failed to ACQUIRE Semaphore!!");
        }
      } else {
        $this->log->warning("LocalDaemon->setCommandOnGoing(): Failed to GET Semaphore!!");
      }
    } catch (Exception $e) {
      $this->log->error("LocalDaemon->setCommandOnGoing(): " . $e->getMessage());

    } finally {
      return $ret;
    }
  }

  private function unsetCommandOnGoing()
  {
    $ret = false;

    try {


      if ($this->semRes) {
        if (sem_acquire($this->semRes)) { // try to acquire the semaphore. this function will block until the sem will be available
          $this->commandOngoing = false;
          $ret = true;
          $this->log->debug("LocalDaemon->unsetCommandOnGoing(): Command on Going: " . ($this->commandOngoing ? 'true' : 'false'));
          sem_release($this->semRes); // release the semaphore so other process can use it
        } else {
          $this->log->warning("unsetCommandOnGoing(): Failed to ACQUIRE Semaphore!!");
        }
      } else {
        $this->log->warning("unsetCommandOnGoing(): Failed to GET Semaphore!!");
      }
    } catch (Exception $e) {
      $this->log->error("unsetCommandOnGoing():" . $e->getMessage());

    } finally {
      return $ret;
    }


  }

  private
  function connect_socket()
  {
    try {
      $this->sock = socket_create(AF_UNIX, SOCK_STREAM, 0);
      if ($this->sock === false) {
        $this->connectionStatusValue = new ParserData();
        $this->log->error("LocalDaemon->open_socket() --> FALSE");
        throw new LocalDaemonNotFoundException(socket_strerror(socket_last_error()));
      } else {
        if (!socket_connect($this->sock, $this->sock_path)) {
          $this->log->error("LocalDaemon->open_socket() --> can't connect");
          throw new LocalDaemonNotFoundException(socket_strerror(socket_last_error()));
        } else {
          $this->log->debug("LocalDaemon->open_socket() read the answer line at initial connetion with daemon:");
          $this->connectionStatusValue = $this->parser->parse($this->readline());
          $this->connectionStatusValue->setExitStatus("undefined");
          $this->checkDaemon();
          $this->log->info("LocalDaemon socket opened successfully");
        }

      }
      return true;
    } catch (\Exception $e) {
      $this->log->error("LocalDaemon->open_socket() --> can't connect: " . $e->getMessage());
      $this->daemonActive = false;
      socket_clear_error();
      return false;
    }
  }

  private
  function prepareCommand($command, $param)
  {
    return $command . ($param != "" ? " " . $param . "\n" : "\n");
  }

  private
  function close_socket()
  {
    socket_close($this->sock);
  }

  private
  function readline()
  {
    $data = "";
    $this->log->debug("LocalDaemon->readline reading from socket...");
    $data = socket_read($this->sock, 8192, PHP_NORMAL_READ);
    if (false === $data) {
      $this->log->error("LocalDaemon->readline data read is FALSE!!");
      throw new LocalDaemonNotFoundException("data read is FALSE: " . "last socketError:" . socket_last_error() . " -> " . socket_strerror(socket_last_error()));
    }
    $this->log->debug("LocalDaemon->readline data read: '" . str_replace(PHP_EOL, '', $data) . "'");
    return (string)$data;
  }

  public
  function issueCommand($command, $param = "")
  {

    $ret = new LocalDaemonAnswer();
    $message = "";
    try {
      if ($this->setCommandOnGoing()) {
        $this->log->debug("LocalDaemon->issueCommand: $command - $param");
        $preparedcommand = $this->prepareCommand($command, $param);
        $this->commandOngoingValue = $command;
        $this->commandOnGoingParam = $param;
        $this->lastCommand = "";
        $this->lastParam = "";
        $this->launchtime = time();
        $this->elaspedtime = 0;
        //$this->log->debug("LocalDaemon->issueCommand: prepared command: $preparedcommand");
        $write_result = socket_write($this->sock, $preparedcommand, strlen($preparedcommand));
        $lastErrorSocket = socket_last_error();
        $this->log->debug("LocalDaemon->issueCommand: write result:" . $lastErrorSocket . " -> " . socket_strerror($lastErrorSocket) . " write_result:" . $write_result);

        $this->log->debug("LocalDaemon->issueCommand command issued: '" . $preparedcommand . "'");
        $this->log->debug("LocalDaemon->issueCommand result of socket writing: " . socket_strerror($lastErrorSocket));;
        if ($lastErrorSocket != 0) {
          throw new \LocalDaemonNotFoundException(socket_strerror($lastErrorSocket));
        }
        $this->connectionStatusValue = $this->parser->parse($this->readline());
        //return the command result parse
        //$this->log->debug($this->connectionStatusValue);

        $message = "Command completed";
        socket_clear_error();

        $this->lastCommand = $command;
        $this->lastParam = $param;
        $this->commandOngoingValue = "";
        $this->commandOnGoingParam = "";
        $this->elaspedtime = time() - $this->launchtime;
        $this->launchtime = 0;
        $this->unsetCommandOnGoing();

        $this->log->debug("LocalDaemon->issueCommand -> yeah!!!");

      } else {
        $message = "Another command is executing wait for its finish";
        $this->log->debug("LocalDaemon->issueCommand: Another command is executing wait for its finish");
      }

    } catch (\Exception $e) {
      $this->log->error("LocalDaemon->issueCommand " . $e->getMessage());
      $this->daemonActive = false;
      $this->connectionStatusValue->setExitStatus(false);
      $this->unsetCommandOnGoing();
      $message = $e->getMessage() . " socketErr: " . socket_strerror(socket_last_error());
      socket_clear_error();
    } finally {

      return $this->getConnectionStatus($message);
    }
  }

  public
  function checkDaemon()
  {
    $message = "";
    try {

      $write = socket_write($this->sock, "", 0);
      //$this->log->debug($write);
      $last_error = socket_last_error();
      $this->log->debug("LocalDaemon->checkDaemon: lastsocketError: " . $last_error . " -> " . socket_strerror($last_error));
      if ($last_error) {
        $this->log->debug("LocalDaemon->checkDaemon: if error lastsocketError:" . $last_error . " -> " . socket_strerror($last_error));
      }

      $this->daemonActive = ($last_error == 0);
      $this->log->debug("LocalDaemon->checkDaemon: commandongoing: " . ($this->commandOngoing ? 'true' : 'false'));
      $message = socket_strerror($last_error);

    } catch (\Exception $e) {
      $this->log->error("LocalDaemon->checkDaemon: exception! lastsocketError:" . socket_last_error() . " -> " . socket_strerror(socket_last_error()));
      $this->log->error($e->getMessage());
      $this->daemonActive = false;
      $message = $e->getMessage();
    } finally {
      socket_clear_error();

      return $this->getConnectionStatus($message);
    }
  }

  public
  function getConnectionStatus($message = "Connection Status")
  {
    $ret = new LocalDaemonAnswer();
    $ret->setCurrentState($this->connectionStatusValue->getCurrentState());
    $ret->setCommandExitStatus($this->connectionStatusValue->getExitStatus());
    $ret->setLaunchtime($this->launchtime);
    $ret->setProcesses($this->connectionStatusValue->getProcesses());
    $ret->setInfos($this->connectionStatusValue->getInfos());
    $ret->setCommandOngoing($this->commandOngoing);
    $ret->setOnGoingCommandValue($this->commandOngoingValue);
    $ret->setOnGoingParam($this->commandOnGoingParam);
    $ret->setLastCommandValue($this->lastCommand);
    $ret->setLastParam($this->lastParam);
    $ret->setLaunchtime($this->launchtime);
    $ret->setElaspedTime($this->elaspedtime);
    $ret->setDaemonActive($this->daemonActive);
    $ret->setMessage($message);
    return $ret;
  }

  public
  function connect()
  {
    try {
      return $this->connect_socket();
    } catch (\Exception $e) {
      $this->log->error($e->getMessage());
      return false;
    }
  }

  function __destruct()
  {
    $this->log->debug("LocalDaemon->deconstruct(): before sem_remove()");
    sem_remove($this->semRes);
    $this->log->debug("LocalDaemon->construct(): after sem_remove()");
    $this->log->debug("LocalDaemon->DESTRUCT() destroyng local daemon connection");

    $this->close_socket();


  }

}
