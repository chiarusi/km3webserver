<?php
/**
 * Created by PhpStorm.
 * User: favaro
 * Date: 19/11/15
 * Time: 14.00
 */

namespace TSCWebServer;


class LocalDaemonAnswer
{
  private $obj = array();


  function __construct()
  {

    $this->obj["processes"] = array();
    $this->obj["daemonActive"] = "";
    $this->obj["currentState"] = "";
    $this->obj["exitStatus"] = "";
    $this->obj["onGoingCommand"] = "";
    $this->obj["onGoingParam"] = "";
    $this->obj["lastCommand"] = "";
    $this->obj["lastParam"] = "";
    $this->obj["commandOngoing"] = "false";;
    $this->obj["infos"] = array();
    $this->obj["launchtime"] = "";
    $this->obj["elapsedtime"] = "";
    $this->obj["message"] = "";

  }

  /**
   * @return array
   */
  public function getObj()
  {
    return $this->obj;
  }

  /**
   * @param array $obj
   */
  public function setObj($obj)
  {
    $this->obj = $obj;
  }

  public function setCurrentState($currentState)
  {
    $this->obj["currentState"] = $currentState;
  }

  public function getCurrentState()
  {
    return $this->obj["currentState"];
  }

  public function setProcesses($processes)
  {
    $this->obj["processes"] = $processes;
  }

  public function getProcesses()
  {
    return $this->obj["processes"];
  }

  public function setDaemonActive($daemonActive)
  {
    $this->obj["daemonActive"] = ($daemonActive) ? "true" : "false";
  }

  public function getDaemonActive()
  {
    return $this->obj["daemonActive"] == 'true';
  }

  public function setCommandOngoing($commandOngoing)
  {
    $this->obj["commandOnGoing"] = ($commandOngoing) ? "true" : "false";
  }

  public function getCommandOngoing()
  {
    return $this->obj["commandOnGoing"] == 'true';
  }


  public function setCommandExitStatus($exitStatus)
  {
    $this->obj["exitStatus"] = $exitStatus;
  }

  public function getLastCommandExitStatus()
  {
    return $this->obj["exitStatus"];
  }

  public function setOnGoingCommandValue($data)
  {
    $this->obj["onGoingCommand"] = $data;
  }

  public function getOnGoingCommandValue()
  {
    return $this->obj["onGoingCommand"];
  }

  public function setOnGoingParam($data)
  {
    $this->obj["onGoingParam"] = $data;
  }

  public function getOnGoingParam()
  {
    return $this->obj["onGoingParam"];
  }

  public function setLastCommandValue($data)
  {
    $this->obj["lastCommand"] = $data;
  }

  public function getLastCommandValue()
  {
    return $this->obj["lastCommand"];
  }

  public function setLastParam($data)
  {
    $this->obj["lastParam"] = $data;
  }

  public function getLastParam()
  {
    return $this->obj["lastParam"];
  }

  public function setInfos($data)
  {
    $this->obj["infos"] = $data;
  }

  public function getInfos()
  {
    return $this->obj["infos"];
  }

  public function setLaunchtime($data)
  {
    $this->obj["launchtime"] = $data;
  }

  public function getLaunchtime()
  {
    return $this->obj["timeelapsed"];
  }

  public function setElaspedTime($data)
  {
    $this->obj["elapsedtime"] = $data;
  }

  public function getElaspedTime()
  {
    return $this->obj["elapsedtime"];
  }


  public function setMessage($data)
  {
    $this->obj["message"] = $data;
  }

  public function getMessage()
  {
    return $this->obj["message"];
  }

  public function getAnswerObject()
  {
    return $this->obj;
  }

}