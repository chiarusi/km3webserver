<?php

namespace TSCWebServer;
class LocalDaemonNotFoundException extends \Exception
{
    // Redefine the exception so message isn't optional
    public function __construct($message = null, $code = 1, Exception $previous = null) {
    
        $message = "Can't open the local socket " . $message;
        parent::__construct($message, $code, $previous);
    }

    // custom string representation of object
    public function __toString() {
        return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
    }
}
