<?php
namespace TSCWebServer;

class LocalDaemonParser
{
  
  /*  Possible output:
   * O idle NODES:TSV:;HM:;TCPU:;EM: <--- AT FIRST CONNECT ALWASY "STATE"
   * O standby RUNSETUP:/home/favaro/Develop/tridas/datacards/configuration.json
   * X idle reason: invalid run setup "/home/favaro/Develop/tridas/datacards/configuration.json"
   * O standby RUNSETUP:/home/favaro/KM3WebServer/storage/runsetup/configuration_rungroup1_ptts_2_sct_2_4tower_4hm_4tcpu.json NODES:TSV:0/1;HM:0/4;TCPU:0/4;EM:0/1
   * X standby reason: invalid request (f)
   * O ready RUNSETUP:/home/favaro/KM3WebServer/storage/runsetup/configuration_rungroup1_ptts_2_sct_2_4tower_4hm_4tcpu.json RUNDATACARD:/lxstorage1_home/km3/datacard/tsc/tridas_configuration_rungroup1_ptts_2_sct_2_4tower_4hm_4tcpu.json_00000001.json RUNNUMBER:1 NODES:TSV:0/1;HM:4/4;TCPU:4/4;EM:0/1
   * X idle reason: invalid request ()
   * O running RUNSETUP:/home/favaro/KM3WebServer/storage/runsetup/configuration_rungroup1_ptts_2_sct_2_4tower_4hm_4tcpu.json RUNDATACARD:/lxstorage1_home/km3/datacard/tsc/tridas_configuration_rungroup1_ptts_2_sct_2_4tower_4hm_4tcpu.json_00000001.json RUNNUMBER:1 RUNSTARTTIME:508410879 NODES:TSV:1/1;HM:4/4;TCPU:4/4;EM:1/1
   * X standby reason: something happened (TM)
   * X standby reason: invalid request (ciao)
   * */
  
  private $log = NULL;
  
  function __construct(\Slim\Log $log = NULL)
  {
    $this->log = $log;
  }
  
  public function parse($string)
  {
    try {
      $string = rtrim($string);
      $this->log->debug("LocalDaemonParser->parse(" . $string . ")");
      $pieces = explode(" ", $string);
      $ret = new ParserData();
      $hasNext = true;
      for ($i = 0; $i < count($pieces) && $hasNext; $i++) {

        $hasNext = $this->readPiece($pieces, $i, $ret);
      }
      //$this->log->debug("----- PARSE RESULT ---------");
      //$this->log->debug($ret->getData());
      //$this->log->debug("----------------------------");
      return $ret;
    } catch (\Exception $e) {
      throw new ParsingException("Problem: " . $e->getMessage());
    }
  }


  private function readPiece(&$pieces, $pos, ParserData &$parseData)
  {
    try {
      $splitted = $this->readColonSeparatedVar($pieces[$pos]);
      $method = "read" . strtoupper($splitted[0]);
      $this->log->debug("LocalDaemonParser->readPiece method:" .
        $method . "(" . (array_key_exists(1, $splitted) ? $splitted[1] : "") . ")");

      if (method_exists($this, $method)) {
        return $this->$method($pieces, $pos, $splitted, $parseData);
      } else {
        throw new \Exception("Please implement the parsing of ( $splitted[1] ) with name: $method");
      }

    } catch (\Exception $e) {
      throw new ParsingException("Problem: " . $e->getMessage());
    }

  }

  private function readO(&$pieces, $pos, &$splittedpiece, ParserData &$parseData)

  {
    $parseData->setExitStatus("true");
    return true;
  }

  private function readX(&$pieces, $pos, &$splittedpiece, ParserData &$parseData)
  {
    $parseData->setExitStatus("false");
    return true;
  }

  private function readIDLE(&$pieces, $pos, &$splittedpiece, ParserData &$parseData)
  {
    $parseData->setCurrentState($this->readState($splittedpiece));
    return true;
  }

  private function readSTANDBY(&$pieces, $pos, &$splittedpiece, ParserData &$parseData)
  {
    $parseData->setCurrentState($this->readState($splittedpiece));
    return true;
  }

  private function readREADY(&$pieces, $pos, &$splittedpiece, ParserData &$parseData)
  {
    $parseData->setCurrentState($this->readState($splittedpiece));
    return true;
  }

  private function readRUNNING(&$pieces, $pos, &$splittedpiece, ParserData &$parseData)
  {
    $parseData->setCurrentState($this->readState($splittedpiece));
    return true;
  }

  private function readState(&$splittedInformation)
  {
    return $splittedInformation[0];
  }

  private function readNODES(&$pieces, $pos, &$splittedpiece, ParserData &$parseData)
  {
    $proccesesPieces = explode(";", $splittedpiece[1]);
    foreach ($proccesesPieces as $proccessPiece) {
      $process = $this->readColonSeparatedVar($proccessPiece);
      $parseData->addProcesses($process[0], $this->readProcessesStatus($process[1]));
    }
    return true;
  }


  private function readProcessesStatus($process)
  {
    $this->log->debug("LocalDaemonParser->readProcessesStatus(" . $process . ")");
    //$this->log->debug($process);

    $values = $this->readSlashSeparatedVar($process);
    //$this->log->debug("split");
    //$this->log->debug($values);

    return array('active' => (isset($values[0]) && !empty($values[0])) ? $values[0] : 0,
      'outOf' => isset($values[1]) ? $values[1] : 0
    );
  }


  private function readRUNSETUP(&$pieces, $pos, &$splittedpiece, ParserData &$parseData)
  {
    return $this->readOther($splittedpiece[0], $splittedpiece[1], $parseData);
  }

  private function readRUNNUMBER(&$pieces, $pos, &$splittedpiece, ParserData &$parseData)
  {
    return $this->readOther($splittedpiece[0], $splittedpiece[1], $parseData);
  }

  private function readRUNDATACARD(&$pieces, $pos, &$splittedpiece, ParserData &$parseData)
  {
    return $this->readOther($splittedpiece[0], $splittedpiece[1], $parseData);
  }

  private function readRUNSTARTTIME(&$pieces, $pos, &$splittedpiece, ParserData &$parseData)
  {
    return $this->readOther($splittedpiece[0], $splittedpiece[1], $parseData);
  }

  private function readREASON(&$pieces, $pos, &$splittedpiece, ParserData &$parseData)
  {

    $parseData->addInfos($splittedpiece[0], ltrim(implode(" ", array_slice($pieces, $pos + 1))));
    return false;
  }

  private function readOther($fieldName, $content, ParserData &$parseData)
  {
    $information = $this->readSpaceSeparatedVar($content);
    $parseData->addInfos($fieldName, ltrim(str_replace("\"", "", $information[0])));
    return true;
  }


/////////////////////////////////////////////////
  
  private function readColonSeparatedVar($colonseparatedString)
  {
    return explode(":", $colonseparatedString, 2);
  }

  private function readSpaceSeparatedVar($spaceseparatedString)
  {
    return explode(" ", $spaceseparatedString, 2);
  }

  private function readSlashSeparatedVar($slashseparatedString)
  {
    return explode("/", $slashseparatedString, 2);
  }
  
}
