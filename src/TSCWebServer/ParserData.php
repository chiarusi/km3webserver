<?php
/**
 * Created by PhpStorm.
 * User: favaro
 * Date: 19/11/15
 * Time: 14.20
 */

namespace TSCWebServer;


class ParserData
{
  private $processes = array();
  private $configurationInfos = array();
  private $currentState = "";
  private $exitStatus = "";
  private $creationTime = 0;


  function __construct()
  {
    $this->creationTime = time();
    $this->configurationInfos['reason'] = "";
  }

  public function setCurrentState($currentState)
  {
    $this->currentState = $currentState;
  }

  public function getCurrentState()
  {
    return (string)$this->currentState;
  }


  public function setExitStatus($existStatus)
  {
    $this->exitStatus = $existStatus;
  }

  public function getExitStatus()
  {
    return (string)$this->exitStatus;
  }

  public function addProcesses($processName, $value)
  {
    $this->processes[$processName] = $value;
  }

  public function addInfos($infoname, $value)
  {
    $this->configurationInfos[$infoname] = $value;
  }

  public function getProcesses()
  {
    return $this->processes;
  }

  public function getInfos()
  {
    return $this->configurationInfos;
  }


}