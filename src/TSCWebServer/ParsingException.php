<?php

namespace TSCWebServer;
class ParsingException extends \Exception
{
  // Redefine the exception so message isn't optional
  public function __construct($message = null, $code = 1, Exception $previous = null)
  {
    
    $message = "Error During Parsing output from Daemon " . $message;
    parent::__construct($message, $code, $previous);
  }

  // custom string representation of object
  public function __toString()
  {
    return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
  }

}
