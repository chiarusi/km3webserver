<?php

namespace TSCWebServer;


class PushNotification
{

  private $secret = "kkjH68GiuUZ";
  private $key = "test";
  private $websocketServerAddress = "http://127.0.0.1:5555";
  protected $log;

  function __construct($log)
  {
    $this->log = $log;
  }


  /*  push messages to client from web server */
  public function stateMachineChange($state)
  {
    $this->log->debug(__CLASS__ . ":" . __FUNCTION__);

    $this->curlPost($this->getStatemachineStateChangeBody($state));
  }
  
  public function escalationInitiated($privilegedUser,
                                      $escalatingUserData
  )
  {
    $this->log->debug(__CLASS__ . ":" . __FUNCTION__);
  
    $this->curlPost(
      $this->getEscalationProcedureInitiatedBody(
        $privilegedUser, $escalatingUserData
      )
    );
  }
  
  public function autoGrantTimer($escalatingUserData) {
    $this->log->debug(__CLASS__ . ":" . __FUNCTION__);
    $this->curlPost($this->getStartAutoGrantTimerBody($escalatingUserData));
  
  }
  
  public function stopAutoGrantTimer() {
    $this->log->debug(__CLASS__ . ":" . __FUNCTION__);
    $this->curlPost($this->getStopAutoGrantTimerBody());
  }
  
  
  public function escalationForced($token)
  {
    $this->log->debug(__CLASS__ . ":" . __FUNCTION__);

  }

  public function escalationDenied($token)
  {
    $this->log->debug(__CLASS__ . ":" . __FUNCTION__);
  }

  public function newPriviledgedUser($newPriviledgedUserData)
  {
    $this->log->debug(__CLASS__ . ":" . __FUNCTION__);

    $this->curlPost($this->getNewPriviledgeUserBody($newPriviledgedUserData));
  }

  public function youHaveBeenKickedOut($token)
  {
    $this->log->debug(__CLASS__ . ":" . __FUNCTION__);

  }

  public function priviledgeReleased($userData)
  {
    $this->log->debug(__CLASS__ . ":" . __FUNCTION__);
    $this->curlPost($this->getPriviledgedReleasedBody($userData));

  }

  /* after retrieveing status crossbarr will public to all clients*/
  public function publishDaemonStatus($status) //object
  {
    $this->log->debug(__CLASS__ . ":" . __FUNCTION__);
    $this->curlPost($this->getPublishToClientsDaemonStatusBody($status));
  }


  /* push to (used from webserver and crossbarWorker crossbar.io that will be delivered to TSC*/

  public function issueCommandToDaemon($param)
  {
    $this->curlPost($this->getDaemonCommandExecutionBody($param));
  }


  //___________________ Member function __________________________________//

  private function curlPost($body)
  {
    //set the url, number of POST vars, POST datai
    $this->log->debug($body);
    $websocketserverpath = $this->getPath($this->getParam(), $body);
    //echo "\ncurl -H \"Content-Type: application/json\" -d '" . json_encode($body) . "' \"". $websocketserverpath . "\"\n\n";

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $websocketserverpath);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));
    curl_setopt($ch, CURLOPT_POST, count($body));
    curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HEADER, 1);

    $responce = curl_exec($ch);
    $result = array('header' => '',
      'body' => '',
      'curl_error' => '',
      'http_code' => '',
      'last_url' => '');
    $output = $responce;

    //var_dump($output);

    $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
    $header = substr($responce, 0, $header_size);
    $body = substr($responce, $header_size, strlen($responce));
    //storing result
    $result['header'] = $header;
    $result['http_code'] = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    $result['last_url'] = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);

    //var_dump($result);
    //$this->log->debug($result);
    //close connection
    curl_close($ch);

  }

  private function getParam()
  {

    $param = array();

    $param['timestamp'] = $this->getUTCTimestamp();
    $param['seq'] = 1; // on multiple push notification this number must increase
    $param['key'] = $this->key;
    $param['nonce'] = rand(0, 2 ^ 53);
    return $param;
  }

  private function getUTCTimestamp()
  {

    $t = microtime(true);
    $micro = sprintf("%06d", ($t - floor($t)) * 1000000);
    $timestamp = new \DateTime(date('Y-m-d H:i:s.' . $micro, $t));
    $timestamp->setTimeZone(new \DateTimeZone("UTC"));
    $formatedtimestamp = $timestamp->format('Y-m-d\TH:i:s.u\Z');
    //$formatedtimestamp ="2015-04-01T14:09:47.960279Z";
    return $formatedtimestamp;
  }

  private function getStatemachineStateChangeBody($state)
  {
    $body = array();
    $body['topic'] = 'com.tridas.statemachine.statechange';
    $body['args'] = array();
    $argument = array();

    $argument[] = array('state' => $state);
    $body['args'] = $argument;


    return utf8_encode(json_encode($body));

  }

  private function getNewPriviledgeUserBody($newPriviledgedUSerData)
  {
    $body = array();
    $argumentElements = new Answer();

    $body['topic'] = 'com.tridas.statemachine.priviledged.change';
    $body['args'] = array();
    $argument = array();

    $argumentElements->setCurrentPriviledgedName($newPriviledgedUSerData->name);
    $argumentElements->setCurrentPriviledgedUserName($newPriviledgedUSerData->username);
    $argumentElements->setPriviledgeExpirationSeconds($newPriviledgedUSerData->priviledge_expiration - time());
    $argumentElements->setCurrentEscalatingName("");


    $argument[] = $argumentElements->getAnsArray();

    $body['args'] = $argument;


    return utf8_encode(json_encode($body));


  }

  private function getPriviledgedReleasedBody($newPriviledgedUSerData)
  {
    $body = array();
    $argumentElements = new Answer();

    $body['topic'] = 'com.tridas.statemachine.priviledged.change';
    $body['args'] = array();
    $argument = array();

    $argumentElements->setCurrentPriviledgedName($newPriviledgedUSerData->name);
    $argumentElements->setCurrentPriviledgedUserName($newPriviledgedUSerData->username);
    $argumentElements->setPriviledgeExpirationSeconds($newPriviledgedUSerData->priviledge_expiration - time());
    $argumentElements->setCurrentEscalatingName("");


    $argument[] = $argumentElements->getAnsArray();

    $body['args'] = $argument;


    return utf8_encode(json_encode($body));


  }
  
  private function getEscalationProcedureInitiatedBody($privilegedUser,
                                                       $escalatingUserData
  )
  {
    //com.tridas.statemachine.escalation.' + token

    $body = array();
    $argumentElements = new Answer();
  
    $body['topic'] = 'com.tridas.statemachine.escalation.'
                     . $privilegedUser->token;
    $body['args']  = array();
    $argument      = array();

    // ADD TO ANSWER the autogranting field
    $argumentElements->setSecondsToAutoGrantPrivilege(
      $escalatingUserData->priviledge_grant_time - time()
    );
    $argumentElements->setSecondsToForceEnabling(
      $escalatingUserData->force_enabling_time - time()
    );
    $argumentElements->setCurrentEscalatingName($escalatingUserData->name);

    $argument[] = $argumentElements->getAnsArray();

    $body['args'] = $argument;


    return utf8_encode(json_encode($body));
  }
  
  private function getStartAutoGrantTimerBody($escalatingUserData) {
    //com.tridas.privilege.autogrant
    
    $body             = array();
    $argumentElements = new Answer();
    
    $body['topic'] = 'com.tridas.privilege.autogrant.start';
    $body['args']  = array();
    $argument      = array();
    
    // ADD TO ANSWER the autogranting field
    $argumentElements->setSecondsToAutoGrantPrivilege(
      $escalatingUserData->priviledge_grant_time - time()
    );
    $argumentElements->setSecondsToForceEnabling(
      $escalatingUserData->force_enabling_time - time()
    );
    $argumentElements->setCurrentEscalatingName($escalatingUserData->name);
    $argumentElements->setEscalatingUserName($escalatingUserData->username);
    //$argumentElements->setStartTimer($start);
    $argument[] = $argumentElements->getAnsArray();
    
    $body['args'] = $argument;
    
    
    return utf8_encode(json_encode($body));
  }
  
  private function getStopAutoGrantTimerBody() {
    //com.tridas.privilege.autogrant
    
    $body             = array();
    $argumentElements = new Answer();
    
    $body['topic'] = 'com.tridas.privilege.autogrant.stop';
    $body['args']  = array();
    $argument      = array();
    
    $body['args'] = $argument;
    
    
    return utf8_encode(json_encode($body));
  }

  private function getPublishToClientsDaemonStatusBody($status)
  {
    //com.tridas.statemachine.escalation.' + token

    $body = array();
    $argumentElements = new Answer();

    $body['topic'] = 'com.tridas.daemon.state';
    $body['args'] = array();
    $argument = array();

    $argument[] = $status;

    $body['args'] = $argument;

    return utf8_encode(json_encode($body));
  }

  /* ______________ Commands notification __________________ */

  private function getDaemonCommandExecutionBody($param)
  {
    $this->log->debug("getDaemonCommandExecutionBody");
    $this->log->debug($param);

    $body = array();
    $body['topic'] = 'com.tridas.execute.daemon';
    $body['args'] = array();
    $argument = is_null($param) ? array() : is_array($param) ? $param : array($param);

    $body['args'] = $argument;

    return utf8_encode(json_encode($body));
  }


  /*_________________ Other Function __________*/


  private function getPath($param, $bodyJsoned)
  {

    $param['signature'] = $this->signatureCalc($param, $bodyJsoned);
    //$this->log->debug($param);
    $path = $this->websocketServerAddress . "?" . http_build_query($param);

    return $path;
  }

  private function signatureCalc($param, $bodyJsoned)
  {
    //$this->log->debug(json_encode($param));
    /* test hash update
    $ctx = hash_init('sha256', HASH_HMAC, $this->secret);
    $signature = hash_final($ctx);
    $this->log->debug( "1-->" . $signature );



    $ctx = hash_init('sha256', HASH_HMAC, $this->secret);
    hash_update($ctx, $this->key);
    $signature = hash_final($ctx);
    $this->log->debug( "2-->" . $signature );


    $ctx = hash_init('sha256', HASH_HMAC, $this->secret);
    hash_update($ctx, $this->key);
    hash_update($ctx, $param['timestamp']);
    $signature = hash_final($ctx);
    $this->log->debug( "3-->" . $signature );

    $ctx = hash_init('sha256', HASH_HMAC, $this->secret);
    hash_update($ctx, $this->key);
    hash_update($ctx, $param['timestamp']);
    hash_update($ctx, $param['seq']);
    $signature = hash_final($ctx);
    $this->log->debug( "4-->" . $signature );


    $ctx = hash_init('sha256', HASH_HMAC, $this->secret);
    hash_update($ctx, $this->key);
    hash_update($ctx, $param['timestamp']);
    hash_update($ctx, $param['seq']);
    hash_update($ctx, $param['nonce']);
    $signature = hash_final($ctx);
    $this->log->debug( "5-->" . $signature );


    $ctx = hash_init('sha256', HASH_HMAC, $this->secret);
    hash_update($ctx, $this->key);
    hash_update($ctx, $param['timestamp']);
    hash_update($ctx, $param['seq']);
    hash_update($ctx, $param['nonce']);
    hash_update($ctx, $bodyJsoned);
    $signature = hash_final($ctx);
    $this->log->debug( "final-->" . $signature );
    */
    $ctx = hash_init('sha256', HASH_HMAC, $this->secret);
    hash_update($ctx, $this->key);
    hash_update($ctx, $param['timestamp']);
    hash_update($ctx, $param['seq']);
    hash_update($ctx, $param['nonce']);
    hash_update($ctx, $bodyJsoned);

    $signature = hash_final($ctx, true);
    $encodedsign = base64_encode($signature);
    $encodedsign = str_replace(array('+', '/'), array('-', '_', ''), $encodedsign);
    return $encodedsign;
  }
  
  
}
