<?php
/**
 * Created by PhpStorm.
 * User: favaro
 * Date: 20/11/15
 * Time: 15.17
 */

namespace TSCWebServer;


use Ratchet\Wamp\Exception;
use Slim\Slim;

class RunSetup
{
    private $db = NULL;
    private $log = NULL;
    private $RunsetupPublishedDirectory;
    private $userdata;

    function __construct($log, SQLiteWrapper $db, $userData)
    {
        $this->db = $db;
        $this->log = $log;
        $this->runsetupPublishedDirectory = getcwd() . '/storage/PublishedRunsetup';
        $this->runsetupDraftDirectory = getcwd() . '/storage/DraftRunsetup';
        $this->runsetupForRunDirectory = getcwd() . '/storage/RunsetupForRun';
        $this->detectorsDirectory = getcwd() . '/storage/Detectors';
        $this->userdata = $userData;

    }


    public function getPublishedRunSetupList()
    {
        $ans = new Answer();
        $reason = "";

        try {
            $this->log->debug("RunSetup->getPublishedRunSetupList()");
            $this->log->debug("RunSetup->getPublishedRunSetupList() CWD:" . getcwd());
            $this->syncDataAndDB($this->runsetupPublishedDirectory,
                $this->db->getPublishedRunsetupList(),
                'insertPublishedRunsetupIntoDB',
                'deletePublishedRunsetupFromDB');
            $ans->setResult($this->db->getPublishedRunsetupList());

            $reason = "This is a fake feature... maybe a day we will be joyful with the real feature, god bless the Datamanager";
            $ans->setOkAction();


        } catch (\Exception $e) {
            $reason = $e->getMessage();
            $ans->setFailAction();

        }
        $ans->setReason($reason);
        return $ans;
    }

    public function getDraftRunSetupList()
    {
        $ans = new Answer();
        $reason = "";

        try {
            $this->log->debug("RunSetup->getDraftRunSetupList()");
            $this->log->debug("RunSetup->getDraftRunSetupList() CWD:" . getcwd());
            $this->syncDataAndDB($this->runsetupDraftDirectory,
                $this->db->getDraftRunsetupList(),
                'insertDraftRunsetupIntoDB',
                'deleteDraftRunsetupFromDB');
            $ans->setResult($this->db->getDraftRunsetupList());

            $reason = "This is a fake feature... I'll turn on the security camera on you";
            $ans->setOkAction();


        } catch (\Exception $e) {
            $reason = $e->getMessage();
            $ans->setFailAction();

        }
        $ans->setReason($reason);
        return $ans;
    }

    public function getDraftRunSetup($param)
    {
        $ans = new Answer();
        $reason = "";

        try {
            $this->log->debug("RunSetup->getDraftRunSetup()");
            $this->log->debug("RunSetup->param");
            $this->log->debug($param);


            $runsetup = $this->db->getDraftRunsetup($param->id);
            $this->log->debug($runsetup);
            $string = file_get_contents($runsetup->path . DIRECTORY_SEPARATOR . $runsetup->filename);

            $json_a = json_decode($string, true);
            $this->log->debug($json_a);

            $ans->setResult($json_a);

            $reason = "This is a fake feature... maybe a day we will be joyful with the real feature, god bless the Datamanager";
            $ans->setOkAction();


        } catch (\Exception $e) {
            $reason = $e->getMessage();
            $ans->setFailAction();

        }
        $ans->setReason($reason);
        return $ans;
    }
    public function deleteDraftRunSetup($param)
    {
        $ans = new Answer();
        $reason = "";

        try {
            $this->log->debug("RunSetup->deleteDraftRunSetup()");
            $this->log->debug("RunSetup->param");
            $this->log->debug($param);

            $runsetup = $this->db->getDraftRunsetup($param->id);
            $this->db->deleteDraftRunsetupFromDB($param->id);
            $this->log->debug($runsetup);

            try {
                $ret = unlink($runsetup->path . DIRECTORY_SEPARATOR . $runsetup->filename);
                if(!$ret) $reason = "DB entry removed, but file didn't exist";
                else $reason = "File removed, yuppy yuppy petaloso";
            } catch (\Exception $e) {
                $reason = "DB entry removed, but file didn't exist";
            }

            $ans->setOkAction();


        } catch (\Exception $e) {
            $reason = $e->getMessage();
            $ans->setFailAction();

        }
        $ans->setReason($reason);
        return $ans;
    }
    public function getPublishedRunSetup($param)
    {
        $ans = new Answer();
        $reason = "";

        try {
            $this->log->debug("RunSetup->getRunSetup()");
            $this->log->debug("RunSetup->param");
            $this->log->debug($param);


            $runsetup = $this->db->getPublishedRunsetup($param->id);
            $this->log->debug($runsetup);
            $string = file_get_contents($runsetup->path . DIRECTORY_SEPARATOR . $runsetup->filename);

            $json_a = json_decode($string, true);
            $this->log->debug($json_a);

            $ans->setResult($json_a);

            $reason = "Security through obscurity";
            $ans->setOkAction();


        } catch (\Exception $e) {
            $reason = $e->getMessage();
            $ans->setFailAction();

        }
        $ans->setReason($reason);
        return $ans;
    }

    public function publishRunsetup($param)
    {
        $ans = new Answer();
        $reason = "";

        try {
            $this->log->debug("RunSetup->movePublishedRunsetupToDraft()");
            $this->log->debug("RunSetup->param");
            $this->log->debug($param);


            $runsetup = $this->db->getDraftRunsetup($param->id);
            $this->db->deleteDraftRunsetupFromDB($param->id);
            $this->log->debug($runsetup);

            $path = $runsetup->path . DIRECTORY_SEPARATOR . $runsetup->filename;

            $newpath = $this->runsetupPublishedDirectory . DIRECTORY_SEPARATOR . $runsetup->filename;

            if (!rename($path, $newpath)) {
                throw new CantWriteToFileException("can't move the file");
            }
            $this->db->insertPublishedRunsetupIntoDB($runsetup->description,
                $this->RunsetupPublishedDirectory,
                $runsetup->filename,
                $runsetup->detector_id);

            $reason = "This is a fake feature... maybe a day we will be joyful with the real feature, let's to reimplement it, the reimplementation is the way!";
            $ans->setOkAction();


        } catch (\Exception $e) {
            $reason = $e->getMessage();
            $ans->setFailAction();

        }
        $ans->setReason($reason);
        return $ans;

    }

    public function movePublishedRunsetupToDraft($param)
    {
        $ans = new Answer();
        $reason = "";

        try {
            $this->log->debug("RunSetup->movePublishedRunsetupToDraft()");
            $this->log->debug("RunSetup->param");
            $this->log->debug($param);


            $runsetup = $this->db->getPublishedRunsetup($param->id);
            $this->db->deletePublishedRunsetupFromDB($param->id);
            $this->log->debug($runsetup);

            $path = $runsetup->path . DIRECTORY_SEPARATOR . $runsetup->filename;

            $newpath = $this->runsetupDraftDirectory . DIRECTORY_SEPARATOR . $runsetup->filename;

            if (!rename($path, $newpath)) {
                throw new CantWriteToFileException("can't move the file");
            }
            $this->db->insertDraftRunsetupIntoDB($runsetup->description,
                $this->runsetupDraftDirectory,
                $runsetup->filename,
                $runsetup->detector_id,
                $this->userdata->user_id);

            $reason = "This is a fake feature... maybe a day we will be joyful with the real feature, let's to reimplement it, the reimplementation is the way!";
            $ans->setOkAction();

        } catch (\Exception $e) {
            $reason = $e->getMessage();
            $ans->setFailAction();

        }
        $ans->setReason($reason);
        return $ans;

    }

    public function getDetectorList()
    {
        $ans = new Answer();
        $reason = "";

        try {
            $this->log->debug("RunSetup->getDetectorList()");
            $this->log->debug("RunSetup->getDetectorList() CWD:" . getcwd());
            $this->syncDataAndDB($this->detectorsDirectory,
                $this->db->getDetectors(),
                'insertDetectorIntoDB',
                'deleteDetectorFromDB');

            $ans->setResult($this->db->getDetectors());

            $reason = "This is a fake feature... Don't touch my red cables, God bless the red cables";
            $ans->setOkAction();


        } catch (\Exception $e) {
            $reason = $e->getMessage();
            $ans->setFailAction();

        }
        $ans->setReason($reason);
        return $ans;
    }

    public function uploadRunsetup($param)
    {
        $ans = new Answer();

        try {
            $this->log->debug("RunSetup->uploadRunsetup()");

            if (count($_FILES) > 1) {
                throw new \RuntimeException("you sent too many files");
            }

            switch ($_FILES['file']['error']) {
                case UPLOAD_ERR_OK:
                    break;
                case UPLOAD_ERR_NO_FILE:
                    throw new \RuntimeException('No file sent.');
                case UPLOAD_ERR_INI_SIZE:
                case UPLOAD_ERR_FORM_SIZE:
                    throw new \RuntimeException('Exceeded filesize limit.');
                default:
                    throw new \RuntimeException('Unknown errors during Upload file');
            }

            // DO NOT TRUST $_FILES['upfile']['mime'] VALUE !!
            // Check MIME Type by yourself.
            $finfo = new \finfo(FILEINFO_MIME_TYPE);

            $this->log->debug($finfo->file($_FILES['file']['tmp_name']));

            if (false === $ext = array_search(
                    $finfo->file($_FILES['file']['tmp_name']),
                    array(
                        'json' => 'text/plain',

                    ),
                    true
                )
            ) {
                throw new \RuntimeException('Invalid file format: ' . $_FILES['file']['type']);
            }

            if ($_FILES['file']['type'] != "application/json") {
                throw new \RuntimeException('Invalid file format: ' . $_FILES['file']['type']);
            }


            $scanned_directory = $array = array_values(array_diff(scandir($this->RunsetupPublishedDirectory), array('..', '.')));

            $filename = $_FILES['file']['name'];
            $tmp_filename = $_FILES['file']['tmp_name'];

            if (in_array($filename, $scanned_directory)) {
                $filename = $filename . "1";
            }
            $dest = $this->RunsetupPublishedDirectory . "/" . $filename;
            if (!move_uploaded_file($tmp_filename, $dest)) {
                throw \RuntimeException('Failed to move uploaded file.');
            }
            $reason = "File Saved sucessfully!";

            $ans->setOkAction();

        } catch (\Exception $e) {
            $reason = $e->getMessage();
            $ans->setFailAction();

        }
        $ans->setReason($reason);
        return $ans;

    }


    public function updateDraftRunsetupDescription($param)
    {
        $ans = new Answer();
        $reason = "";
        try {
            $this->log->debug("RunSetup->updateDraftRunsetupDescription()");
            $this->log->debug("RunSetup->param");
            $this->log->debug($param);

            $this->db->updateDraftRunsetupDescription($param->id, $param->description);
            $reason = "This is a fake feature... maybe a day we will be joyful with the real feature, god bless the Datamanager";
            $ans->setOkAction();
        } catch (\Exception $e) {
            $reason = $e->getMessage();
            $ans->setFailAction();

        }
        $ans->setReason($reason);
        return $ans;
    }

    public function updateDraftRunsetupDetector($param)
    {
        $ans = new Answer();
        $reason = "";

        try {
            $this->log->debug("RunSetup->updateDraftRunsetupDetector()");
            $this->log->debug("RunSetup->param");
            $this->log->debug($param);
            $reason = "This is a fake feature... your update make me hot...";

            $this->db->updateDraftRunsetupDetector($param->id, $param->detectorId);

        } catch (\Exception $e) {
            $reason = $e->getMessage();
            $ans->setFailAction();

        }
        $ans->setReason($reason);
        return $ans;
    }

    public function updateDraftRunsetupContent($param)
    {

        $ans = new Answer();
        $reason = "";

        try {
            $this->log->debug("RunSetup->updateDraftRunsetupContent()");
            $this->log->debug("RunSetup->param");
            $this->log->debug($param);
            $runsetup = $this->db->getDraftRunsetup($param->id);
            $ret = file_put_contents($runsetup->path . DIRECTORY_SEPARATOR . $runsetup->filename,
                json_encode($param->data));
            if ($ret == 0)
                throw new CantWriteToFileException();
            $this->log->debug("userid: " .$this->userdata->user_id);
            $this->db->updateRunsetupDraftUser($param->id, $this->userdata->user_id);
            $this->db->updateRunsetupDraftTimestamp($param->id);
            $this->db->updateDraftRunsetupDescription($param->id, $param->description);

            $reason = "Runsetup updated Successfully, incredibly it works... even if Favaro has touched it :)";
            $ans->setOkAction();

        } catch (\Exception $e) {
            $reason = $e->getMessage();
            $ans->setFailAction();

        }
        $ans->setReason($reason);
        return $ans;

    }
    public function cloneDraftRunsetup($param)
    {
        $ans = new Answer();
        $reason = "";

        try {
            $this->log->debug("RunSetup->cloneDraftRunsetup()");
            $this->log->debug("RunSetup->param");
            $this->log->debug($param);
            $runsetup = $this->db->getDraftRunsetup($param->id);
            $newname = $this->db->getRunsetupName();
            $this->db->insertDraftRunsetupIntoDB(
                $runsetup->description,
                $this->runsetupDraftDirectory,
                $newname,
                $runsetup->detector_id,
                $this->userdata->user_id
            );
            $string = file_get_contents($runsetup->path . DIRECTORY_SEPARATOR . $runsetup->filename);
            $json_a = json_decode($string, true);

            $ret = file_put_contents($runsetup->path . DIRECTORY_SEPARATOR . $newname,
                json_encode($json_a));

            if ($ret == 0)
                throw new CantWriteToFileException();
            $reason = "Runsetup cloned Successfully, incredibly it works... It is amazing, this stuf works...";
            $ans->setOkAction();

        } catch (\Exception $e) {
            $reason = $e->getMessage();
            $ans->setFailAction();

        }
        $ans->setReason($reason);
        return $ans;

    }

    public function newDraftRunsetup($param)
    {

        $ans = new Answer();
        $reason = "";

        try {
            $this->log->debug("RunSetup->updateDraftRunsetupContent()");
            $this->log->debug("RunSetup->param");
            $newname = $this->db->getRunsetupName();
            $runsetup = $this->db->insertDraftRunsetupIntoDB(
                $param->description,
                $this->runsetupDraftDirectory,
                $newname,
                $param->detectorId,
                $this->userdata->user_id
                );
            $ret = file_put_contents($runsetup->path . DIRECTORY_SEPARATOR . $newname,
                json_encode($param->data));
            if ($ret == 0)
                throw new CantWriteToFileException();

            $reason = "Runsetup updated Successfully";
            $ans->setOkAction();

        } catch (\Exception $e) {
            $reason = $e->getMessage();
            $ans->setFailAction();

        }
        $ans->setReason($reason);
        return $ans;

    }



    private function syncDataAndDB($directory, $data, $insertcallback, $deletecallback)
    {
        $this->log->debug("RunSetup->syncDataAndDB()");

        $scanned_directory = array_values(array_diff(scandir($directory),
            array('..', '.')
        ));
        $this->log->debug("scanned stuff '$directory':");
        $this->log->debug($scanned_directory);

        $this->log->debug("Loaded stuff:");
        $this->log->debug($data);

        foreach ($scanned_directory as $filename) {

            $dbobjects = array_filter(
                $data,
                function ($e) use ($filename) {
                    $this->log->debug($e->filename);
                    $this->log->debug($filename);

                    return $e->filename == $filename;
                }
            );

            $this->log->debug("dbobject count: " . count($dbobjects));


            if (empty($dbobjects)) {
                $this->log->debug("dbobject empty inserting $filename");
                $this->db->$insertcallback(
                    $this->filenameToDescription($filename),
                    $directory,
                    $filename,
                    1,
                    -1);
            }
        }
        foreach ($data as $dbobject) {
            $duplicates = array_filter($data,
                function ($e) use ($dbobject) {
                    return $e->filename == $dbobject->filename;
                });
            if(count($duplicates) > 1) {
                    $newest = $duplicates[0];
                    foreach ($duplicates as $entry) {
                        if($entry->creation_timestamp > $newest->creation_timestamp ) {
                            $newest = $entry;
                        }
                    }
                    $toremove = array_filter(
                        $duplicates,
                        function ($e) use ($newest) {
                            return $e->id != $newest->id;
                        });
                    $this->log->debug("toremove count: ". count($toremove));
                    foreach ($toremove as $item) {
                        $this->log->debug("removing duplicates runsetup $item->id");
                        $this->db->$deletecallback($item->id);

                    }
                }
            }


        $this->log->debug("RunSetup->syncDataAndDB() END");

    }
    
    private function filenameToDescription($filename)
    {
        $elements = explode("_", $filename);
        $ret = $filename;
        if (count($elements) > 0) {
            $ret = "name: " . $elements[0] . "\n";
            unset($elements[0]);
            foreach ($elements as $k => $v) {
                if ($k % 2) {
                    $ret = $ret . $elements[$k] . ":";
                } else {
                    $ret = $ret . $elements[$k] . "\n";
                }
            }
        }
        return $ret;
    }
}