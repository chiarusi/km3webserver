<?php
namespace TSCWebServer;


use \PDO;

// I HATE THIS KIND OF USE SQL WITH STRING!!!!!!
// I WANT TO USE CRUD METHOD

class SQLiteWrapper extends \PDO
{
    private $defaultSessionDuration
        = 60 * 60 * 24
        * 30; //default duration = 60 * 60 * 24 * 30 = 2592000 ~1 month
    private $defaultStateHistory = 30; //default duration = 30 sec
    private $defaultPriviledgeDurantion = 60 * 15; //default duration = 15 min
    private $forceEnablingTime = 60; // 60 sec -> 1 min
    private $autoPriviledgeGrantingTimeout
        = 5
        * 2; // 120 sec -> 2 min from the timerequest

    protected $headers = array();

    protected $config;

    protected $log;

    protected $pdo; // PDO

    public function __construct(\Slim\Log $log,
                                $dbDir
    )
    {
        parent::__construct("sqlite:" . $dbDir . "/storage/webserver.db");
        $this->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
        $this->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $this->log = $log;
        $this->log->debug("DB Wrapper Construct");

        $this->log->debug("DB Wrapper Construct: " . $dbDir);
        $this->log->debug(
            "DB Wrapper Construct: " . $dbDir . "/storage/webserver.db "
        );

        $this->log->info("DB costructed successfully");

    }

    //TRUE/FALSE
    public function matches($username,
                            $password
    )
    {
        try {
            $this->log->debug("SQLiteWrapper.matches:" . $username);

            $password = hash("sha256", $password);
            $this->log->debug("hashed password->" . $password);
            $query = "SELECT COUNT(*) AS count " .
                "FROM USERS " .
                "WHERE username = :username " .
                "AND password = :password;";
            $stmnt = $this->prepare($query);
            $stmnt->bindValue(':username', $username);
            $stmnt->bindValue(':password', $password);
            if (!$stmnt->execute()) {
                throw new \Exception(implode(" ", $stmnt->errorInfo()));
            }
            $resultData = $stmnt->fetch();
            $stmnt->closeCursor();

            $this->log->debug("SQLiteWrapper.matches count:" . $resultData->count);

            return $resultData->count == 1;

        } catch (\Exception $e) {
            $this->log->critical("SQLiteWrapper.matches:" . $e->getMessage());
            throw $e;
        }
    }

    //TRUE/FALSE
    public function checkUserAlreadyLogged($username)
    {
        try {
            $this->log->debug('SQLiteWrapper.checkUserAlreadyLogged' . $username);

            $query = "SELECT COUNT(*) AS count " .
                "FROM SESSIONS " .
                "INNER JOIN USERS ON USERS.id = user_id " .
                "WHERE USERS.username = :username;";

            $stmnt = $this->prepare($query);
            $stmnt->bindValue(':username', $username);
            if (!$stmnt->execute()) {
                throw new \Exception(implode(" ", $stmnt->errorInfo()));
            }
            $resultData = $stmnt->fetch();
            $stmnt->closeCursor();

            $this->log->debug(
                "SQLiteWrapper.checkUserAlreadyLogged count:" . $resultData->count
            );

            return $resultData->count >= 1;

        } catch (\Exception $e) {
            $this->log->critical(
                "SQLiteWrapper.checkUserAlreadyLogged:" . $e->getMessage()
            );
            throw $e;
        }
    }

    public function resetUserSession($username)
    {
        try {
            $this->log->debug('SQLiteWrapper.resetUserSession');

            $user_id = $this->getUserID($username)->id;

            $query = "DELETE " .
                "FROM SESSIONS " .
                "WHERE user_id == :user_id;";

            $stmnt = $this->prepare($query);
            $stmnt->bindValue(':user_id', $user_id);
            if (!$stmnt->execute()) {
                throw new \Exception(implode(" ", $stmnt->errorInfo()));
            }

            $stmnt->closeCursor();


        } catch (Exception $e) {
            $this->log->critical(
                "SQLiteWrapper.resetUserSession: " . $e->getMessage()
            );
            throw $e;
        }
    }

    public function getRole($username)
    {
        try {
            $this->log->debug('SQLiteWrapper.getRole');

            $query = "SELECT ROLES.* " .
                "FROM ROLES " .
                "INNER JOIN USERS ON USERS.user_role = ROLES.id " .
                "WHERE USERS.username = :username;";

            $stmnt = $this->prepare($query);
            $stmnt->bindValue(':username', $username);
            if (!$stmnt->execute()) {
                throw new \Exception(implode(" ", $stmnt->errorInfo()));
            }

            $resultData = $stmnt->fetch();
            $stmnt->closeCursor();


            return $resultData->role_name;

        } catch (Exception $e) {
            $this->log->critical("getRole:" . $e->getMessage());
            throw $e;
        }


    }

    public function getUserData($username)
    {
        try {
            $this->log->debug('SQLiteWrapper.getUserData');
            $query = "SELECT USERS.id as id, USERS.*, ROLES.*, SESSIONS.* " .
                "FROM USERS " .
                "INNER JOIN ROLES ON USERS.user_role = ROLES.id " .
                "LEFT JOIN SESSIONS ON SESSIONS.user_id = USERS.id " .
                "WHERE USERS.username = :username;";

            $stmnt = $this->prepare($query);
            $stmnt->bindValue(':username', $username);
            $this->log->debug("SQLiteWrapper.getUserData:" . $stmnt->queryString);
            if (!$stmnt->execute()) {
                throw new \Exception(implode(" ", $stmnt->errorInfo()));
            }
            $resultData = $stmnt->fetch();
            $stmnt->closeCursor();

            $this->log->debug('getUserData username:' . $username);

            if (!$resultData) {
                return $this->createEmptyUserData();
            } else {
                $resultData->UserEmpty = false;
                return $resultData;
            }
        } catch (Exception $e) {
            $this->log->critical("SQLiteWrapper.getUserData:" . $e->getMessage());
            throw $e;
        }
    }


    public function getPriviledgedUserData()
    {
        try {

            $this->log->debug('SQLiteWrapper.getPriviledgedUserData');

            $query
                = "SELECT USERS.id as id, USERS.*, ROLES.*, SESSIONS.*, PRIVILEDGE.* " .
                "FROM PRIVILEDGE " .
                "INNER JOIN USERS ON USERS.id = PRIVILEDGE.user_id " .
                "INNER JOIN ROLES ON USERS.user_role = ROLES.id " .
                "INNER JOIN SESSIONS ON SESSIONS.user_id = USERS.id " .
                "WHERE PRIVILEDGE.priviledge_expiration > :now_timestamp " .
                "OR ROLES.priviledge_will_expire = :will_expire " .
                "LIMIT 1;";

            $stmnt = $this->prepare($query);
            $stmnt->bindValue(':now_timestamp', time());
            $stmnt->bindValue(':will_expire', 0);
            if (!$stmnt->execute()) {
                throw new \Exception(implode(" ", $stmnt->errorInfo()));
            }
            $resultData = $stmnt->fetch();
            $stmnt->closeCursor();

            if (!$resultData) {
                return $this->createEmptyUserData();
            } else {
                $resultData->UserEmpty = false;
                return $resultData;
            }
        } catch (Exception $e) {
            $this->log->critical(
                "SQLiteWrapper.getPriviledgedUserData:" . $e->getMessage()
            );
            throw $e;
        }
    }

    public function getEscalatingUserData()
    {
        try {
            $this->log->debug("SQLiteWrapper.getEscalatingUserData()");
            $query
                = "SELECT USERS.id,
                 USERS.name,
                 USERS.username,
                 ROLES.role_name,
                 ROLES.isAdmin,
                 ESCALATION.request_time,
                 ESCALATION.priviledge_grant_time,
                 ESCALATION.force_enabling_time
                FROM ESCALATION 
                INNER JOIN USERS ON USERS.id = ESCALATION.user_id 
                INNER JOIN ROLES ON USERS.user_role = ROLES.id
                LIMIT 1;";

            $stmnt = $this->prepare($query);
            if (!$stmnt->execute()) {
                throw new \Exception(implode(" ", $stmnt->errorInfo()));
            }
            $resultData = $stmnt->fetch();
            $stmnt->closeCursor();

            if (!$resultData) {
                return $this->createEmptyUserData();
            } else {
                $resultData->UserEmpty = false;
                return $resultData;
            }
        } catch (Exception $e) {
            $this->log->critical(
                "SQLiteWrapper.getEscalatingUserData:" . $e->getMessage()
            );
            throw $e;
        }
    }

    public function getUserDataFromToken($token)
    {
        try {

            $this->log->debug("SQLiteWrapper.getUserDataFromToken()");

            $query = "SELECT USERS.id as id, USERS.*, ROLES.*, SESSIONS.* " .
                "FROM USERS " .
                "INNER JOIN ROLES ON USERS.user_role = ROLES.id " .
                "INNER JOIN SESSIONS ON SESSIONS.user_id = USERS.id " .
                "WHERE SESSIONS.token = :token " .

                "LIMIT 1;";

            $stmnt = $this->prepare($query);
            $stmnt->bindValue(':token', $token);

            if (!$stmnt->execute()) {
                throw new \Exception(implode(" ", $stmnt->errorInfo()));
            }
            $resultData = $stmnt->fetch();
            $stmnt->closeCursor();

            if (!$resultData) {
                return $this->createEmptyUserData();
            } else {
                return $resultData;
            }
        } catch (\Exception $e) {
            $this->log->critical(
                "SQLiteWrapper.getUserDataFromToken:" . $e->getMessage()
            );
            throw $e;
        }

    }

    public function createAuthToken($username)
    {
        try {
            $this->log->debug("SQLiteWrapper.createAuthToken($username)");

            $token = md5(uniqid(mt_rand(), true));

            $userID = $this->getUserID($username)->id;

            $query = "INSERT INTO SESSIONS " .
                "( user_id, token, expire ) " .
                "VALUES ( :user_id, :token, :expire);";

            $stmnt = $this->prepare($query);
            $stmnt->bindValue(':user_id', $userID);
            $stmnt->bindValue(':token', $token);
            $stmnt->bindValue(':expire', time() + $this->defaultSessionDuration);

            if (!$stmnt->execute()) {
                throw new \Exception(implode(" ", $stmnt->errorInfo()));
            }
            $stmnt->closeCursor();

            $this->log->debug('SQLiteWrappercreateAuthToken token:' . $token);
            return $token;
        } catch (\Exception $e) {
            $this->log->critical("SQLiteWrapper.createAuthToken:" . $e->getMessage());
            throw $e;
        }

    }

    public function renewSession($token)
    {

        try {
            $this->log->debug("SQLiteWrapper.renewSession($token)");

            $query = "UPDATE SESSIONS " .
                "SET expire = :expire " .
                "WHERE token = :token ;";

            $stmnt = $this->prepare($query);
            $this->log->debug(
                "SQLiteWrapper.renewSession($token):$stmnt->queryString"
            );
            $stmnt->bindValue(':token', $token);
            $stmnt->bindValue(':expire', time() + $this->defaultSessionDuration);
            if (!$stmnt->execute()) {
                throw new \Exception(implode(" ", $stmnt->errorInfo()));
            }
            $stmnt->closeCursor();

        } catch (\Exception $e) {
            $this->log->critical("SQLiteWrapper.renewSession:" . $e->getMessage());
            throw $e;
        }
    }

    public function checkToken($token)
    {
        try {
            $this->log->debug("SQLiteWrapper.checkToken($token)");
            $query = "SELECT * " .
                "FROM SESSIONS " .
                "WHERE SESSIONS.token = :token;";

            $stmnt = $this->prepare($query);
            $stmnt->bindValue(':token', $token);

            if (!$stmnt->execute()) {
                throw new \Exception(implode(" ", $stmnt->errorInfo()));
            }
            $resultData = $stmnt->fetch();
            $stmnt->closeCursor();

            if ($resultData) {
                if (!$this->getRoleFromToken($token)->session_will_expire) {
                    $this->log->debug(
                        "SQLiteWrapper.checkToken: datamanager is logging in"
                    );
                    return true;
                } else {
                    $now = time();
                    $this->log->debug(
                        "SQLiteWrapper.checkToken: Token Expired?:" . $resultData->expire
                        . " >= " . $now
                    );
                    return $resultData->expire >= $now;
                }
            }

            return false;

        } catch (\Exception $e) {
            $this->log->critical(
                "SQLiteWrapper.checkToken Exception:" . $e->getMessage()
            );
            throw $e;
        }
    }

    //TRUE/FALSE
    public function tokenExist($token)
    {
        try {
            $this->log->debug("SQLiteWrapper.tokenExist($token)");
            $query = "SELECT COUNT(*) AS count " .
                "FROM SESSIONS " .
                "WHERE SESSIONS.token = :token;";

            $stmnt = $this->prepare($query);
            $stmnt->bindValue(':token', $token);

            if (!$stmnt->execute()) {
                throw new \Exception(implode(" ", $stmnt->errorInfo()));
            }
            $resultData = $stmnt->fetch();
            $stmnt->closeCursor();

            return $resultData->count != 0;

        } catch (\Exception $e) {
            $this->log->critical("SQLiteWrapper.tokenExist:" . $e->getMessage());
            throw $e;
        }
    }

    public function invalidateToken($userData,
                                    $token
    )
    {
        try {
            $this->log->debug("SQLiteWrapper.invalidateToken($userData->id, $token)");
            $query = "DELETE " .
                "FROM SESSIONS " .
                "WHERE SESSIONS.token = :token " .
                "AND " .
                "SESSIONS.user_id = :user_ID;";

            $stmnt = $this->prepare($query);
            $stmnt->bindValue(':token', $token);
            $stmnt->bindValue(':user_ID', $userData->id);

            if (!$stmnt->execute()) {
                throw new \Exception(implode(" ", $stmnt->errorInfo()));
            }
            $stmnt->closeCursor();
            return true;
        } catch (\Exception $e) {
            $this->log->critical("SQLiteWrapper.invalidateToken:" . $e->getMessage());
            throw $e;
        }
    }

    public function getRoleFromToken($token)
    {
        try {
            $this->log->debug("SQLiteWrapper.getRoleFromToken($token)");
            $query = "SELECT ROLES.* " .
                "FROM ROLES " .
                "INNER JOIN USERS ON USERS.user_role = ROLES.id " .
                "INNER JOIN SESSIONS ON SESSIONS.user_id = USERS.id " .
                "WHERE SESSIONS.token = :token " .
                "LIMIT 1;";

            $stmnt = $this->prepare($query);
            $stmnt->bindValue(':token', $token);

            if (!$stmnt->execute()) {
                throw new \Exception(implode(" ", $stmnt->errorInfo()));
            }
            $resultData = $stmnt->fetch();
            $stmnt->closeCursor();

            return $resultData;

        } catch (\Exception $e) {
            $this->log->critical(
                "SQLiteWrapper.getRoleFromToken:" . $e->getMessage()
            );
            throw $e;
        }


    }

    public function getTokenFromUsername($username)
    {
        try {
            $this->log->debug("SQLiteWrapper.getTokenFromUsername($username)");

            $query = "SELECT SESSION.token " .
                "FROM SESSIONS " .
                "INNER JOIN USERS ON SESSIONS.user_id = USERS.id " .
                "WHERE USERS.username = :username " .
                "LIMIT 1;";

            $stmnt = $this->prepare($query);
            $stmnt->bindValue(':username', $username);

            if (!$stmnt->execute()) {
                throw new \Exception(implode(" ", $stmnt->errorInfo()));
            }
            $resultData = $stmnt->fetch();
            $stmnt->closeCursor();

            return $resultData;

        } catch (\Exception $e) {
            $this->log->critical(
                "SQLiteWrapper.getTokenFromUsername:" . $e->getMessage()
            );
            throw $e;
        }

    }

    public function getCurrentState()
    {
        try {
            $this->log->debug('SQLiteWrapper.getCurrentState()');
            $query = "SELECT state " .
                "FROM CURRENT_STATE ;";
            $stmnt = $this->prepare($query);

            if (!$stmnt->execute()) {
                throw new \Exception(implode(" ", $stmnt->errorInfo()));
            }
            $resultData = $stmnt->fetch();
            $stmnt->closeCursor();
            $this->log->debug($resultData);
            $this->log->debug('SQLiteWrapper.getCurrentState() END');

            return $resultData;

        } catch (\Exception $e) {
            $this->log->error("SQLiteWrapper.getCurrentState:" . $e->getMessage());
            throw $e;
        }
    }

    public function getPublishedRunsetupList()
    {
        try {
            $this->log->debug('SQLiteWrapper.getPublishedRunsetupList()');
            $query = "SELECT * " .
                "FROM PUBLISHED_RUNSETUP ;";
            $stmnt = $this->prepare($query);

            if (!$stmnt->execute()) {
                throw new \Exception(implode(" ", $stmnt->errorInfo()));
            }
            $resultData = $stmnt->fetchAll();
            $stmnt->closeCursor();
            $this->log->debug($resultData);
            $this->log->debug('SQLiteWrapper.getPublishedRunsetupList() END');
            if (empty($resultData)) $resultData = array();

            foreach ($resultData as $item) {
                $item->state = "published";
            }

            return $resultData;

        } catch (\Exception $e) {
            $this->log->error("SQLiteWrapper.getPublishedRunsetupList:" . $e->getMessage());
            throw $e;
        }
    }
    public function getDraftRunsetupList()
    {
        try {
            $this->log->debug('SQLiteWrapper.getDraftRunsetupList()');
            $query = "SELECT DRAFT_RUNSETUP.*, USERS.name as name " .
                "FROM  DRAFT_RUNSETUP " .
                "LEFT OUTER JOIN USERS " .
                "ON USERS.id = user_id ;";

            $stmnt = $this->prepare($query);

            if (!$stmnt->execute()) {
                throw new \Exception(implode(" ", $stmnt->errorInfo()));
            }
            $resultData = $stmnt->fetchAll();
            $stmnt->closeCursor();
            //$this->log->debug($resultData);
            $this->log->debug('SQLiteWrapper.getDraftRunsetupList() END');
            if (empty($resultData)) $resultData = array();

            foreach ($resultData as $item) {
                $item->state = "draft";
            }

            return $resultData;

        } catch (\Exception $e) {
            $this->log->error("SQLiteWrapper.getDraftRunsetupList:" . $e->getMessage());
            throw $e;
        }
    }

    public function deleteAllDraftFromDB() {
        try {
            $this->log->debug('SQLiteWrapper.deleteAllDraftFromDB()');
            $query = "SELECT DRAFT_RUNSETUP.*, USERS.name as name " .
                "FROM  DRAFT_RUNSETUP " .
                "LEFT OUTER JOIN USERS " .
                "ON USERS.id = user_id ;";

            $stmnt = $this->prepare($query);

            if (!$stmnt->execute()) {
                throw new \Exception(implode(" ", $stmnt->errorInfo()));
            }
            $resultData = $stmnt->fetchAll();
            $stmnt->closeCursor();
            $this->log->debug($resultData);
            $this->log->debug('SQLiteWrapper.deleteAllDraftFromDB() END');
            if (empty($resultData)) $resultData = array();

            foreach ($resultData as $item) {
                $item->state = "draft";
            }

            return $resultData;

        } catch (\Exception $e) {
            $this->log->error("SQLiteWrapper.deleteAllDraftFromDB:" . $e->getMessage());
            throw $e;
        }
    }
    public function getPublishedRunsetup($id)
    {
        try {
            $this->log->debug('SQLiteWrapper.getPublishedRunsetup()');
            $query = "SELECT * " .
                "FROM PUBLISHED_RUNSETUP " .
                "WHERE id = :id;";
            $stmnt = $this->prepare($query);
            $stmnt->bindValue(':id', $id);

            if (!$stmnt->execute()) {
                throw new \Exception(implode(" ", $stmnt->errorInfo()));
            }
            $resultData = $stmnt->fetch();
            $stmnt->closeCursor();
            $this->log->debug($resultData);
            $this->log->debug('SQLiteWrapper.getPublishedRunsetup() END');
            if (empty($resultData)) $resultData = array();
            return $resultData;

        } catch (\Exception $e) {
            $this->log->error("SQLiteWrapper.getPublishedRunsetup:" . $e->getMessage());
            throw $e;
        }
    }




    public function getDraftRunsetup($id)
    {
        try {
            $this->log->debug('SQLiteWrapper.getDraftRunsetup()');
            $query = "SELECT * " .
                "FROM DRAFT_RUNSETUP as dr " .
                "WHERE id = :id;";
            $stmnt = $this->prepare($query);
            $stmnt->bindValue(':id', $id);

            if (!$stmnt->execute()) {
                throw new \Exception(implode(" ", $stmnt->errorInfo()));
            }
            $resultData = $stmnt->fetch();
            $stmnt->closeCursor();
            $this->log->debug($resultData);
            $this->log->debug('SQLiteWrapper.getDraftRunsetup() END');
            if (empty($resultData)) $resultData = array();
            return $resultData;

        } catch (\Exception $e) {
            $this->log->error("SQLiteWrapper.getDraftRunsetup:" . $e->getMessage());
            throw $e;
        }
    }

    public function insertPublishedRunsetupIntoDB($description,
                                                  $path,
                                                  $runsetupfilename,
                                                  $detectorId = 1,
                                                  $user_id)
    {
        try {
            $this->log->debug("SQLiteWrapper.insertPublishedRunsetupIntoDB");


            $query = "INSERT INTO PUBLISHED_RUNSETUP (description, path, filename, detector_id, user_id, creation_timestamp) " .
                "VALUES(:description, :path, :filename, :detector_id, :user_id, :creation_timestamp);";

            $stmnt = $this->prepare($query);

            $stmnt->bindValue(':description', $description);
            $stmnt->bindValue(':path', $path);
            $stmnt->bindValue(':filename', $runsetupfilename);
            $stmnt->bindValue(':detector_id', $detectorId);
            $stmnt->bindValue(':user_id', $user_id);
            $stmnt->bindValue(':creation_timestamp', time());

            if (!$stmnt->execute()) {
                throw new \Exception(implode(" ", $stmnt->errorInfo()));
            }
            $stmnt->closeCursor();
            return true;

        } catch (\Exception $e) {
            $this->log->critical("SQLiteWrapper.insertPublishedRunsetupIntoDB:" . $e->getMessage());
            throw $e;
        }
    }
    public function insertDraftRunsetupIntoDB($description,
                                                  $path,
                                                  $runsetupfilename,
                                                  $detectorId = 1,
                                                  $user_id)
    {
        try {
            $this->log->debug("SQLiteWrapper.insertDraftRunsetupIntoDB");


            $query = "INSERT INTO DRAFT_RUNSETUP (description, path, filename, detector_id, user_id, creation_timestamp) " .
                "VALUES(:description, :path, :filename, :detector_id, :user_id, :creation_timestamp);";

            $stmnt = $this->prepare($query);

            $stmnt->bindValue(':description', $description);
            $stmnt->bindValue(':path', $path);
            $stmnt->bindValue(':filename', $runsetupfilename);
            $stmnt->bindValue(':detector_id', $detectorId);
            $stmnt->bindValue(':user_id', $user_id);
            $stmnt->bindValue(':creation_timestamp', time());

            if (!$stmnt->execute()) {
                throw new \Exception(implode(" ", $stmnt->errorInfo()));
            }
            $stmnt->closeCursor();
            return true;

        } catch (\Exception $e) {
            $this->log->critical("SQLiteWrapper.insertDraftRunsetupIntoDB:" . $e->getMessage());
            throw $e;
        }
    }


    public function deletePublishedRunsetupFromDB($id)
    {
        try {
            $this->log->debug("SQLiteWrapper.deletePublishedRunsetupFromDB id: " . $id);


            $query = "DELETE FROM PUBLISHED_RUNSETUP " .
                "WHERE id = :id ;";

            $stmnt = $this->prepare($query);

            $stmnt->bindValue(':id', $id);

            if (!$stmnt->execute()) {
                throw new \Exception(implode(" ", $stmnt->errorInfo()));
            }
            $stmnt->closeCursor();
            return true;
        } catch (\Exception $e) {
            $this->log->critical("SQLiteWrapper.deletePublishedRunsetupFromDB: " . $e->getMessage());
            throw $e;
        }
    }
    public function deleteDraftRunsetupFromDB($id)
    {
        try {
            $this->log->debug("SQLiteWrapper.deleteDraftRunsetupFromDB" . $id);


            $query = "DELETE FROM DRAFT_RUNSETUP " .
                "WHERE id = :id ;";

            $stmnt = $this->prepare($query);

            $stmnt->bindValue(':id', $id);

            if (!$stmnt->execute()) {
                throw new \Exception(implode(" ", $stmnt->errorInfo()));
            }
            $stmnt->closeCursor();
            return true;
        } catch (\Exception $e) {
            $this->log->critical("SQLiteWrapper.deleteDraftRunsetupFromDB:" . $e->getMessage());
            throw $e;
        }
    }

    public function updateDraftRunsetupDetector($runsetupid,
                                            $detectorId = -1)
    {
        try {
            $this->log->debug("SQLiteWrapper.updateDraftRunsetupDetector");

                $query = "UPDATE DRAFT_RUNSETUP " .
                    "SET detector_id=:detectorId " .
                    "WHERE id= :runsetupid ;";

            $stmnt = $this->prepare($query);
            $stmnt->bindValue(':runsetupid', $runsetupid);
            $stmnt->bindValue(':detectorId', $detectorId);

            if (!$stmnt->execute()) {
                throw new \Exception(implode(" ", $stmnt->errorInfo()));
            }
            $stmnt->closeCursor();
            return true;

        } catch (\Exception $e) {
            $this->log->critical("SQLiteWrapper.updateDraftRunsetupDetector:" . $e->getMessage());
            throw $e;
        }
    }
    public function updateDraftRunsetupDescription($runsetupid,
                                            $description = "")
    {
        try {
            $this->log->debug("SQLiteWrapper.updateDraftRunsetupDescription");

            $query = "UPDATE DRAFT_RUNSETUP " .
                "SET description=:description " .
                "WHERE id= :runsetupid ;";

            $stmnt = $this->prepare($query);
            $stmnt->bindValue(':runsetupid', $runsetupid);
            $stmnt->bindValue(':description', $description);

            if (!$stmnt->execute()) {
                throw new \Exception(implode(" ", $stmnt->errorInfo()));
            }
            $stmnt->closeCursor();
            return true;

        } catch (\Exception $e) {
            $this->log->critical("SQLiteWrapper.updateDraftRunsetupDescription:" . $e->getMessage());
            throw $e;
        }
    }
    public function updateRunsetupDraftUser($runsetupid, $userid)
        {
            try {
                $this->log->debug("SQLiteWrapper.updateRunsetupDraftUser");

                $query = "UPDATE DRAFT_RUNSETUP " .
                    "SET user_id=:user_id " .
                    "WHERE id= :runsetupid ;";

                $stmnt = $this->prepare($query);
                $stmnt->bindValue(':runsetupid', $runsetupid);
                $stmnt->bindValue(':user_id', $userid);

                if (!$stmnt->execute()) {
                    throw new \Exception(implode(" ", $stmnt->errorInfo()));
                }
                $stmnt->closeCursor();
                $this->log->debug("SQLiteWrapper.updateRunsetupDraftUser END");

                return true;

            } catch (\Exception $e) {
                $this->log->critical("SQLiteWrapper.updateRunsetupDraftUser:" . $e->getMessage());
                throw $e;
            }
        }   
    public function updateRunsetupDraftTimestamp($runsetupid)
        {
            try {
                $this->log->debug("SQLiteWrapper.updateRunsetupDraftTimestamp");

                $query = "UPDATE DRAFT_RUNSETUP " .
                    "SET creation_timestamp=:creation_timestamp " .
                    "WHERE id= :runsetupid ;";

                $stmnt = $this->prepare($query);
                $stmnt->bindValue(':runsetupid', $runsetupid);
                $stmnt->bindValue(':creation_timestamp', time());

                if (!$stmnt->execute()) {
                    throw new \Exception(implode(" ", $stmnt->errorInfo()));
                }
                $stmnt->closeCursor();
                $this->log->debug("SQLiteWrapper.updateRunsetupDraftTimestamp END");

                return true;

            } catch (\Exception $e) {
                $this->log->critical("SQLiteWrapper.updateRunsetupDraftTimestamp:" . $e->getMessage());
                throw $e;
            }
        }
    public function getDetectors()
    {
        try {
            $this->log->debug('SQLiteWrapper.getPublishedDetectors()');
            $query = "SELECT * " .
                "FROM PUBLISHED_DETECTORS ;";
            $stmnt = $this->prepare($query);

            if (!$stmnt->execute()) {
                throw new \Exception(implode(" ", $stmnt->errorInfo()));
            }
            $resultData = $stmnt->fetchAll();
            $stmnt->closeCursor();
            $this->log->debug($resultData);
            $this->log->debug('SQLiteWrapper.getPublishedDetectors() END');
            if (empty($resultData)) $resultData = array();
            return $resultData;

        } catch (\Exception $e) {
            $this->log->error("SQLiteWrapper.getPublishedDetectors:" . $e->getMessage());
            throw $e;
        }
    }

    public function insertDetectorIntoDB($description,
                                          $directory,
                                          $detectorsFilename)
    {
        try {
            $this->log->debug("SQLiteWrapper.insertDetectorsIntoDB");


            $query = "INSERT INTO PUBLISHED_DETECTORS (description, path, filename, published_timestamp) " .
                "VALUES(:description, :path, :filename, :published_timestamp);";

            $stmnt = $this->prepare($query);

            $stmnt->bindValue(':description', $description);
            $stmnt->bindValue(':path', $directory);
            $stmnt->bindValue(':filename', $detectorsFilename);
            $stmnt->bindValue(':published_timestamp', time());

            if (!$stmnt->execute()) {
                throw new \Exception(implode(" ", $stmnt->errorInfo()));
            }
            $stmnt->closeCursor();
            return true;

        } catch (\Exception $e) {
            $this->log->critical("SQLiteWrapper.insertDetectorsIntoDB:" . $e->getMessage());
            throw $e;
        }
    }
    public function deleteDetectorFromDB($id)
    {
        try {
            $this->log->debug("SQLiteWrapper.deletePublishedDetectorsFromDB" . $id);


            $query = "DELETE FROM PUBLISHED_DETECTORS " .
                "WHERE id = :id ;";

            $stmnt = $this->prepare($query);

            $stmnt->bindValue(':id', $id);

            if (!$stmnt->execute()) {
                throw new \Exception(implode(" ", $stmnt->errorInfo()));
            }
            $stmnt->closeCursor();
            return true;
        } catch (\Exception $e) {
            $this->log->critical("SQLiteWrapper.deletePublishedDetectorsFromDB:" . $e->getMessage());
            throw $e;
        }
    }

    public function getRunsetupName()
    {
        try {
            $this->log->debug("SQLiteWrapper.getSetupName()");

            $query = "SELECT value, name  " .
                "FROM RUNSETUP_NAME;";

            $stmnt = $this->prepare($query);
            if (!$stmnt->execute()) {
                throw new \Exception(implode(" ", $stmnt->errorInfo()));
            }
            $resultData = $stmnt->fetch();
            $stmnt->closeCursor();

            $this->updateRunsetupName($resultData->value + 1);

            return $resultData->name.$resultData->value;

        } catch (\Exception $e) {
            $this->log->critical("SQLiteWrapper.getRunsetupName:" . $e->getMessage());
            throw $e;
        }
    }

    private function updateRunsetupName($newNumber)
    {
        try {
            $this->log->debug("SQLiteWrapper.updateRunsetupName($newNumber)");

            $query = "UPDATE RUNSETUP_NAME " .
                "SET value = :newvalue " .
                "WHERE id = 1;";

            $stmnt = $this->prepare($query);
            $stmnt->bindValue(':newvalue', $newNumber);
            if (!$stmnt->execute()) {
                throw new \Exception(implode(" ", $stmnt->errorInfo()));
            }
            $stmnt->closeCursor();;

        } catch (\Exception $e) {
            $this->log->critical("SQLiteWrapper.updateRunsetupName:" . $e->getMessage());
            throw $e;
        }

    }


    public function setStatus($data,
                              $newState)
    {
        try {
            $this->log->debug("SQLiteWrapper.setStatus($newState)");

            $lastState = $this->getCurrentState();
            $lastReason = array_values(
                array_filter(
                    $this->getStatus(),
                    function ($e) {
                        return $e->valueID == "infos:reason";
                    }
                )
            );
            if (count($lastReason)) {
                $lastReason = $lastReason[0];
            } else {
                $lastReason = "";
            }
            $query = "REPLACE INTO CURRENT_STATE (id, state) VALUES ";
            $query .= "(1,'" . $newState . "');";

            $query2
                = "REPLACE INTO CURRENT_STATE_OPTIONS (valueID, value, state) VALUES ";
            $query2 .= "('timestamp'," . time() . ",'" . $newState . "'),";

            if (isset($lastState->state)) {
                $this->log->debug(
                    'SQLiteWrapper.setStatus, current state: ' . $lastState->state
                    . " new state: " . $newState
                );
                //$this->log->debug(
                //  'SQLiteWrapper.setStatus, isset($currentState->state) -> YES!'
                //);

                if ($newState != $lastState->state) {
                    $query2 .= "('infos:" . $newState . "_timestamp'," . time()
                        . ",'none'),";
                } else {
                    $this->log->debug(
                        'SQLiteWrapper.setStatus, same status not updating timestamp'
                    );
                }
            } else {
                $this->log->debug(
                    'SQLiteWrapper.setStatus, $current->state not set ?!'
                );
            }

            $newdata = utils::getPlainArrayWithseparatorKey($data);
            $this->log->debug('SQLiteWrapper.setStatus last reason:');
            $this->log->debug($lastReason);

            //$this->log->debug('SQLiteWrapper.setStatus: data to insert');
            $this->log->debug($newdata);
            foreach ($newdata as $key => $value) {
                if (isset($lastState->state)
                    && isset($lastReason)
                    && $newState == $lastState->state
                    && $key == "infos:reason"
                    && ($lastReason->value != "" || $value != "")
                ) {
                    if (
                        ($newdata['lastCommand'] == 'state'
                            || $newdata['lastCommand'] == 'query')
                        && $newdata['exitStatus'] == 'true'
                    ) {
                        $this->log->debug(
                            'SQLiteWrapper.setStatus froma state or query keeping last reason:'
                            . $lastReason->value
                        );
                        $sqlvalue = "('" . $key . "','" . $lastReason->value
                            . "','" . $newState . "'),";
                    } else {

                        $this->log->debug(
                            'SQLiteWrapper.setStatus something went wrong inserting new reason: "'
                            . $value . '"'
                        );
                        $sqlvalue = "('" . $key . "','" . addslashes($value)
                            . "','" . $newState . "'),";
                    }

                } else {
                    $sqlvalue = "('" . $key . "','" . addslashes($value) . "','"
                        . $newState
                        . "'),";
                    $query2 .= $sqlvalue;
                }
                $query2 .= $sqlvalue;

            }
            $query2 = rtrim($query2, ",") . ";";
            //$this->dumpToLog($query2);

            $stmnt = $this->prepare($query);
            if (!$stmnt->execute()) {
                throw new \Exception(implode(" ", $stmnt->errorInfo()));
            }
            $stmnt->closeCursor();

            $stmnt = $this->prepare($query2);
            if (!$stmnt->execute()) {
                throw new \Exception(implode(" ", $stmnt->errorInfo()));
            }
            $stmnt->closeCursor();

            $this->log->debug("SQLiteWrapper.setStatus end");
            return true;
        } catch (\Exception $e) {
            $this->log->error("SQLiteWrapper.setStatus:" . $e->getMessage());
            throw $e;
        }

    }

    public function getStatus()
    {
        try {
            $this->log->debug('SQLiteWrapper.getStatus()');

            $query
                = "SELECT valueID, value FROM CURRENT_STATE_OPTIONS
                WHERE state IN ( SELECT state FROM CURRENT_STATE )";

            $stmnt = $this->prepare($query);
            if (!$stmnt->execute()) {
                throw new \Exception(implode(" ", $stmnt->errorInfo()));
            }
            $resultData = $stmnt->fetchAll();

            $stmnt->closeCursor();

            $query2
                = "SELECT * FROM CURRENT_STATE_OPTIONS WHERE valueID LIKE '%_timestamp%'";
            $stmnt = $this->prepare($query2);
            if (!$stmnt->execute()) {
                throw new \Exception(implode(" ", $stmnt->errorInfo()));
            }
            $resultData = array_merge($resultData, $stmnt->fetchAll());
            //$this->log->debug($resultData);

            return $resultData;

        } catch (\Exception $e) {
            $this->log->error("SQLiteWrapper.getStatus:" . $e->getMessage());
            throw $e;
        }

    }


    //TRUE/FALSE
    public function priviledgeUser($username)
    {
        try {
            $this->log->debug('SQLiteWrapper.priledgeUser(' . $username . ')');

            $userID = $this->getUserID($username)->id;
            try {
                $query = "DELETE " .
                    "FROM PRIVILEDGE;";
                $query1 = "DELETE " .
                    "FROM ESCALATION;";
                $query2
                    = "INSERT INTO PRIVILEDGE (user_id, priviledge_expiration, no_answer_count)"
                    .
                    "VALUES(:user_id, :priviledge_expiration, :no_answer_count);";

                $stmnt = $this->prepare($query);
                if (!$stmnt->execute()) {
                    throw new \Exception(implode(" ", $stmnt->errorInfo()));
                }
                $stmnt->closeCursor();

                $stmnt = $this->prepare($query1);
                if (!$stmnt->execute()) {
                    throw new \Exception(implode(" ", $stmnt->errorInfo()));
                }
                $stmnt->closeCursor();

                $stmnt = $this->prepare($query2);
                $stmnt->bindValue(':user_id', $userID);
                $stmnt->bindValue(
                    ':priviledge_expiration', time() + $this->defaultPriviledgeDurantion
                );
                $stmnt->bindValue(':no_answer_count', 0);

                if (!$stmnt->execute()) {
                    throw new \Exception(implode(" ", $stmnt->errorInfo()));
                }
                $stmnt->closeCursor();
                return true;

            } catch (\Exception $e) {
                $this->log->critical(
                    "SQLiteWrapper.priviledgeUser ROLLBACK:" . $e->getMessage()
                );
                throw $e;
            }
        } catch (\Exception $e) {
            $this->log->critical("SQLiteWrapper.priviledgeUser:" . $e->getMessage());
            return false;
        }
    }

//TRUE/FALSE
    public function releasePriviledge()
    {
        try {
            $this->log->debug('SQLiteWrapper.releasePriviledge()');

            $query = "DELETE " .
                "FROM PRIVILEDGE;";

            $stmnt = $this->prepare($query);
            if (!$stmnt->execute()) {
                $stmnt->closeCursor();
                throw new \Exception(implode(" ", $stmnt->errorInfo()));
            }
            $stmnt->closeCursor();

            //$query1 = "DELETE " .
            //  "FROM ESCALATION;";

            //$stmnt = $this->prepare($query1);
            //if (!$stmnt->execute())
            //  throw new \Exception(implode(" ", $stmnt->errorInfo()));
            //$stmnt->closeCursor();
            return true;

        } catch (\Exception $e) {
            $this->log->critical(
                "SQLiteWrapper.releasePriviledge:" . $e->getMessage()
            );
            return false;
        }
    }

    //TRUE/FALSE
    public function priviledgeAdmin($username)
    {
        try {
            $this->log->debug("SQLiteWrapper.priviledgeAdmin($username)");

            //$userID = $this->getUserID($username)->id;

            try {

                $this->releasePriviledge();
                $this->priviledgeUser($username);
                return true;

            } catch (\Exception $e) {
                $this->log->critical(
                    "SQLiteWrapper.priviledgeAdmin: ROLLBACK" . $e->getMessage()
                );
                throw $e;

            }
        } catch (\Exception $e) {
            $this->log->critical("SQLiteWrapper.priviledgeAdmin:" . $e->getMessage());
            return false;
        }
    }

    public function startEscalation($username,
                                    $isAdmin
    )
    {
        try {
            $this->log->debug("SQLiteWrapper.startEscalation($username)");

            $userID = $this->getUserID($username)->id;

            $query = "DELETE " .
                "FROM ESCALATION;";

            $stmnt = $this->prepare($query);
            if (!$stmnt->execute()) {
                throw new \Exception(implode(" ", $stmnt->errorInfo()));
            }
            $stmnt->closeCursor();

            $query1
                = "INSERT INTO ESCALATION (user_id, request_time, priviledge_grant_time, force_enabling_time)"
                .
                "VALUES(:user_id, :request_time, :priviledge_grant_time, :force_enabling_time);";
            $stmnt = $this->prepare($query1);
            $now = time();
            $stmnt->bindValue(':user_id', $userID);
            $stmnt->bindValue(':request_time', $now);
            $stmnt->bindValue(
                ':priviledge_grant_time', $now + $this->autoPriviledgeGrantingTimeout
            );
            $stmnt->bindValue(
                ':force_enabling_time',
                $isAdmin ? $now : $now + $this->forceEnablingTime
            );
            if (!$stmnt->execute()) {
                throw new \Exception(implode(" ", $stmnt->errorInfo()));
            }
            $stmnt->closeCursor();

            return true;

        } catch (\Exception $e) {
            $this->log->critical("SQLiteWrapper.startEscalation:" . $e->getMessage());
            return false;
        }
    }


    public function getRunnumber()
    {
        try {
            $this->log->debug("SQLiteWrapper.getRunnumber()");

            $query = "SELECT runnumber " .
                "FROM RUNNUMBER;";

            $stmnt = $this->prepare($query);
            if (!$stmnt->execute()) {
                throw new \Exception(implode(" ", $stmnt->errorInfo()));
            }
            $resultData = $stmnt->fetch();
            $stmnt->closeCursor();

            return $resultData;

        } catch (\Exception $e) {
            $this->log->critical("SQLiteWrapper.getRunnumber:" . $e->getMessage());
            throw $e;
        }
    }

    public function setRunnumber($runnumber)
    {
        try {
            $this->log->debug("SQLiteWrapper.setRunnumber($runnumber)");

            $query = "UPDATE RUNNUMBER " .
                "SET runnumber = CASE id " .
                "WHEN '1' THEN  :runnumber " .
                " ELSE runnumber " .
                "END " .
                "WHERE id=1;";

            $stmnt = $this->prepare($query);
            $stmnt->bindValue(':runnumber', $runnumber);
            if (!$stmnt->execute()) {
                throw new \Exception(implode(" ", $stmnt->errorInfo()));
            }

            $resultData = $stmnt->fetch();
            $stmnt->closeCursor();;

            return $resultData;

        } catch (\Exception $e) {
            $this->log->critical("SQLiteWrapper.setRunnumber:" . $e->getMessage());
            throw $e;
        }

    }

    public function logAction($userid,
                              $data
    )
    {
        try {
            $this->log->debug("SQLiteWrapper.logAction");


            $query = "INSERT INTO AUDITLOG (user_id, timestamp, description) " .
                "VALUES(:userid, :timestamp, :description);";

            $stmnt = $this->prepare($query);

            $stmnt->bindValue(':userid', $userid);
            $stmnt->bindValue(':timestamp', time());
            $stmnt->bindValue(':description', $data);

            if (!$stmnt->execute()) {
                throw new \Exception(implode(" ", $stmnt->errorInfo()));
            }
            $stmnt->closeCursor();
            return true;

        } catch (\Exception $e) {
            $this->log->critical("SQLiteWrapper.logAction:" . $e->getMessage());
            throw $e;
        }
    }

    public function logCommand($userid,
                               $data
    )
    {
        try {
            $this->log->debug("SQLiteWrapper.logCommand($userid)");

            $query = "INSERT INTO DEMON_COMMANDS (userID,timestamp,answer) " .
                "VALUES(:userID, :timestamp, :answer);";


            $stmnt = $this->prepare($query);
            $stmnt->bindValue(':userID', $userid);
            $stmnt->bindValue(':timestamp', time());
            $stmnt->bindValue(':answer', $data);

            if (!$stmnt->execute()) {
                throw new \Exception(implode(" ", $stmnt->errorInfo()));
            }
            $stmnt->closeCursor();

            return true;

        } catch (\Exception $e) {
            $this->log->critical("SQLiteWrapper.logCommand:" . $e->getMessage());
            var_dump($e);
            throw $e;
        }
    }


    private function getUserID($username)
    {
        try {

            $query = "SELECT * " .
                "FROM USERS " .
                "WHERE USERS.username = :username;";

            $stmnt = $this->prepare($query);
            $stmnt->bindValue(':username', $username);
            if (!$stmnt->execute()) {
                throw new \Exception(implode(" ", $stmnt->errorInfo()));
            }

            $resultData = $stmnt->fetch();
            $stmnt->closeCursor();

            return $resultData;
        } catch (\Exception $e) {
            $this->log->critical("SQLiteWrapper.getUserID:" . $e->getMessage());
            throw $e;
        }
    }


    public function createEmptyUserData()
    {
        $empty = new \stdClass();

        $empty->id = -1;
        $empty->name = "";
        $empty->username = "";
        $empty->password = "";
        $empty->user_role = -1;
        $empty->user_can_escalate = 0;
        $empty->can_login = 0;
        $empty->role_name = "";
        $empty->role_can_escalate = 0;
        $empty->priviledge_will_expire = 1;
        $empty->isAdmin = 0;
        $empty->can_edit_user = 0;
        $empty->session_will_expire = 1;
        $empty->priviledge_expiration = 0;
        $empty->request_time = 0;
        $empty->priviledge_grant_time = 0;
        $empty->force_enabling_time = 0;
        $empty->token = "";
        $empty->expire = -1;
        $empty->UserEmpty = true;

        return $empty;

    }


    private function dumpToLog($var)
    {
        ob_start();
        var_dump($var);
        $this->log->debug(ob_get_clean());
    }


    private function getObject($queryResult)
    {
        return json_decode(json_encode($queryResult), false);
    }

    public function __destruct()
    {
        $this->log->debug("SQLiteWrapper.destruct");
    }
}
