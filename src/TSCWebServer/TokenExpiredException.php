<?php

namespace TSCWebServer;


class TokenExpiredException extends \Exception
{
    // Redefine the exception so message isn't optional
    public function __construct($message = null, $code = 403, Exception $previous = null) {
    
        $message = "The token is expired please login again " . $message;
        parent::__construct($message, $code, $previous);
    }

    // custom string representation of object
    public function __toString() {
        return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
    }

}
