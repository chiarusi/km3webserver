<?php

namespace TSCWebServer;
class UnathorizedUserExeption extends \Exception
{
    // Redefine the exception so message isn't optional
    public function __construct($message = null, $code = 1, Exception $previous = null) {
    
        $message = "The User can't issue commands: " . $message;
        parent::__construct($message, $code, $previous);
    }

    // custom string representation of object
    public function __toString() {
        return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
    }
}
