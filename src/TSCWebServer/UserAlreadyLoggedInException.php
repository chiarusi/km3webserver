<?php

namespace TSCWebServer;

class UserAlreadyLoggedInException extends \Exception
{
    // Redefine the exception so message isn't optional
    public function __construct($user, $code = 1, Exception $previous = null) {
    
        $message = "User:" . $user . " has already logged In, use the retrive token service or logout";
        parent::__construct($message, $code, $previous);
    }

    // custom string representation of object
    public function __toString() {
        return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
    }

}
