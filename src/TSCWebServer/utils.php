<?php

namespace TSCWebServer;

class utils
{
  public static function functionallyEmpty($o)
  {
    if (empty($o)) return true;
    else if (is_numeric($o)) return false;
    else if (is_string($o)) return !strlen(trim($o));
    else if (is_object($o)) return utils::functionallyEmpty((array)$o);

    // It's an array!
    else if (is_array($o)) {
      //echo "array!!!\n";
      foreach ($o as $element)
        if (utils::functionallyEmpty($element)) continue; // so far so good.
        else return false;
    }

    // all good.
    return true;
  }
  
  
  public static function getObject($array)
  {
    return json_decode(json_encode($array), FALSE);
  }

  public static function getPlainArrayWithseparatorKey($multidimensionalArray, $prefix = "")
  {
    $ret = array();
    foreach ($multidimensionalArray as $key => $value) {
      if (is_array($value)) {
        $ret = array_merge(
          $ret,
          utils::getPlainArrayWithseparatorKey($value,
            ($prefix == "") ? $key : $prefix . ":" . $key)
        );
      } else {
        $ret[($prefix == "") ? $key : $prefix . ":" . $key] = $value;
      }
    }
    return $ret;
  }

  public static function getObjectFromSeparatedArray($array)
  {
    $ret = array();

    foreach ($array as $object) {
      $ret = array_merge($ret, utils::keysToArray($object->valueID, $object->value, $ret));
    }

    return $ret;
  }

  public static function keysToArray($keys, $value, $array)
  {
    $explodedkeys = explode(":", $keys, 2);
    if (count($explodedkeys) > 1) {
      if (!array_key_exists($explodedkeys[0], $array)) {
        $array[$explodedkeys[0]] = array();
      }

      $array[$explodedkeys[0]] = utils::keysToArray($explodedkeys[1], $value, $array[$explodedkeys[0]]);
    } else {
      $array[$explodedkeys[0]] = $value;
    }
    return $array;
  }

}
