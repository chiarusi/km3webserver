CREATE TABLE AUDITLOG (
  id          INTEGER PRIMARY KEY  NOT NULL,
  user_id INTEGER NOT NULL,
  timestamp   INTEGER              NOT NULL,
  description TEXT                 NOT NULL
);
CREATE TABLE SESSIONS (
  user_id INTEGER PRIMARY KEY NOT NULL,
  token   TEXT                NOT NULL,
  expire  INTEGER             NOT NULL
);

CREATE TABLE ROLES (
  id                     INTEGER PRIMARY KEY  NOT NULL,
  role_name              TEXT                 NOT NULL,
  role_can_escalate      BOOLEAN              NOT NULL,
  priviledge_will_expire BOOLEAN              NOT NULL,
  isAdmin                BOOLEAN              NOT NULL,
  can_edit_user          BOOLEAN              NOT NULL,
  session_will_expire    BOOLEAN              NOT NULL
);

INSERT INTO ROLES (role_name, role_can_escalate, priviledge_will_expire, isAdmin, can_edit_user, session_will_expire)
VALUES ('viewer', 0, 1, 0, 0, 1);
INSERT INTO ROLES (role_name, role_can_escalate, priviledge_will_expire, isAdmin, can_edit_user, session_will_expire)
VALUES ('shifter', 1, 1, 0, 0, 1);
INSERT INTO ROLES (role_name, role_can_escalate, priviledge_will_expire, isAdmin, can_edit_user, session_will_expire)
VALUES ('datamanager', 1, 0, 0, 0, 0);
INSERT INTO ROLES (role_name, role_can_escalate, priviledge_will_expire, isAdmin, can_edit_user, session_will_expire)
VALUES ('admin', 1, 1, 1, 1, 1);

CREATE TABLE USERS (
  id                INTEGER PRIMARY KEY NOT NULL,
  name              TEXT                NOT NULL,
  username          TEXT                NOT NULL,
  password          TEXT                NOT NULL,
  user_role         INTEGER             NOT NULL,
  user_can_escalate BOOLEAN             NOT NULL,
  can_login         BOOLEAN             NOT NULL
);

INSERT INTO USERS (name, username, password, user_role, user_can_escalate, can_login)
VALUES ('nologin', 'nologin', '84de6401d62b73ad4c6b7686f5728eeb58823c9f4fcde48f248c280a788330ab', 3, 1, 0);
INSERT INTO USERS (name, username, password, user_role, user_can_escalate, can_login)
VALUES ('Matteo Favaro', 'matteo', '7aabe71ad057c84e013da845044f3d67aa421e5e0f4392556bcfeca9827bd6d1', 4, 1, 1);
INSERT INTO USERS (name, username, password, user_role, user_can_escalate, can_login) VALUES
  ('Francesco Giacomini', 'francesco', '19ed75a60d1c596135eac995ed66e7e3851752e8df3ab9c721943822851ac304', 4, 1, 1);
INSERT INTO USERS (name, username, password, user_role, user_can_escalate, can_login)
VALUES ('Matteo Manzali', 'manzali', 'e6dc478c0cb9f9dde864bf940c478060852dedd730020101fa2e29cd554454a1', 4, 1, 1);
INSERT INTO USERS (name, username, password, user_role, user_can_escalate, can_login)
VALUES ('Tommaso Chiarusi', 'tommaso', '5dc4727750bf748e42f555977d31e95318d4fc6a2e9356ab760bda6a16ffcd49', 4, 1, 1);
INSERT INTO USERS (name, username, password, user_role, user_can_escalate, can_login)
VALUES ('Carmelo Pellegrino', 'budda', '272d4631fccd78b51323eaeb22bca53fc1d193ffc449fafab85eef31382d2d20', 4, 1, 1);
INSERT INTO USERS (name, username, password, user_role, user_can_escalate, can_login)
VALUES
  ('Giuseppe Terreni', 'giuseppe', 'd63b6a53ba6fbbd0082877b2ad40ee7232fb12c822359aa5668dcff4c8f3fea4', 2, 1, 1);
INSERT INTO USERS (name, username, password, user_role, user_can_escalate, can_login)
VALUES ('datamanager', 'datamanager', 'c5c33e1cd326074e42afb1a8e52fc7162a456269c4b5e227e6f790f90c8e5b13', 3, 1, 1);
INSERT INTO USERS (name, username, password, user_role, user_can_escalate, can_login)
VALUES ('viewer', 'viewer', 'd50fd44f541e5bf8cca76137a5fccba1fc801caf62b2340d1f3cba080f52ffe5', 1, 0, 1);


CREATE TABLE DEMON_COMMANDS (
  id        INTEGER PRIMARY KEY NOT NULL,
  timestamp INTEGER             NOT NULL,
  userID    INTEGER             NOT NULL,
  answer    TEXT                NOT NULL
);

CREATE TABLE CURRENT_STATE_OPTIONS (
  valueID TEXT PRIMARY KEY NOT NULL,
  value   TEXT             NOT NULL,
  state   TEXT  KEY        NOT NULL
);
CREATE TABLE CURRENT_STATE (
  id    INTEGER PRIMARY KEY    NOT NULL,
  state TEXT         KEY       NOT NULL
);

CREATE TABLE RUNNUMBER (
  id        TEXT PRIMARY KEY NOT NULL,
  runnumber TEXT             NOT NULL
);

CREATE TABLE PUBLISHED_RUNSETUP (
  id          INTEGER PRIMARY KEY NOT NULL,
  description TEXT             NOT NULL,
  path        TEXT             NOT NULL,
  filename    TEXT             NOT NULL,
  user_id            INTEGER   NOT NULL,
  creation_timestamp INTEGER   NOT NULL,
  detector_id INTEGER
);
CREATE TABLE DRAFT_RUNSETUP (
  id                 INTEGER PRIMARY KEY NOT NULL,
  description        TEXT             NOT NULL,
  path               TEXT             NOT NULL,
  filename           TEXT             NOT NULL,
  detector_id        INTEGER                  ,
  user_id            INTEGER          NOT NULL,
  creation_timestamp INTEGER          NOT NULL
);

CREATE TABLE DATACARD_PER_RUN (
id          INTEGER PRIMARY KEY NOT NULL,
description TEXT             NOT NULL,
path        TEXT             NOT NULL,
filename    TEXT             NOT NULL,
detector_id INTEGER          NOT NULL,
run_number  INTEGER          NOT NULL,
timestamp   INTEGER          NOT NULL
);



CREATE TABLE PUBLISHED_DETECTORS (
  id          INTEGER PRIMARY KEY NOT NULL,
  description TEXT             NOT NULL,
  path        TEXT             NOT NULL,
  filename    TEXT             NOT NULL,
  published_timestamp INTEGER             NOT NULL
);

CREATE TABLE RUNSETUP_NAME (
  id         INTEGER PRIMARY KEY NOT NULL,
  value      INTEGER            NOT NULL,
  name       TEXT               NOT NULL
);

INSERT INTO RUNSETUP_NAME (value, name)
VALUES (0, 'runsetup');

CREATE TABLE ESCALATION (
  id                    INTEGER PRIMARY KEY NOT NULL,
  user_id               INTEGER             NOT NULL,
  request_time          INTEGER             NOT NULL,
  priviledge_grant_time INTEGER             NOT NULL,
  force_enabling_time   INTEGER             NOT NULL
);

CREATE TABLE PRIVILEDGE (
  id                    INTEGER PRIMARY KEY NOT NULL,
  user_id               INTEGER             NOT NULL,
  priviledge_expiration INTEGER             NOT NULL,
  no_answer_count       INTEGER
);

