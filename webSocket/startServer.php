<?php
if(!isset($docroot)){
   $docroot = realpath(dirname(__FILE__));
}
$statusFile = $docroot."/../logs/serverStatus.txt";

$fp = fopen("/tmp/lock.txt", "c");

if (flock($fp, LOCK_EX)) {  // acquire an exclusive lock
    ftruncate($fp, 0);      // truncate file
    fwrite($fp, "0");
    fflush($fp);            // flush output before releasing the lock
    flock($fp, LOCK_UN);    // release the lock
} else {
    echo "Couldn't get the lock!";
}

fclose($fp);


if(!file_exists($statusFile)) {
  file_put_contents($docroot."/../logs/serverStatus.txt", "0", LOCK_EX);
}
$status = file_get_contents($statusFile);
if(!isset($status) or $status == "" ) {
    file_put_contents($docroot."/../logs/serverStatus.txt", "0", LOCK_EX);
    $status = file_get_contents($statusFile);
}

error_log($status);
if($status == "0"){
	/* This means, the WebSocket server is not started. So we, start it */
	function execInbg($cmd) {
    if(!isset($docroot)){
  
      $docroot = realpath(dirname(__FILE__));
    } 
    error_log( "executing in background:" . $cmd . " >> ". $docroot."/../logs/websocketslog.txt &"); 
    exec($cmd . " >> " . $docroot . "/../logs/websocketslog.txt &");   

  }
  file_put_contents($statusFile, "1", LOCK_EX);
  execInbg("php " . $docroot. "/background.php");
}
