<?php

if(!isset($docroot)) {
  $docroot = realpath(dirname(__FILE__));
}

require_once $docroot.'/../vendor/autoload.php';
 

use \Ratchet\MessageComponentInterface;
use \Ratchet\ConnectionInterface;

class TridasWebSocket implements MessageComponentInterface {
	protected $clients;
	private $users = array();
	
	public function __construct() {
     $this->clients = new \SplObjectStorage;
    }
	
	public function onOpen(ConnectionInterface $conn) {
		$this->clients->attach($conn);
		echo "New connection! ({$conn->resourceId})";
	}

	public function onMessage(ConnectionInterface $from, $data) {
		$numRecv = count($this->clients) - 1;
        echo sprintf('Connection %d sending message "%s" to %d other connection%s' . "\n"
            , $from->resourceId, $data, $numRecv, $numRecv == 1 ? '' : 's');

        foreach ($this->clients as $client) {
            if ($from !== $client) {
                // The sender is not the receiver, send to each client connected
                $client->send($data);
            }
        }
		
	}

	public function onClose(ConnectionInterface $conn) {
		if( isset($this->users[$conn->resourceId]) ){
			unset($this->users[$conn->resourceId]);
		}
		$this->clients->detach($conn);
    echo sprintf('Connection Closed by %d' . "\n"
            , $conn->resourceId);
	}

	public function onError(ConnectionInterface $conn, Exception $e) {
		$conn->close();
	}
	

	public function send($client, $type, $data){
		$send = array(
			"type" => $type,
			"data" => $data
		);
		$send = json_encode($send, true);
		$client->send($send);
	}
}
?>
